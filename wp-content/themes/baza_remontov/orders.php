<?php /* Template Name: Orders */ ?>
<?php get_header() ; ?>
<?php if (is_user_logged_in()): ?>
<?php
  $user_id = get_current_user_id();
  $get_active_orders = get_my_orders($user_id, array(), array("3","5","36","34","2","35"));
  $get_inactive_orders = get_my_orders($user_id, array(), array("33","4","261"));
  echo "<script>window.active_orders = ".json_encode($get_active_orders)."</script>";
  echo "<script>window.inactive_orders = ".json_encode($get_inactive_orders)."</script>";
?>



  <section class="page-content flex-column flex-grow card">

    <header class="page-header flex-row flex-align-space-between">
      <section><h1 class="page-title"><?php echo the_title() ?></h1></section>
      <section class="flex-row flex-justify-center">
          <div id="search-table"></div>
      </section>
      <section class="flex-row flex-justify-end">
        <button type="button" onClick="table.addOrder()" class="waves-effect waves-light btn hide-on-small-only">+ Заказ</button>
        <button type="button" onClick="table.addOrder()" class="icon-button hide-on-med-and-up"><i class="material-icons">add</i></button>
      </section>
    </header>


          <ul id="tabs" class="tabs">
            <li class="tab"><a id="active_orders" href="#active_orders-block" class="active">Активные <span id="active_orders_counter"></span></a></li>
            <li class="tab"><a id="inactive_orders" href="#inactive_orders-block">Завершенные <span id="inactive_orders_counter"></span></a></li>
          </ul>

          <div id="active_orders-block"></div>
          <div id="inactive_orders-block"></div>

        <div id="orders" class="flex-grow flex-column">
          <div id="content-filter"></div>
          <div id="content" class="flex-grow"></div>
        </div>


    <?php get_template_part( 'custom', 'footer' ); ?>

</section>






<script type="text/javascript">
  var tabs = document.getElementById("tabs");
  var instance = M.Tabs.init(tabs, {onShow: showOrders});
  var tableFilter
  var table
  showOrders()

  function showOrders(){
    var attr = instance.$activeTabLink[0].getAttribute("id");
    tableFilter = new TableFilter(window[attr], "#content-filter", "#content");
    table = new Datatable(window[attr], "#content", tableFilter, document.querySelector('#' + attr + '_counter'));
  }
</script>

<?php endif; ?>
<?php get_footer(); ?>
