<?php /* Template Name: My repair parts */ ?>

<?php get_header(); ?>
<?php if (is_user_logged_in()): ?>
<?php

  $current_user_id = get_current_user_id();
  $get_repair_parts = get_repair_parts($current_user_id);

  echo "<script>window.apiData = ".json_encode($get_repair_parts)."</script>";
  $user_groups = wp_get_object_terms($current_user_id, 'user_position');
  $user_repair_parts = get_term_by( 'name', $user_groups[0]->name, 'repair_parts_taxonomy' );

  $repair_parts = get_terms( array(
      'taxonomy' => 'repair_parts_taxonomy',
      'parent' => $user_repair_parts->term_id,
      'hide_empty' => false
  ) );

?>

<section class="page-content flex-column flex-grow card">

  <header class="page-header flex-row flex-align-space-between">
    <section><h1 class="page-title"><?php echo the_title() ?></h1></section>
    <section class="flex-row flex-justify-center"><div id="search-table"></div></section>
    <section class="flex-row flex-justify-end"><div id="add-catalog-item"></div></section>
  </header>
  <div id="content"></div>
  <?php get_template_part( 'custom', 'footer' ); ?>
</section>




<script type="text/javascript">
  let target = document.querySelector('#content');
  let catalog = new EditibleCatalog(window.apiData, target);
</script>
<?php endif; ?>

<?php get_footer(); ?>
