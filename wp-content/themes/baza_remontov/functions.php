<?php

require_once ABSPATH . 'wp-admin/includes/user.php';

include_once dirname(__FILE__) . '/inc/core.php';
include_once dirname(__FILE__) . '/inc/custom-taxonomy.php';
include_once dirname(__FILE__) . '/inc/comment.php';
include_once dirname(__FILE__) . '/inc/get_orders.php';
include_once dirname(__FILE__) . '/inc/get_repair_parts.php';
include_once dirname(__FILE__) . '/inc/get_works.php';
include_once dirname(__FILE__) . '/inc/get_users.php';
include_once dirname(__FILE__) . '/inc/get_clients.php';
include_once dirname(__FILE__) . '/inc/get_docs.php';

add_theme_support('menus');


function create_post_type(){
	register_post_type( 'my_doc',
		array(
			'labels'=> array(
				'name' => __('my_docs'),
				'singular_name' => __('my_doc'),
			),
			'public' => true,
			'has_archive' => true,
			'supports' => array('title', 'editor', 'thumbnail'),
		)
	);
}
add_action('init', 'create_post_type');


if(isset($_POST['submit-login-dialog'])) {
	$creds = array(
					'user_login'    => $_POST['username'],
					'user_password' => $_POST['password'],
					'remember'      => true
			);

			$user = wp_signon( $creds, false );
			if ( is_wp_error($user) ):
				echo $user->get_error_message();
			endif;
			wp_set_current_user($user->ID);
				return $user;
}




// 
//
//
// add_action('after_setup_theme', 'remove_admin_bar');
// function remove_admin_bar() {
// if (!current_user_can('administrator') && !is_admin()) {
//   show_admin_bar(false);
// }
// }


## Оставляет пользователя на той же странице при вводе неверного логина/пароля в форме авторизации wp_login_form()

function my_front_end_login_fail( $username ) {
	$referrer = $_SERVER['HTTP_REFERER'];  // откуда пришел запрос

	// Если есть referrer и это не страница wp-login.php
	if( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
		wp_redirect( add_query_arg('login', 'failed', $referrer ) );  // редиркетим и добавим параметр запроса ?login=failed
		exit;
	}
}
add_action( 'wp_login_failed', 'my_front_end_login_fail' );
//// Logout redirect

// add_action('wp_logout','ps_redirect_after_logout');
// function ps_redirect_after_logout(){
//          $page_url = get_page_link(997);
//          wp_redirect($page_url);
//          exit();
// }



//DANGER THIS WILL HIDE ALL CONTENT
// add_action( 'login_form_login', 'action_function_name_7970' );
// function action_function_name_7970(){
// 	$page_url = get_page_link(789);
//   wp_redirect($page_url);
//   exit();
// }



function custom_login() {
    if (isset($_POST['submit'])) {

			//printf("dasdsdsd");
        // $login_data = array();
        // $login_data['user_login'] = sanitize_user($_POST['username']);
        // $login_data['user_password'] = esc_attr($_POST['password']);
				//
        // $user = wp_signon( $login_data, false );
				//
        // if ( is_wp_error($user) ) {
        //     echo $user->get_error_message();
        // } else {
        //     wp_clear_auth_cookie();
        //     do_action('wp_login', $user->ID);
        //     wp_set_current_user($user->ID);
        //     wp_set_auth_cookie($user->ID, true);
        //     $redirect_to = $_SERVER['REQUEST_URI'];
        //     wp_safe_redirect($redirect_to);
        //     exit;
        // }
    }
}

add_action( 'init', 'custom_login' );




function user_last_login( $user_login, $user ) {
    update_user_meta( $user->ID, 'last_login', time() );
}
add_action( 'wp_login', 'user_last_login', 10, 2 );





//
// // Show posts count in category
//
// add_filter('the_title', 'generate_category_post_count_title', 10, 2);
// function generate_category_post_count_title($title, $post_ID){
//   $cur_user_id = get_current_user_id();
//   $user_groups = wp_get_object_terms($cur_user_id, 'user_position');
//   $category_new = new WP_Query(  array(
//                                   'category_name' => 'new',
//                                   'user_position' => $user_groups[0]->slug
//                                 ) );
//
//   $category_active = new WP_Query(  array(
//                                   'category_name' => 'active_orders',
//                                   'user_position' => $user_groups[0]->slug
//                                 ) );
//
//   $cat_new_slug = get_category_by_slug($category_new->query['category_name']);
//
//     if( 'nav_menu_item' == get_post_type($post_ID) )
//     {
//         if( 'taxonomy' == get_post_meta($post_ID, '_menu_item_type', true) && 'category' == get_post_meta($post_ID, '_menu_item_object', true) )
//         {
// 						$category = get_category( get_post_meta($post_ID, '_menu_item_object_id', true) );
//
//             $args = array(
//              'category_name' => $category->slug,
//              'user_position' => $user_groups[0]->slug
//             );
//
//             $query = new WP_Query( $args );
//
//             if($category->cat_ID == 259){ // категория заказы
//               //$title .= sprintf(' (' . $category_active->found_posts . ')');
//               if($category_new->found_posts > 0){
//                 $title .= sprintf('<span id="orders-count" class="new badge right"> ' . $category_new->found_posts . '</span>');
//               }
//             }
//             //if($category->category_parent == 263){
//               //$title .= sprintf(' (%d)', $query->post_count);
//             //}
//         }
//     }
//     return $title;
// }





//
// function template_category_template_redirect()
// {
//   $url = get_category_link( 259 ); /// категория активные заказы
//     if( is_category('263')) /// категория заказы
//     {
//         wp_safe_redirect( $url, 301 );
//         die;
//     }
// }
// add_action( 'template_redirect','template_category_template_redirect' );
//
//
//








function wpb_custom_new_menu() {
  register_nav_menus(
    array(
      'pages-menu' => __( 'pages menu' ),
			'my-service' => __( 'my service' ),
			'main-menu' => __( 'main menu' ),
			'mobile-menu' => __( 'mobile menu' ),
			'main-menu-mobile' => __( 'main menu mobile' ),

    )
  );
}
add_action( 'init', 'wpb_custom_new_menu' );


// let's add "*active*" as a class to the li

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
     if( in_array('current-menu-item', $classes) ){
             $classes[] = 'active ';
     }
     return $classes;
}

// let's add our custom class to the actual link tag

// function atg_menu_classes($classes, $item, $args) {
//   if($args->theme_location == 'pages-menu') {
//     $classes[] = 'nav-link';
//   }
//   return $classes;
// }
// add_filter('nav_menu_css_class', 'atg_menu_classes', 1, 3);




// function add_menuclass($ulclass) {
//    return preg_replace('/<a /', '<a class="mdl-navigation__link"', $ulclass);
// }
// add_filter('wp_nav_menu','add_menuclass');
//





//
//
//
//
// function set_master_to_post() {
//     // Query Arguments
//
//   	$post_id = $_POST['post_id'];
// 		$cur_user_id = get_current_user_id();
//
// 		//$new_post_term = wp_set_object_terms( $post_id, '76', 'user_position' );
//     $new_post_term = wp_set_post_terms( 724, [123], 'user_position' );  //  intval(42)
// 		echo json_encode($new_post_term);
// 		exit;
// }
//
// // Fire AJAX action for both logged in and non-logged in users
// add_action('wp_ajax_set_master_to_post', 'set_master_to_post');
//
//
//



 ?>


 <?php

show_admin_bar(false);
?>



<?php
// уходит в бесконечный цикл если нет ремонтов!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// для показа имени главной родительской категории
 // determine the topmost parent of a term
    //function get_term_top_most_parent($term_id, $taxonomy) {
       // start from the current term
       //$parent = get_term_by('id', $term_id, $taxonomy);
       // climb up the hierarchy until we reach a term with parent = '0'
      // while ($parent->parent != '0') {
        //  $term_id = $parent->parent;

        //  $parent = get_term_by('id', $term_id, $taxonomy);
       //}
      // return $parent;
    //}
?>





















<?php


function getPostsNumber($user_id, $from_cats){

  $user_groups = wp_get_object_terms($user_id, 'user_position');

	$posts_array = get_posts( array(
		'numberposts' => 0,
		'category'    => $from_cats,
		'orderby'     => 'date',
		'meta_key'    => '',
		'nopaging'    => true,
		'fields'      => 'ids',
		'include'     => '',
		'meta_value'  =>'',
		'post_type'   => 'post',
		'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
		'tax_query' => array(
			array(
				'taxonomy' => 'user_position',
				'field' => 'term_id',
				'terms' =>  array($user_groups[0]->term_id),
				'include_children' => false
			)
		)
	) );

		return sizeof($posts_array);

}

?>












 <?php
 function mytheme_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
      <div id="comment-<?php comment_ID(); ?>">
 			 <span class="mdl-chip mdl-chip--contact">
 			    <span class="mdl-chip__contact"><?php echo get_avatar($comment,$size='32',$default='<path_to_url>' ); ?></span>
 			    <span class="mdl-chip__text"> <?php echo get_comment_author_link() ?></span>
 			</span>

       <?php if ($comment->comment_approved == '0') : ?>
          <em><?php _e('Your comment is awaiting moderation.') ?></em>
          <br />
       <?php endif; ?>

       <div class="comment-meta commentmetadata">
 					<?php printf(__('%1$s, %2$s'), get_comment_date(),  get_comment_time()) ?>
           <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
                 <?php edit_comment_link(__('<i class="material-icons">edit</i>'),'<button class="mdl-button mdl-js-button mdl-button--icon" data-upgraded=",MaterialButton">','</button>') ?>
           </a>

       </div>

       <?php comment_text() ?>

       <div class="reply">
          <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
       </div>
      </div>
 <?php
         }
 ?>
