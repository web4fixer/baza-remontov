<?php get_header() ; ?>
<?php get_template_part( 'toolbar-page' ); ?>
<main id="page">
	<div class="container">
		Single.php
	<?php
		if (have_posts()):
			while (have_posts()) : the_post();
				the_title();
				the_content();
			endwhile;
		else:
			echo '<p>Sorry, no posts matched your criteria.   SINGLE PHP</p>';
		endif;
	?>

</div>
</main>

<?php get_footer(); ?>
