
<?php get_header(); ?>
<?php get_template_part( 'header-toolbar' ); ?>
<?php get_sidebar(); ?>

<main class="mdl-layout__content mdl-color--grey-100">
	<section class="mdl-grid demo-content" style="flex-direction: column">

		<?php if (have_posts()) { ?>
		<section class="mdl-grid demo-content">
			<table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp full-width selectable-rows">
						<thead>
							<tr>
								<th class="mdl-data-table__cell--non-numeric">ID</th>
								<th class="mdl-data-table__cell--non-numeric">Название</th>
								<th class="mdl-data-table__cell--non-numeric">Описание</th>
								<th class="mdl-data-table__cell--non-numeric">Комментарии</th>
							</tr>
						</thead>
					<tbody>
			<?php while (have_posts()) : the_post(); ?>

					<tr class="post" id="post-<?php the_ID(); ?>">
						<td class="mdl-data-table__cell--non-numeric"><?php the_ID() ?></h5></td>
						<td class="mdl-data-table__cell--non-numeric"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></td>
						<td class="mdl-data-table__cell--non-numeric"><?php echo wp_trim_words( get_the_content(), $num_words = 20, $more = null ); ?></td>
						<td class="mdl-data-table__cell--non-numeric">
							<a href="<?php the_permalink() ?>#comments" class="comments-number">
								<?php comments_number('нет комментариев', '1 комменатрий', '% комментариев'); ?>
							</a>
						</td>
					</tr>

			<?php endwhile; ?>

				</tbody>
			</table>
			</section>


		<?php } else { ?>
			<h5>Ничего не найдено</h5>
			<?php } ?>

	</section>
</main>


	<?php get_footer(); ?>
