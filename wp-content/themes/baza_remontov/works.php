<?php /* Template Name: My works */ ?>

<?php get_header(); ?>

<?php
  $current_user_id = get_current_user_id();
  $get_works = get_works($current_user_id);
  echo "<script>window.apiData = ".json_encode($get_works)."</script>";
?>

<div class="flex-row flex-align-stretch flex-grow">
	<?php get_template_part('services-pages-menu'); ?>
	<section class="page-content flex-column flex-grow card">
		<?php
			wp_nav_menu( [
				'menu' => 'my service',
				'container' => 'ul',
				'menu_class' => 'tabs-pages',
			 ]);
		?>
    <header class="page-header flex-row flex-align-space-between">
			<section><h1 class="page-title"><?php echo the_title() ?></h1></section>
			<section class="flex-row flex-justify-center"><div id="search-table"></div></section>
			<section class="flex-row flex-justify-end"><div id="add-catalog-item"></div></section>
		</header>

    <div id="content">
    </div>
    <?php get_template_part( 'custom', 'footer' ); ?>
	</section>
</div>



<script type="text/javascript">
  let target = document.querySelector('#content');
  let catalog = new EditibleCatalog(window.apiData, target);
</script>


<?php get_footer(); ?>
