'use strict';

jQuery(document).ready(function($) {


 $('select').formSelect();

 $('.modal').modal({ dismissible: true, preventScrolling: true });

 //$('.tabs').tabs({});

 $(".dropdown-menu").dropdown({ alignment: 'right', constrainWidth: false });

 $('.tooltipped').tooltip({ exitDelay:0, enterDelay: 500});

 $('.sidenav').sidenav();

 $('.collapsible').collapsible();

 $('.materialboxed').materialbox();

});

var calendarLocales = {
 cancel:	'Отмена',
 clear:	'Сбросить',
 done:	'Применить',
 previousMonth:	'‹',
 nextMonth:	'›',
 months:
 [
	 'Январь',
	 'Февраль',
	 'Март',
	 'Апрель',
	 'Май',
	 'Июнь',
	 'Июль',
	 'Август',
	 'Сентябрь',
	 'Октябрь',
	 'Ноябрь',
	 'Декабрь'
 ],

 monthsShort:
 [
	 'Янв',
	 'Фев',
	 'Мар',
	 'Апр',
	 'Май',
	 'Июн',
	 'Июл',
	 'Авг',
	 'Сен',
	 'Окт',
	 'Ноя',
	 'Дек'
 ],

 weekdays:
 [
	 'Воскресенье',
	 'Понедельник',
	 'Вторник',
	 'Среда',
	 'Четверг',
	 'Пятница',
	 'Суббота'
 ],

 weekdaysShort:
 [
	 'Вс',
	 'Пн',
	 'Вт',
	 'Ср',
	 'Чт',
	 'Пт',
	 'Сб'
 ],

 weekdaysAbbrev:	['В','П','В','С','Ч','П','С']
}



function formatPhoneNumber(value) {
  if (!value) return value;
  const phoneNumber = value.replace(/[^\d]/g, "");
  const phoneNumberLength = phoneNumber.length;
  if (phoneNumberLength < 4) return phoneNumber;
  if (phoneNumberLength < 7) {
    return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(3)}`;
  }
  return `(${phoneNumber.slice(0, 3)}) ${phoneNumber.slice(
    3,
    6
  )} ${phoneNumber.slice(6, 9)}`;
}

function phoneNumberFormatter() {
  const inputField = document.getElementById("phone-number");
  const formattedInputValue = formatPhoneNumber(inputField.value);
  inputField.value = formattedInputValue;
}




  function previewFile(){
    console.log("я запустился previewFile")
  	var preview = document.querySelector('#attach-image-preview');
  	var saved = document.querySelector('#saved-image-preview');
  	var removeAvatarButton = document.querySelector('#remove-avatar-button');

  	removeAvatarButton.onclick = event => {
  		preview.src = "";
  		saved.src = "";
  		event.target.closest('button').classList.add("hide");
  		preview.classList.add("hide");
  	}

  	var file    = document.querySelector('input[type=file]').files[0];
  	var reader  = new FileReader();

  	reader.onloadend = function () {
  		preview.src = reader.result;
  	}

  	if (file) {
  		reader.readAsDataURL(file);
  		saved.classList.add("hide");
  		preview.classList.remove("hide");
  		removeAvatarButton.classList.remove("hide");
  	}
  	else {
  		preview.src = "";
  	}
  }
