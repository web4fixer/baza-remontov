

'use strict';





class EditibleCatalog{
	constructor(data, targetID, selectable, selectedList) {
		this.data = data;
		this.targetID = targetID;
		this.selectable = selectable;
		this.searchQuery = "";
		this.selected = {};
		this.tables = {};
		this.selectedIds = [];
		if(selectedList){
			this.selectedIds = selectedList;
		}

		this.renderHtml(this.data, this.targetID);
		this.activeTab = 0;



}
  // Methods
	searchTable(searchQuery){

		for(var table in this.tables){
		//	console.log(this.tables[table]);
	//		console.log(this.tables[table].getElementsByTagName("tr"));
			var tr, td, i, txtValue;
			tr = this.tables[table].getElementsByTagName("tr");
			// Loop through all table rows, and hide those who don't match the search query
			for (i = 1; i < tr.length; i++) {
				td = tr[i].querySelector("td[search-index]")
				if (td) {
					txtValue = td.textContent || td.innerText;
					if (txtValue.toUpperCase().indexOf(searchQuery) > -1) {
						tr[i].style.display = "";
					} else {
						tr[i].style.display = "none";
					}
				}
			}
		}


	}
	reloadData(){
		let formData = new FormData();
		formData.append('action', this.data.params.getDataFunctionName);

		this.sendData(formData, '', '', (data) => {this.renderHtml(data, this.targetID)});
		//console.log('%c data reloaded! ', 'background: #222; color: #bada55');
	}
	showLoader(){
		let loader = document.createElement('div');
		let loaderContent = document.createElement('div');
		loader.classList.add("progress");
		loaderContent.classList.add("indeterminate");
		loader.appendChild(loaderContent);
		document.body.prepend(loader);
		return loader;
	}
	sendData(obj, toastSuccessText, toastFailText, callback){
		// Ajax
		var request = new XMLHttpRequest();
		request.open('POST', MyAjax.ajaxurl, true);
		request.setRequestHeader('accept', 'application/json');
		request.send(obj);
		var loader = this.showLoader();
		// Функция для наблюдения изменения состояния request.readyState обновления statusMessage
		request.onreadystatechange = function () {
			if (request.readyState < 4){
				// <4 =  ожидаем ответ от сервера
			}
			else if (request.readyState === 4) {
				// 4 = Ответ от сервера полностью загружен

				if (request.status == 200 && request.status < 300){


					loader.remove();

					var data = JSON.parse(request.responseText);

					if(data.error_data){
						M.toast({html: data.errors, classes: 'toast-error', displayLength: 3000});
					}

					else{
						if(toastSuccessText){
							M.toast({html: toastSuccessText, classes: 'toast-success', displayLength: 3000});
						}
						if(callback){
								//console.log("callback in send data");
								callback(data);
						}
					}


					//	window.apiData = data;


				}
				else{
						if(toastFailText){
							M.toast({html: toastFailText, classes: 'toast-success', displayLength: 3000});
						}
				}
			}
		}
		// end Ajax
	}
	renderHtml(data, targetID){

		targetID.innerHTML = '';

		var component = document.createElement('article');
		component.classList.add("catalog");

		//let tabsListAddItem = document.createElement('li');
		//tabsListAddItem.classList.add('tab');

	//	tabsListAddItem.append(addCatButton);
		let addCatButton = this.createTextButton(this.data.params.addCatButton, true);
		addCatButton.onclick = () => {
			this.createDialog(
				this.data.params.addCatDialogTitle,  // title
				new Array(this.data.params.topLevelCatData),	      // content
				this.data.params.addCatFunctionName, // actionName
				this.data.params.addCatToastSuccessText,		// toastSuccessText
				this.data.params.addCatToastFailText,				// toastFailText
				this.data.params.dialogApplyButtonText,    // confirmButtonText
				0,														// id
				() => {this.reloadData();}  // callback
			);
		};

		let tabsList = document.createElement('ul');
		tabsList.classList.add("tabs");
		var data = data['page_data'];



		var addCatButtonPath = document.querySelector('#add-catalog-item');

		if(addCatButtonPath){
			addCatButtonPath.prepend(addCatButton);
		}
		else{
			addCatButton.classList.add("btn-secondary", "flex-order-3");
			targetID.parentElement.prepend(addCatButton);
		}

		component.append(tabsList);

		Object.size = function(obj) {
		  var size = 0,
		    key;
		  for (key in obj) {
		    if (obj.hasOwnProperty(key)) size++;
		  }
		  return size;
		};

		// Get the size of an object
		if(Object.size(data) > 0){
			var	keys = Object.keys(this.data.params.columns);
			for(let i in data){
				var tabsItem = document.createElement('li');
				var tabsItemLink = document.createElement('a');

				var tabsContentItem = document.createElement('div');
				tabsContentItem.setAttribute("id", data[i].cat_id);

				var tabsContentItemHeader = document.createElement('header');
				tabsContentItemHeader.classList.add("tab-content", "flex-row", "flex-align-space-between");

				// let importSubCatButton = this.createFileButton('upload', this.data.params.exportTooltipText);
				// let exportSubCatButton = this.createButton('download', this.data.params.exportTooltipText);
				let addSubCatButton = this.createButton('add', this.data.params.addDialogTitle);
				// let editCatButton = this.createButton('edit', this.data.params.updateTooltipText);
		    // let deleteCatButton = this.createButton('delete', this.data.params.deleteTooltipText);


				let importSubCatButton = this.createFileButton('Импорт');
				let exportSubCatButton = this.createMenuItem('Экспорт');
				let editCatButton = this.createMenuItem('Редактировать');
		    let deleteCatButton = this.createMenuItem('Удалить');


				var tabActionsWrapper = document.createElement('div');
				tabActionsWrapper.classList.add("tab-actions");
				tabActionsWrapper.append(addSubCatButton);


				let moreButton = this.createButton('more_vert');
				moreButton.classList.add("dropdown-trigger", "icon-button");
				moreButton.setAttribute("data-target", "cat-" + data[i].cat_id);
				tabActionsWrapper.append(moreButton);

				let actionsMenu = document.createElement("ul");
				actionsMenu.classList.add("dropdown-content");
				actionsMenu.setAttribute("id", "cat-" + data[i].cat_id);
				tabActionsWrapper.append(actionsMenu);

				actionsMenu.append(importSubCatButton);
				actionsMenu.append(exportSubCatButton);
				actionsMenu.append(editCatButton);
				actionsMenu.append(deleteCatButton);


				let	catDataKeys = Object.values(data[i].data);

				importSubCatButton.addEventListener('change', (event) => {
						var that = this;
						var id = data[i].cat_id;
						var oFile = event.target.files[0];
						var sFilename = oFile.name;

						var reader = new FileReader();
						var result = {};

						reader.onload = function (e) {

								var data = e.target.result;
								data = new Uint8Array(data);
								var workbook = XLSX.read(data, {type: 'array'});

								var result = {};
								workbook.SheetNames.forEach(function (sheetName) {
										var roa = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {header: 1});
										if (roa.length) result = roa;
								});


								var formData = new FormData;
								formData.append('action', that.data.params.importFunctionName);
								formData.append('cat_id', id);
								formData.append('data', JSON.stringify(result));
								console.log(result);
								// for (var pair of formData.entries()) {
								//     console.log(pair[0]+ ', ' + pair[1]);
								// }
								// that.sendData(
								// 	formData,
								// 	that.data.params.importToastSuccessText,
								// 	that.data.params.importToastFailText,
								// 	 () => {
								// 		 that.reloadData();
								// 	 }
								//  );
						};
						reader.readAsArrayBuffer(oFile);

				});

				editCatButton.onclick = () => {
					this.createDialog(
						this.data.params.editCatDialogTitle,	 // title
						new Array(data[i].data),																			 // content
						this.data.params.editCatFunctionName,// actionName
						this.data.params.editCatToastSuccessText,	// toastSuccessText
						this.data.params.editCatToastFailText,	// toastFailText
						this.data.params.dialogApplyButtonText,  // confirmButtonText
						data[i].cat_id,                                       // id
						(updatedData) => {
							console.log(this.data.page_data[i]);
							this.data.page_data[i].name = updatedData.get([0]);
							this.data.page_data[i].data.value = updatedData.get([0]);
							tabsItemLink.textContent = updatedData.get([0]);
							tabsContentItemTitle.textContent = updatedData.get([0]);
							console.log(this.data.page_data[i].name);
						//	this.data.page_data[data[i]] = formData.get([j]);
						}																// callback															// callback
					);
				};
				deleteCatButton.onclick = () => {
					this.createDialog(
						this.data.params.deleteCatDialogTitle,  // title
						data[i].name,                             // content
						this.data.params.deleteCatFunctionName, // actionName
						this.data.params.deleteCatToastSuccessText,		// toastSuccessText
						this.data.params.deleteCatToastFailText,				// toastFailText
						this.data.params.deleteDialogButton,    // confirmButtonText
						data[i].cat_id,  															// id
						() => {console.log("2 callback in delete button"); this.reloadData();}
					);

				};

				var tabsContentItemTitle = document.createElement('h6');
				tabsContentItemTitle.classList.add("tab-title");



				// Set header content
				tabsContentItemTitle.textContent = data[i].name;
				tabsContentItemHeader.append(tabsContentItemTitle);

				tabsContentItemHeader.append(tabActionsWrapper);


				tabsContentItem.append(tabsContentItemHeader);

				let table = document.createElement('table');

				addSubCatButton.onclick = () => {
					this.createDialog(
						this.data.params.addDialogTitle, // title
						data[i].columns,
						this.data.params.addFunctionName,// actionName
						this.data.params.addToastSuccessText,	// toastSuccessText
						this.data.params.addToastFailText,	// toastFailText
						this.data.params.dialogApplyButtonText,   // confirmButtonText
						data[i].cat_id,																							 // id  -- parent category
						(responseObj) => {	this.insertNewRow(table, responseObj, data[i].cat_id)}  // callback
					);
				};


				this.tables[i] = table;

				let tableHeader = table.createTHead();
				let tableHeaderRow = tableHeader.insertRow();
				var tableBody = table.createTBody();
				table.classList.add('catalog-table', 'highlight', 'striped', 'sortable');

			if(this.selectable){
				tableHeaderRow.appendChild(document.createElement('th'))
			}

				keys.forEach(function (value, item) {
					let th = document.createElement('th');
				//	console.log(data[i].columns)

					th.textContent = data[i].columns[item].name;
					tableHeaderRow.appendChild(th);
				});

				tableHeaderRow.appendChild(document.createElement('th'))

				//console.log(this.selectedIds)


				if(data[i].sub_cats){
					if(Object.keys(data[i].sub_cats).length > 0){

						for(let j in data[i].sub_cats){

							let newRow = tableBody.insertRow();

							if(this.selectable){
								this.createSelectableCell(newRow, data[i].sub_cats[j].cat_id, data[i].sub_cats[j]);
							}

							var count = 0;
							var keys = Object.keys( data[i].sub_cats[j].data );

							for(let k in data[i].columns){

								let currentItem = data[i].sub_cats[j].data[keys[count]];

									if(this.selectedIds.includes(data[i].sub_cats[j].cat_id)){

										this.selectedIds.push(data[i].sub_cats[j].cat_id);
										this.selected[data[i].sub_cats[j].cat_id] = data[i].sub_cats[j];

										newRow.classList.add("selected");

										newRow.cells[0].querySelector("input[type='checkbox']").checked = true;
									}

								let newCell = newRow.insertCell();


								if(data[i].columns[count].type == "currency"){
									newCell.textContent = new Intl.NumberFormat('ru-RU', { style: 'currency', currency: 'UAH' }).format(currentItem)
								}
								else{
									newCell.setAttribute("search-index", "");
									newCell.textContent = currentItem;
								}

								count++;
							}

							this.createActionsCell(newRow, data[i].sub_cats[j], data[i].sub_cats[j].name, data[i]);

						}
					}
					else{
						table.style.display = "none";

						var emptyStateWrapper = document.createElement("section");
						emptyStateWrapper.classList.add("empty-state", "flex-column", "flex-justify-center", "flex-align-center", "flex-grow");

						var emptyStateText = document.createElement("h6");
						emptyStateText.classList.add("empty-state-title");

					

						emptyStateText.textContent = "Тут пока пусто";

						var emptyStateIcon = document.createElement("div");
						emptyStateIcon.innerHTML += '<svg width="167px" height="101px" viewBox="0 0 167 101" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="NoDocuments" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Group" transform="translate(0.000000, 0.000000)"><path d="M160,15 C163.865993,15 167,18.1340068 167,22 C167,25.8659932 163.865993,29 160,29 L120,29 C123.865993,29 127,32.1340068 127,36 C127,39.8659932 123.865993,43 120,43 L142,43 C145.865993,43 149,46.1340068 149,50 C149,53.8659932 145.865993,57 142,57 L131.826087,57 C126.951574,57 123,60.1340068 123,64 C123,66.5773288 125,68.9106622 129,71 C132.865993,71 136,74.1340068 136,78 C136,81.8659932 132.865993,85 129,85 L46,85 C42.1340068,85 39,81.8659932 39,78 C39,74.1340068 42.1340068,71 46,71 L7,71 C3.13400675,71 0,67.8659932 0,64 C0,60.1340068 3.13400675,57 7,57 L47,57 C50.8659932,57 54,53.8659932 54,50 C54,46.1340068 50.8659932,43 47,43 L22,43 C18.1340068,43 15,39.8659932 15,36 C15,32.1340068 18.1340068,29 22,29 L62,29 C58.1340068,29 55,25.8659932 55,22 C55,18.1340068 58.1340068,15 62,15 L160,15 Z M160,43 C163.865993,43 167,46.1340068 167,50 C167,53.8659932 163.865993,57 160,57 C156.134007,57 153,53.8659932 153,50 C153,46.1340068 156.134007,43 160,43 Z" id="Background" fill="#F3F7FF"></path><path d="M111.708558,17.671844 L112.673157,86.1424997 L112.673157,92.9999216 C112.673157,95.2090606 110.882296,96.9999216 108.673157,96.9999216 L49.6731572,96.9999216 C47.4640182,96.9999216 45.6731572,95.2090606 45.6731572,92.9999216 L45.6731572,19.0209641 C45.6731572,17.9163946 46.5685877,17.0209641 47.6731572,17.0209641 C47.6801346,17.0209641 47.687112,17.0209641 47.694089,17.0209641 L52.5826591,17.0722397 M56.535613,17.1136132 L61.1511792,17.161922" id="Shape" stroke="#75A4FE" stroke-width="2.5" fill="#FFFFFF" stroke-linecap="round" transform="translate(79.173157, 57.010443) rotate(-7.000000) translate(-79.173157, -57.010443) "></path><path d="M108.700739,21.5543308 L109.564559,83.6058625 L109.564559,89.8204011 C109.564559,91.8224333 107.960803,93.4454011 105.98247,93.4454011 L53.1466487,93.4454011 C51.1683153,93.4454011 49.5645591,91.8224333 49.5645591,89.8204011 L49.5645591,22.9454011 C49.5645591,21.8408316 50.4599896,20.9454011 51.5645591,20.9454011 L58.1008814,20.9454011 L58.1008814,20.9454011" id="Rectangle" fill="#E8F0FE" transform="translate(79.564559, 57.195401) rotate(-7.000000) translate(-79.564559, -57.195401) "></path><path d="M109.228636,1.25 C109.957687,1.25 110.656902,1.53949778 111.172578,2.05485483 L111.172578,2.05485483 L124.615817,15.4897778 C125.13191,16.0055517 125.421875,16.7052828 125.421875,17.434923 L125.421875,17.434923 L125.421875,80 C125.421875,80.7593915 125.114071,81.4468915 124.616419,81.9445436 C124.118767,82.4421958 123.431267,82.75 122.671875,82.75 L122.671875,82.75 L63.671875,82.75 C62.9124835,82.75 62.2249835,82.4421958 61.7273314,81.9445436 C61.2296792,81.4468915 60.921875,80.7593915 60.921875,80 L60.921875,80 L60.921875,4 C60.921875,3.24060847 61.2296792,2.55310847 61.7273314,2.05545635 C62.2249835,1.55780423 62.9124835,1.25 63.671875,1.25 L63.671875,1.25 Z" id="Rectangle" stroke="#75A4FE" stroke-width="2.5" fill="#FFFFFF"></path><path d="M109.671875,2.40283203 L109.671875,14 C109.671875,15.6568542 111.015021,17 112.671875,17 L120.605469,17" id="Shape" stroke="#75A4FE" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"></path><path d="M71.671875,17 L97.671875,17 M71.671875,29 L114.671875,29 M71.671875,42 L114.671875,42 M71.671875,55 L114.671875,55 M71.671875,68 L97.671875,68" id="lines" stroke="#A4C3FE" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"></path></g></g></svg>';

						emptyStateWrapper.append(emptyStateIcon);
						emptyStateWrapper.append(emptyStateText);
					//	console.log(tabsContentItem)
						tabsContentItem.append(emptyStateWrapper)
				//console.log(table.parentNode)
						//table.insertBefore(emptyStateWrapper, table)
						//table.innerHTML = emptyStateWrapper.innerHTML;
					}
				}


				// else{
				// 	console.log(this)
				// 	table.style.display = "none";
			  // 	let newRow = tableBody.insertRow();
				// 	let newCell = newRow.insertCell();
				// 	newCell.textContent  = window.apiData.params.EmptyText;
				// 	newCell.setAttribute("colspan", keys.length + 1)
				// }

				tabsItem.classList.add("tab");
				tabsItemLink.setAttribute("href", '#' + data[i].cat_id);
				tabsItemLink.textContent = data[i].name;
				tabsItem.appendChild(tabsItemLink);
				tabsList.appendChild(tabsItem);
				tabsContentItem.appendChild(table);
				component.appendChild(tabsContentItem);

				// Export
				exportSubCatButton.onclick = () => {

																	var formattedTable = table.cloneNode(true);

																		for (var i = 0; i < formattedTable.rows.length; i++){
																			if(this.selectable){
																				formattedTable.rows[i].deleteCell(0);
																			}
																			formattedTable.rows[i].deleteCell(-1);
																		}



																		var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
																		var textRange; var j=0;

																		for(j = 0 ; j < formattedTable.rows.length ; j++)
																		{
																				tab_text=tab_text+formattedTable.rows[j].innerHTML+"</tr>";

																		}

																		tab_text=tab_text+"</table>";
																	//	tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
																		//tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
																		//tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

																		var ua = window.navigator.userAgent;
																		var msie = ua.indexOf("MSIE ");

																		if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
																		{
																				txtArea1.document.open("txt/html","replace");
																				txtArea1.document.write(tab_text);
																				txtArea1.document.close();
																				txtArea1.focus();
																				sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
																		}
																		else                 //other browser not tested on IE 11
																				var sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

																		return (sa);

				};
				// table search
			}

			// Create search field
			//var searchContainer = document.querySelector('#search-table');

			var searchContainer;

			if(addCatButtonPath){
				searchContainer = document.querySelector('#search-table');
			}
			else{
				searchContainer = targetID.parentElement;
			}

			var searchBlock = document.createElement('div');
			searchBlock.classList.add("input-field", "search-field", "flex-order-2", "flex-grow");
			var search = document.createElement('input');
			search.setAttribute('type', 'search');
			search.setAttribute('placeholder', 'Поиск');
			searchBlock.append(search);
			searchContainer.prepend(searchBlock);

			search.onkeyup = (event) => {
				this.searchQuery = event.target.value.toUpperCase();
				this.searchTable(this.searchQuery);
			}

		//	this.searchTable(this.searchQuery);

			this.targetID.append(component);
			let tabs = component.querySelector(".tabs");
			let instance = M.Tabs.init(tabs, {});
			var menus = document.querySelectorAll('.dropdown-trigger');
			var menusinstances = M.Dropdown.init(menus, {'alignment': 'right', 'constrainWidth': false});

		 } // end if data
		else{
		//	component.prepend(addCatButton);
		}




 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	}
	insertNewRow(table, dataObj, id){

		let newObjKey = Object.keys(dataObj);

		let obj = dataObj[newObjKey[0]];
		console.log(obj)

		 let newRow = document.createElement("tr");
		 let catid = obj.cat_id;
		let parentId = obj.parent_id;

		// update instance
		console.log(this.data.page_data[parentId])
		this.data.page_data[parentId].sub_cats[catid] = obj;

		var i = 0;

		if(this.selectable){
			this.createSelectableCell(newRow, catid, this.data.page_data[parentId].sub_cats[catid]);
		}

		for(let item in obj.data){
			console.log(obj.data[item]);
			let newCell = newRow.insertCell();
			if(this.data.page_data[parentId].columns[i].type == "currency"){
				newCell.textContent = new Intl.NumberFormat('ru-RU', { style: 'currency', currency: 'UAH' }).format(obj.data[item])
			}
			else{
				newCell.textContent = obj.data[item];
			}
		i++;
		}

		this.createActionsCell(newRow, this.data.page_data[parentId].sub_cats[catid], obj.data.name, this.data.page_data[parentId]);
	  table.tBodies[0].prepend(newRow);

	}
	createSelectableCell(row, id, rowData){
		let newCell = row.insertCell();
		let checkbox = document.createElement("input");
		checkbox.classList.add("filled-in");
		let label = document.createElement("label");
		let span = document.createElement("span");
		checkbox.setAttribute("type", "checkbox");
		label.append(checkbox);
		label.append(span);
		newCell.append(label);
		newCell.classList.add("actions-cell");
		checkbox.onclick = (event) => {
				var idx = this.selectedIds.indexOf(id);

				if(event.target.checked){
					this.selectedIds.push(id);
					this.selected[rowData.cat_id] = rowData;
				}

				else{
					this.selectedIds.splice(idx, 1);
					delete this.selected[rowData.cat_id];
				}
				label.closest('tr').classList.toggle("selected");

				//console.log(this.selected);

		};
	}
	createActionsCell(row, dataItem, name, parent){
		let id = dataItem.cat_id;
    let newCell = row.insertCell();
    let editButton = this.createButton('edit', this.data.params.updateTooltipText);
    let deleteButton = this.createButton('delete', this.data.params.deleteTooltipText);
    newCell.append(editButton);
    newCell.append(deleteButton);
    newCell.classList.add("actions-cell");
    editButton.onclick = () => {this.editRow(editButton.closest("tr"), dataItem, parent)};
    deleteButton.onclick = () => {this.deleteRow(dataItem, row)};
  }
  createButton(icon, tooltipText){
    let newButton = document.createElement("button");
    newButton.classList.add("waves-effect", "waves-teal", "btn-flat", "tooltipped", "icon-button");
    newButton.innerHTML = '<i class="material-icons">' + icon + '</i>';
		newButton.setAttribute("data-position", "bottom");
		newButton.setAttribute("data-tooltip", tooltipText);
    return newButton;
  }
	createMenuItem(text){
		let newItem = document.createElement("li");
		let newItemLink = document.createElement("a");
		newItemLink.setAttribute("href", "#!");
		newItemLink.textContent = text;
		newItem.append(newItemLink);
		return newItem;
	}
	createFileButton(text){
		let newItem = document.createElement("li");
		let newItemLink = document.createElement("a");
		newItemLink.setAttribute("href", "#!");
    let newInput = document.createElement("input");
		newInput.setAttribute("type", "file");
		newInput.setAttribute("accept", ".xls, .xlsx");
		newInput.setAttribute("style", "display: none");
		let newButtonLabel = document.createElement("label");
		newButtonLabel.textContent = text;
		newButtonLabel.append(newInput);
		newItemLink.append(newButtonLabel);
		newItem.append(newItemLink);
    return newItem;
  }
	createTextButton(text, isPrimary){
		let newButton = document.createElement("button");
		newButton.classList.add("waves-effect", "waves-teal");
		if(isPrimary){
			newButton.classList.add("btn");
		}
		else{
			newButton.classList.add("btn-flat");
		}
		newButton.textContent = text;
	  return newButton;
	}
	editRow(row, dataItem, parent){
		let id = dataItem.cat_id;
		let keys = Object.keys(dataItem.data)
		var that = this;
		let parentTable = row.closest('table');
		let tableHeadCells = parentTable.rows[0];
		let editRowElem = document.createElement("tr");

		row.style.display = "none";

		var i = 0;
		if(this.selectable){
			editRowElem.insertCell();
			i = 1;
		}

		var count = 0;
		for(i; i < row.cells.length - 1; i++){
			let newCell = document.createElement("td");
			editRowElem.append(newCell);
			this.createInputs(dataItem, parent.columns[count], newCell, count);
		  count++;
		}

		let newCell = editRowElem.insertCell();
		let cancelButton = this.createButton('clear', this.data.params.dialogCancelButtonText);
		let saveButton = this.createButton('done', this.data.params.dialogApplyButtonText);
		newCell.append(cancelButton);
		newCell.append(saveButton);

		row.after(editRowElem);
		M.Tooltip.init(document.querySelectorAll('.tooltipped'), { exitDelay:0, enterDelay: 500});

		cancelButton.onclick = () => {
			editRowElem.remove();
			row.removeAttribute("style");
		};

		var i = 0;
		var dataCount = 0;
		if(this.selectable){
			i = 1;
		}

		var formData = new FormData;

		saveButton.onclick = () => {
			formData.append('action', this.data.params.updateFunctionName);
			formData.append('cat_id', id);
			for(i; i < editRowElem.cells.length - 1; i++){
				row.cells[i].innerHTML = editRowElem.cells[i].querySelector("input").value;
				formData.append(dataCount,row.cells[i].textContent);
				dataCount++;
			}
			this.sendData(formData, this.data.params.updateToastSuccessText, this.data.params.updateToastFailText, () => {
				//this.reloadData();
				var i = 0;
				var j = 0;
				if(this.selectable){
					i = 1;
				}
				var dataObjkeys = Object.keys(this.data.page_data[parent.cat_id].sub_cats[id].data);
				for(i; i < row.cells.length - 1; i++){

					//console.log(this.data.page_data[parent.cat_id].columns[i].type)
					// if(data[i].columns[count].type == "currency"){
					// 	newCell.textContent = new Intl.NumberFormat('ru-RU', { style: 'currency', currency: 'UAH' }).format(currentItem)
					// }
					// else{
					// 	newCell.setAttribute("search-index", "");
					// 	newCell.textContent = currentItem;
					// }

					if(this.data.page_data[parent.cat_id].columns[i].type == "currency"){
					  row.cells[i].textContent = new Intl.NumberFormat('ru-RU', { style: 'currency', currency: 'UAH' }).format(formData.get([j]))
					}
					else{
						row.cells[i].textContent = formData.get([j]);
					}

					// update instance
					this.data.page_data[parent.cat_id].sub_cats[id].data[dataObjkeys[j]] = formData.get([j]);
					j++;

				}

				editRowElem.remove();
				row.removeAttribute("style");
			});
		};


	}
	deleteRow(dataItem, row){
		this.createConfirmDialog(
			this.data.params.deleteDialogTitle,  // title
			dataItem.data.name	,       // content
			this.data.params.deleteFunctionName, // actionName
			this.data.params.deleteToastSuccessText,		// toastSuccessText
			this.data.params.deleteToastFailText,				// toastFailText
			this.data.params.deleteDialogButton, // confirmButtonText
			dataItem.cat_id,													// id
			() => {row.remove()}
		);
	}
	createDialogInputs(array, selected){



		let content = document.createElement("div");
		array.forEach((element, index) => {

			let inputWrap = document.createElement('div');
			inputWrap.classList.add("input-field");
			let input = document.createElement('input');
			if(element.value){
				input.value = element.value;
			}
			let inputLabel = document.createElement('label');
			inputLabel.textContent = element.name;


			if( element.type == "currency"){
				input.setAttribute("type", "number");
			}
			else{
				input.setAttribute("type", element.type);
			}
			if( element.required){
				inputLabel.textContent += " *";
				input.setAttribute("required", "required");
			}
			inputWrap.append(inputLabel);
			inputWrap.append(input);
			content.append(inputWrap);

			if(index == 0){
				setTimeout(() =>{	input.focus() });
			}
		})

		return content;
	}
	createInputs(dataItem, columnData, target, index){

				let inputWrap = document.createElement('div');
				inputWrap.classList.add("input-field");
				let input = document.createElement('input');
				let inputLabel = document.createElement('label');
				inputLabel.textContent = columnData.name;

				if(dataItem.data[columnData.field_name]){
					inputLabel.classList.add("active");
					input.value = dataItem.data[columnData.field_name];
				}

					// if(i == 0){
					// 	setTimeout(() =>{	input.focus() });
					// }




					if( columnData.type == "currency"){
						input.setAttribute("type", "number");
					}
					else{
						input.setAttribute("type", columnData.type);
					}
					if( columnData.required){
						inputLabel.textContent += " *";
						//input.setAttribute("placeholder", columnData.name + " *");
						input.setAttribute("required", "required");
					}
					else{
						//input.setAttribute("placeholder", columnData.name);
					}
					inputWrap.append(inputLabel);


				inputWrap.append(input);
				//inputWrap.append(inputLabel);



					target.append(inputWrap);


				if(index == 0){
					setTimeout(() =>{	input.focus() });
				}


	}
	createConfirmDialog(title, content, actionName, toastSuccessText, toastFailText, confirmButtonText, id, callback){

		var dialog = document.createElement('form');
		dialog.classList.add("modal");

		let formData = new FormData();

		var dialogContent = document.createElement('div');
		dialogContent.classList.add("modal-content");
		dialogContent.innerHTML = '<h4>' + title + '</h4>';
		dialogContent.append(content);
		let dialogActions = document.createElement('div');
		dialogActions.classList.add("modal-footer");

		let dialogCancelButton = this.createTextButton(this.data.params.dialogCancelButtonText);
		dialogActions.append(dialogCancelButton);

		let dialogApplyButton = this.createTextButton(confirmButtonText);
		dialogActions.append(dialogApplyButton);

		dialog.append(dialogContent);
		dialog.append(dialogActions);



		document.body.append(dialog);
		var dialogInstance = M.Modal.init(dialog, {dismissible:true});
		dialogInstance.open();

		dialogCancelButton.onclick = () => {dialogInstance.close();dialog.remove()};
		dialogApplyButton.onclick = () => {

			formData.append('postId', id);
			formData.append('action', actionName);
			event.preventDefault();
			this.sendData(formData, toastSuccessText, toastFailText, callback);
			dialogInstance.close();
			dialog.remove();
		};
	}
	createDialog(title, content, actionName, toastSuccessText, toastFailText, confirmButtonText, id, callback){

		var dialog = document.createElement('form');
		dialog.classList.add("modal");

		let formData = new FormData();

		var dialogContent = document.createElement('div');
		dialogContent.classList.add("modal-content");
		dialogContent.innerHTML = '<h4>' + title + '</h4>';

		if(typeof content === "object"){

			dialogContent.append(this.createDialogInputs(content));
		}
		else{
			dialogContent.append(content);
		}

    let dialogActions = document.createElement('div');
		dialogActions.classList.add("modal-footer");

		let dialogCancelButton = this.createTextButton(this.data.params.dialogCancelButtonText);
		dialogActions.append(dialogCancelButton);

		let dialogApplyButton = this.createTextButton(confirmButtonText);
		dialogActions.append(dialogApplyButton);

		dialog.append(dialogContent);
		dialog.append(dialogActions);



		document.body.append(dialog);
		var dialogInstance = M.Modal.init(dialog, {dismissible:true});
		dialogInstance.open();

		dialogCancelButton.onclick = () => {dialogInstance.close();dialog.remove()};

		var modalInputs = dialogContent.querySelectorAll('input');

		let checkInputsArray = [];

		dialogApplyButton.onclick = () => {

			let fieldsData = {};
			formData.append('postId', id);
			formData.append('action', actionName);

			modalInputs.forEach(function (value, i) {
				formData.append([i], modalInputs[i].value);
					fieldsData[i] = modalInputs[i].value;
					if (modalInputs[i].validity.valid) {
						checkInputsArray.push(true);
					}
					else{
						checkInputsArray.push(false);
					}
				}
			);

			let checker = arr => arr.every(Boolean);

			if(checker(checkInputsArray)){
				event.preventDefault();

				this.sendData(formData, toastSuccessText, toastFailText, (data) => {callback(data)});
				dialogInstance.close();
				dialog.remove();
			}
			else{
				console.log("Не все значения валидны");
			}

		};
	}
}
