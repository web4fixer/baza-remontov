'use strict';



class TableFilter{
	constructor(data, targetID, tableTargetID) {
		window.tableFilterActive = '';
		this.targetID = targetID;
		this.tableTargetID = tableTargetID;
		var tableFilterData;
		this.data = data;
		this.renderHtml(this.data);

	}
	// END OF CONSTRUCTOR
	filterData(status){
		var filteredObj = {};


		filteredObj['page_data'] = [];
		filteredObj['params'] = this.data.params;

		if(status === "Все"){
			filteredObj['page_data'] = this.data.page_data;
		}
		else{
			for (let key in this.data.page_data) {
				if(this.data.page_data[key]['status'] == status){
					filteredObj['page_data'].push(this.data.page_data[key]);
				}
			}
		}
		table.renderHtml(filteredObj, this.tableTargetID);
	}
	createFilterItems(name, count, selector){
		let button = document.createElement('button');
		button.classList.add('chip');
		button.setAttribute('cat-name', name);
		button.innerHTML =  name + ' ' + '(' + count + ')';
		selector.appendChild(button);
	}
	renderHtml(data){
			if(this.data){
			this.data = data;

			document.querySelector(this.targetID).innerHTML = '';
			var targetPlace = document.querySelector(this.targetID);
			var catKeys = Object.keys(data.params.categories);
			var catValues = Object.values(data.params.categories);

			this.createFilterItems(['Все'], data.page_data.length, document.querySelector(this.targetID));

			for(let i in catKeys){
				this.createFilterItems(catKeys[i], catValues[i], targetPlace);
			}

			let filterBlock = document.querySelector(this.targetID);
			let filterButtons = filterBlock.querySelectorAll("button");
			var that = this;

			filterButtons.forEach(function(item, i, arr) {
				item.onclick = (event) => {
					window.tableFilterActive = event.target.getAttribute('cat-name');
					that.filterData(event.target.getAttribute('cat-name'));
					var activeFilterButton = document.querySelector( that.targetID + ' button.active');
					activeFilterButton.classList.remove("active");
					event.target.classList.add("active");
				}
			});

			if(window.tableFilterActive){
				document.querySelector(this.targetID + ' button[cat-name="'+ window.tableFilterActive +'"]').classList.add('active');
				this.filterData(window.tableFilterActive);
			}
			else{
				document.querySelector(this.targetID + ' button:first-child').classList.add('active');
			}
		}
	}
}







class Datatable{
	constructor(data, targetID, tableFilter, counter) {
		this.data = data;
		this.targetID = targetID;
		this.tableFilter = tableFilter;
		this.servicesParent = '#services-list';
		this.selectedServices = {};
		this.currentSidenav = {};
		this.searchQuery = "";
		this.ids = this.data.params.ids;

			this.renderHtml(data, targetID, counter);

		var elems = document.querySelectorAll('select');
		var instances = M.FormSelect.init(elems, {constrainWidth: false});

		// Create search field
		var searchBlock = document.createElement('div');
		searchBlock.classList.add("input-field");
		var search = document.createElement('input');
		search.setAttribute('type', 'search');
		search.setAttribute('placeholder', 'Поиск');
		searchBlock.append(search);
		document.querySelector("#search-table").innerHTML = "";
		document.querySelector("#search-table").append(searchBlock);

		search.onkeyup = (event) => {
			this.searchQuery = event.target.value.toUpperCase();
			this.searchTable(this.searchQuery);
		}
}
  // Methods
	renderHtml(data, targetID, counter){
		console.log(data)
		document.querySelector(this.targetID).innerHTML = '';
		var columnsCount = 0;
		var data = data['page_data'];
		var table = document.createElement('table');

		this.table = table;

		let tableHeader = table.createTHead();
		tableHeader.classList.add("hide-on-small-only");
		let tableHeaderRow = tableHeader.insertRow();
		var tableBody = table.createTBody();
		table.classList.add('datatable-table', 'highlight', 'sortable');

		this.data.params.columns.forEach(function (value, i) {
			let th = document.createElement('th');
			let span = document.createElement('span');
			span.textContent = value;
			th.appendChild(span);
			tableHeaderRow.appendChild(th);
			columnsCount++;
		});

		if(data.length > 0){
			var that = this
			for(let i in data){
				let newRow = tableBody.insertRow();
				if(this.data.params.pageTemplate == 'users'){
					newRow.innerHTML = `
															<td class="actions-cell">
																<figure class="user-avatar">
																	<img class="user-avatar" src="${ data[i]["avatar"] }" width="48" height="48" alt="${ data[i]["name"] }">
																</figure>
															</td>
															<td search-index>
																<a href="${data[i]["term_url"]}" class="text-underline">${ data[i]["name"] }</a>
															</td>
															<td>
																<span>${ data[i]["type"] }</span>
															</td>
															<td>
																<a href="mailto:${ data[i]["mail"] ? data[i]["mail"] : ''}">${ data[i]["mail"] }</a>
															</td>
															<td>
																<a href="tel:${data[i]["tel"]}">${data[i]["tel"] ? this.phoneNumberFormatter(data[i]["tel"]) : '' }</a>
															</td>
															<td>
																<span>${this.dateFormatter(data[i]["registered"], false) }</span>
															</td>
															<td class="actions-cell">
																<button type="button" onClick="table.editRow( ${data[i]["user_id"]})" class="icon-button"><i class="material-icons">edit</i></button>
																<button type="button" onClick="table.deleteRow( ${data[i]["user_id"]}, '${data[i]["fio"]}')" class="icon-button"><i class="material-icons">delete</i></button>
															</td>`;

				}

				if(this.data.params.pageTemplate == 'my_docs'){
					newRow.innerHTML = `

															<td search-index>
																<span onClick="table.viewDetails(${data[i]["ID"]})" class="view-details-link light-blue-text text-accent-4 pointer">${ data[i]["post_title"] }</span>
															</td>
															<td>
																<span>${ data[i]["creator"] }</span>
															</td>
															<td>
																<span>${this.dateFormatter(data[i]["post_modified"], true) }</span>
															</td>
															<td>
																<span>${this.dateFormatter(data[i]["post_date"], true) }</span>
															</td>
															<td class="actions-cell">
																<button type="button" onClick="table.editRow( ${data[i]["ID"]})" class="icon-button"><i class="material-icons">edit</i></button>
																<button type="button" onClick="table.deleteRow( ${data[i]["ID"]}, '${data[i]["post_title"]}')" class="icon-button"><i class="material-icons">delete</i></button>
															</td>`;

				}

				if(this.data.params.pageTemplate == 'clients'){
					newRow.innerHTML = `

															<td search-index>
																<a href="${data[i]["term_url"]}" class="text-underline">${ data[i]["name"] }</a>
															</td>
															<td>
																<span>${ data[i]["posts_number"] }</span>
															</td>
															<td>
																<a href="tel:${data[i]["tel"]}">${data[i]["tel"] ? this.phoneNumberFormatter(data[i]["tel"]) : '' }</a>
															</td>
															<!-- <td>
																<a href="mailto:${ data[i]["mail"] ? data[i]["mail"] : ''}">${ data[i]["mail"] }</a>
															</td> -->
															<td>
																<span>${ data[i]["source"] }</span>
															</td>
															<td class="actions-cell">
																<button type="button" onClick="table.editRow( ${data[i]["client_id"]})" class="icon-button"><i class="material-icons">edit</i></button>
																<button type="button" onClick="table.deleteRow( ${data[i]["client_id"]}, '${data[i]["name"]}')" class="icon-button"><i class="material-icons">delete</i></button>
															</td>`;

				}


				if(this.data.params.pageTemplate == 'orders'){

					newRow.innerHTML = `
															<td class="actions-cell hide-on-small-only">
																<span>${ data[i]["order_number"] }</span>

															</td>
															<td class="user-cell actions-cell hide-on-small-only" data-sort="${data[i]["master"]}" type-attr="${data[i]["master"]}">
																${data[i]["master"] ? '<a href=' + this.data.params.masters[data[i]["master"]][2] + '><img class="rounded tooltipped" width="32" height="32" alt="'+ data[i]["master"] +'" data-position="top" data-tooltip="'+ data[i]["master"] +'" src="'+ this.data.params.masters[data[i]["master"]][1] +'" /></a>' : "<i class='material-icons avatar-icon'>account_circle</i>"}
															</td>
															<td search-index>
																<span onclick='table.viewDetails(${data[i]["ID"]})'  onClick="that.viewDetails(${ data[i]["ID"]})" class="view-details-link light-blue-text text-accent-4 pointer tooltipped" data-position="top" data-tooltip="${data[i]["serial_number"]}">${data[i]["device_name"]}</span>
																<div class="flex-row mt-half">
																	<span class="cell-details-icon flex-row">
																		<i class="material-icons small-icon mr-half">chat_bubble_outline</i>
																		<span class="grey-text">${ data[i]["comments"] }</span>
																	</span>
																	<span class="cell-details-icon flex-row">
																		<i class="material-icons small-icon mr-half">build_circle</i>
																		<span class="grey-text">${ data[i]["services"].length }</span>
																	</span>
																	<span class="cell-details-icon flex-row">
																		<i class="material-icons small-icon mr-half">grid_view</i>
																		<span class="grey-text">${ data[i]["spares"].length }</span>
																	</span>
																	<span class="cell-details-icon flex-row">
																		${data[i]["urgent_remont"] == 'on' ? '<i class="material-icons small-icon deep-orange-text text-accent-3">local_fire_department</i>' : ''}
																	</span>
																</div>
															</td>
															<td class="status-cell" type-attr="${data[i]["status_id"]}">
																<select onchange="table.changeStatus(this.options[this.selectedIndex].value, ${data[i]["ID"]})">
																	${this.getSelectOptions(data[i]["status"],
																													Object.keys(this.data['params']['all_categories']),
																													Object.keys(this.data['params']['all_categories']),
																													'')
																												}
																</select>
															</td>
															<td class="hide-on-small-only">
																<div>${ this.dateFormatter(data[i]["remont_ready_date"], false) }</div>

															</td>
															<td class="hide-on-small-only">
																<span>${ data[i]["device_trouble_client_says"] }</span>
															</td>

															<td class="actions-cell text-right hide-on-small-only">
																<span>${ new Intl.NumberFormat('ru-RU', { style: 'currency', currency: 'UAH' }).format(data[i]["price"]) }</span>
															</td>
															<td class="hide-on-small-only">
																<div>${ data[i]["creator"] }</div>
																<div>${ this.dateFormatter(data[i]["creation_date"], true) }</div>
															</td>
															<td class="hide-on-small-only">
																${ this.dateFormatter(data[i]["edit_date"], true) }
															</td>
															<td class="actions-cell">
																<button data-target="menu-${data[i]["ID"]}" class="dropdown-menu dropdown-trigger waves-effect icon-button" alignment="right" constrainWidth="false">
																	<i class="material-icons">more_vert</i>
																</button>
																<ul id="menu-${data[i]["ID"]}" class="dropdown-content">
																	<li class="edit_comment_button" onClick="table.editRow( ${data[i]["ID"]})"><a href="#">Редактировать</a></li>
																	<li class="delete_comment_button" onClick="table.deleteRow( ${data[i]["ID"]}, '${data[i]["device_name"]}')"><a href="#" >Удалить</a></li>
																</ul>

															</td>`;
				}
			}
			this.searchTable(this.searchQuery);

			document.querySelector(targetID).append(table);

			var selectInstances = M.FormSelect.init(document.querySelectorAll('select'), {constrainWidth: false});
			var tooltipInstances = M.Tooltip.init(document.querySelectorAll('.tooltipped'));
	    var menuInstances = M.Dropdown.init(document.querySelectorAll('.dropdown-trigger'), {constrainWidth: false});

						//document.addEventListener("DOMContentLoaded", function(event) {
							if(counter){

								counter.textContent = '(' + data.length + ')';
						}
			  		//});


		}

		else{
			var emptyStateWrapper = document.createElement("section");
			emptyStateWrapper.classList.add("empty-state", "flex-column", "flex-justify-center", "flex-align-center", "flex-grow");

			var emptyStateText = document.createElement("h6");
			emptyStateText.classList.add("empty-state-title");
			emptyStateText.textContent = this.data.params['empty_text'];

			var emptyStateIcon = document.createElement("div");
			emptyStateIcon.innerHTML += '<svg width="167px" height="101px" viewBox="0 0 167 101" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="NoDocuments" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Group" transform="translate(0.000000, 0.000000)"><path d="M160,15 C163.865993,15 167,18.1340068 167,22 C167,25.8659932 163.865993,29 160,29 L120,29 C123.865993,29 127,32.1340068 127,36 C127,39.8659932 123.865993,43 120,43 L142,43 C145.865993,43 149,46.1340068 149,50 C149,53.8659932 145.865993,57 142,57 L131.826087,57 C126.951574,57 123,60.1340068 123,64 C123,66.5773288 125,68.9106622 129,71 C132.865993,71 136,74.1340068 136,78 C136,81.8659932 132.865993,85 129,85 L46,85 C42.1340068,85 39,81.8659932 39,78 C39,74.1340068 42.1340068,71 46,71 L7,71 C3.13400675,71 0,67.8659932 0,64 C0,60.1340068 3.13400675,57 7,57 L47,57 C50.8659932,57 54,53.8659932 54,50 C54,46.1340068 50.8659932,43 47,43 L22,43 C18.1340068,43 15,39.8659932 15,36 C15,32.1340068 18.1340068,29 22,29 L62,29 C58.1340068,29 55,25.8659932 55,22 C55,18.1340068 58.1340068,15 62,15 L160,15 Z M160,43 C163.865993,43 167,46.1340068 167,50 C167,53.8659932 163.865993,57 160,57 C156.134007,57 153,53.8659932 153,50 C153,46.1340068 156.134007,43 160,43 Z" id="Background" fill="#F3F7FF"></path><path d="M111.708558,17.671844 L112.673157,86.1424997 L112.673157,92.9999216 C112.673157,95.2090606 110.882296,96.9999216 108.673157,96.9999216 L49.6731572,96.9999216 C47.4640182,96.9999216 45.6731572,95.2090606 45.6731572,92.9999216 L45.6731572,19.0209641 C45.6731572,17.9163946 46.5685877,17.0209641 47.6731572,17.0209641 C47.6801346,17.0209641 47.687112,17.0209641 47.694089,17.0209641 L52.5826591,17.0722397 M56.535613,17.1136132 L61.1511792,17.161922" id="Shape" stroke="#75A4FE" stroke-width="2.5" fill="#FFFFFF" stroke-linecap="round" transform="translate(79.173157, 57.010443) rotate(-7.000000) translate(-79.173157, -57.010443) "></path><path d="M108.700739,21.5543308 L109.564559,83.6058625 L109.564559,89.8204011 C109.564559,91.8224333 107.960803,93.4454011 105.98247,93.4454011 L53.1466487,93.4454011 C51.1683153,93.4454011 49.5645591,91.8224333 49.5645591,89.8204011 L49.5645591,22.9454011 C49.5645591,21.8408316 50.4599896,20.9454011 51.5645591,20.9454011 L58.1008814,20.9454011 L58.1008814,20.9454011" id="Rectangle" fill="#E8F0FE" transform="translate(79.564559, 57.195401) rotate(-7.000000) translate(-79.564559, -57.195401) "></path><path d="M109.228636,1.25 C109.957687,1.25 110.656902,1.53949778 111.172578,2.05485483 L111.172578,2.05485483 L124.615817,15.4897778 C125.13191,16.0055517 125.421875,16.7052828 125.421875,17.434923 L125.421875,17.434923 L125.421875,80 C125.421875,80.7593915 125.114071,81.4468915 124.616419,81.9445436 C124.118767,82.4421958 123.431267,82.75 122.671875,82.75 L122.671875,82.75 L63.671875,82.75 C62.9124835,82.75 62.2249835,82.4421958 61.7273314,81.9445436 C61.2296792,81.4468915 60.921875,80.7593915 60.921875,80 L60.921875,80 L60.921875,4 C60.921875,3.24060847 61.2296792,2.55310847 61.7273314,2.05545635 C62.2249835,1.55780423 62.9124835,1.25 63.671875,1.25 L63.671875,1.25 Z" id="Rectangle" stroke="#75A4FE" stroke-width="2.5" fill="#FFFFFF"></path><path d="M109.671875,2.40283203 L109.671875,14 C109.671875,15.6568542 111.015021,17 112.671875,17 L120.605469,17" id="Shape" stroke="#75A4FE" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"></path><path d="M71.671875,17 L97.671875,17 M71.671875,29 L114.671875,29 M71.671875,42 L114.671875,42 M71.671875,55 L114.671875,55 M71.671875,68 L97.671875,68" id="lines" stroke="#A4C3FE" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"></path></g></g></svg>';

			emptyStateWrapper.append(emptyStateIcon);
			emptyStateWrapper.append(emptyStateText);
			document.querySelector(targetID).append(emptyStateWrapper);
		}


		// if there is a search query - filter the table


	}

	searchTable(searchQuery){
		var tr, td, i, txtValue;
		tr = this.table.getElementsByTagName("tr");
		// Loop through all table rows, and hide those who don't match the search query
		for (i = 1; i < tr.length; i++) {
			td = tr[i].querySelector("td[search-index]")
			if (td) {
				txtValue = td.textContent || td.innerText;
				if (txtValue.toUpperCase().indexOf(searchQuery) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
			}
		}
	}

	addUser(){
		let data = {};
		let sidepageContent = document.createElement("div");
		sidepageContent.innerHTML = this.getFormTemplate(data).trim();

		this.createSidenav(
			this.data.params.addDialogTitle,
			'',
			sidepageContent,
			'100%',
			this.data.params.dialogCancelButtonText,
			this.data.params.dialogApplyButtonText,
			(elem) => {
				this.saveFunction(
					document.forms.data_form,
					0,
					this.data.params.addToastSuccessText,
					this.data.params.addToastFailText,
					this.data.params.saveFunctionName,
					elem
			)},
			this.data.params.formId
		);
		var elems = document.querySelectorAll('select');
		M.updateTextFields();
		var instances = M.FormSelect.init(elems, {constrainWidth: false});


	}
	addDoc(){
		let data = {};
		let sidepageContent = document.createElement("div");
		sidepageContent.innerHTML = this.getFormTemplate(data).trim();
		this.createSidenav(
			this.data.params.addDialogTitle,
			'',
			sidepageContent,
			'100%',
			this.data.params.dialogCancelButtonText,
			this.data.params.dialogApplyButtonText,
			(elem) => {
				this.saveFunction(
					document.forms.data_form,
					0,
					this.data.params.addToastSuccessText,
					this.data.params.addToastFailText,
					this.data.params.saveFunctionName,
					elem
			)},
			this.data.params.formId
		)

		this.wpContentEditorInit();
	}
	wpContentEditorInit(){

		if(wp.editor){
			wp.editor.remove('post-editor');
			wp.editor.initialize( 'post-editor', {
				tinymce: {
					wpautop  : true,
					theme    : 'modern',
					skin     : 'lightgray',
					language : 'ru',
					formats  : {
						alignleft  : [
							{ selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li', styles: { textAlign: 'left' } },
							{ selector: 'img,table,dl.wp-caption', classes: 'alignleft' }
						],
						aligncenter: [
							{ selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li', styles: { textAlign: 'center' } },
							{ selector: 'img,table,dl.wp-caption', classes: 'aligncenter' }
						],
						alignright : [
							{ selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li', styles: { textAlign: 'right' } },
							{ selector: 'img,table,dl.wp-caption', classes: 'alignright' }
						],
						strikethrough: { inline: 'del' }
					},
					relative_urls       : false,
					remove_script_host  : false,
					convert_urls        : false,
					browser_spellcheck  : true,
					fix_list_elements   : true,
					entities            : '38,amp,60,lt,62,gt',
					entity_encoding     : 'raw',
					keep_styles         : false,
					paste_webkit_styles : 'font-weight font-style color',
					preview_styles      : 'font-family font-size font-weight font-style text-decoration text-transform',
					tabfocus_elements   : ':prev,:next',
					plugins    : 'charmap,hr,media,paste,tabfocus,textcolor,fullscreen,wordpress,wpeditimage,wpgallery,wplink,wpdialogs,wpview',
					resize     : 'vertical',
					menubar    : false,
					indent     : false,
					toolbar1   : 'bold,italic,strikethrough,bullist,numlist,blockquote,hr,alignleft,aligncenter,alignright,link,unlink,wp_more,spellchecker,fullscreen,wp_adv',
					toolbar2   : 'formatselect,underline,alignjustify,forecolor,pastetext,removeformat,charmap,outdent,indent,undo,redo,wp_help',
					toolbar3   : '',
					toolbar4   : '',
					body_class : 'id post-type-post post-status-publish post-format-standard',
					wpeditimage_disable_captions: false,
					wpeditimage_html5_captions  : true

				},
				quicktags   : true,
				mediaButtons: false

			} )
		}


	}
	addOrder(){

		let data = {};
		let sidepageContent = document.createElement("div");
		sidepageContent.innerHTML = this.getFormTemplate(data).trim();
		this.createSidenav(
			this.data.params.addDialogTitle,
			'',
			sidepageContent,
			'100%',
			this.data.params.dialogCancelButtonText,
			this.data.params.dialogApplyButtonText,
			(elem) => {
				this.saveFunction(
					document.forms.data_form,
					0,
					this.data.params.addToastSuccessText,
					this.data.params.addToastFailText,
					this.data.params.saveFunctionName,
					elem
			)},
			this.data.params.formId
		)


	//	$("[name='client_tel_number']").mask("(999) 999-99-99");
		var elems = document.querySelectorAll('select');
		M.FormSelect.init(elems, {constrainWidth: false});
	  M.updateTextFields();

		let clientsObj = {};
		for (let key in this.data.params.clients) {
			clientsObj[key] = null;
		}

		var device_trouble_client_says = document.querySelector('#device_trouble_client_says');
    var autoCompleteDevice_trouble_client_says = M.Autocomplete.init( device_trouble_client_says, {data: this.data.params.device_trouble_client_says});


		var autoCompleteClients = M.Autocomplete.init( document.querySelector('#fio_clienta'), {data: clientsObj, onAutocomplete: () => {
			document.querySelector('#phone-number').value = this.data.params.clients[document.querySelector('#fio_clienta').value]
		}});

		var dateElems = document.querySelectorAll('.datepicker');

		var dateInstances = M.Datepicker.init(
			dateElems,
			{
				autoClose: true,
				showClearBtn: true,
				container: document.body,
				i18n: calendarLocales,
				firstDay: 1,
				format: 'dd.mm.yyyy'
		});


	}

	addComment(post_id){
		this.saveComment(document.querySelector("#textarea-comment").value,
										 '',
										 post_id,
										 this.data.params.addCommentFunctionName,
										 this.data.params.addCommentToastSuccessText,
										 this.data.params.addCommentToastFailText
									 );
	}
	saveComment(commentText, comment_id, post_id, action, addCommentToastSuccessText, addCommentToastFailText){
		window.activeTab = "comments";
		if(commentText){
			event.preventDefault();
			var formData = new FormData;
			formData.append('comment_text', commentText);
			formData.append('action', action);
			formData.append('comment_id', comment_id);
			formData.append('post_id', post_id);
			this.sendData(formData, addCommentToastSuccessText, addCommentToastFailText,  () => {this.reloadData(); this.updateDetails(post_id)});
		}
	}
	getSelectedServices(){
    let servicesParent = document.querySelector(this.servicesParent);
		let selectedServices = servicesParent.children
		let selectedIds = [];
		for(let item of selectedServices){
				selectedIds.push(parseInt(item.getAttribute("data-attr")));
		}

		return selectedIds;
	}
	getIds(obj){
		let array = [];
		for(let item in obj){
				array.push(parseInt(item));
		}
		return array;
	}
	createServicesList(parent, data){
		var price = 0;
		for(let item in data){

				let element = document.createElement("li");
				let servicesTypeIcon = document.createElement("i");
				servicesTypeIcon.classList.add("material-icons");

				let elementName = document.createElement("span");
				elementName.classList.add("collection-item__name", "flex-grow", "m-left-base")
				let elementValue = document.createElement("strong");
				let removeElementButton = this.createButton('close');
				removeElementButton.classList.add("m-right-base-neg", "m-left-double")

				element.classList.add("collection-item");

				element.setAttribute("data-attr", item);
				element.setAttribute("price-attr", data[item]['data'].price);
				servicesTypeIcon.textContent = data[item].type;

				elementName.textContent = data[item]['data'].name;

				elementValue.textContent = new Intl.NumberFormat('ru-RU', { style: 'currency', currency: 'UAH' }).format(data[item]['data'].price);

				price += parseInt(data[item]['data'].price, 10);

				element.append(servicesTypeIcon);
				element.append(elementName);
				element.append(elementValue);
				element.append(removeElementButton);
				parent.append(element);

				removeElementButton.onclick = (event) => {
					element.remove();
					delete data[event.target.closest("li").getAttribute("data-attr")];
					document.forms.data_form.price.value = document.forms.data_form.price.value - parseInt(event.target.closest("li").getAttribute("price-attr"), 10);
				}
		}
		return price;

	}
	addServicesToForm(servicesParent, catalogServices, catalogSpares){
		servicesParent.innerHTML = '';

		this.catalogServices = catalogServices;
		this.catalogSpares = catalogSpares;

		var total_price = this.createServicesList(servicesParent, this.catalogServices) + this.createServicesList(servicesParent, this.catalogSpares);

		document.forms.data_form.price.value = total_price;

	}
	addServices(){

		// var selectedSpares = this.getSelectedServices();
		// this.selectedSpares = selectedSpares;
		//create tabs
		var customTabs = document.createElement('div');
		customTabs.classList.add("custom-tabs");
		customTabs.innerHTML = `<input type="radio" name="tab-btn" id="tab-btn-1" value="" checked>
														<label for="tab-btn-1">
															<i class="material-icons mr-half">grid_view</i>
															<span>Материалы</span>
														</label>
														<input type="radio" name="tab-btn" id="tab-btn-2" value="">
														<label for="tab-btn-2">
															<i class="material-icons mr-half">build_circle</i>
															<span>Услуги</span>
														</label>
														<div id="content-1" class="row flex-wrap flex-align-center">
															<div id="RepairPartsBlock" class="flex-grow flex-order-3 full-width"></div>
														</div>
														<div id="content-2" class="row flex-wrap">
															<div id="ServicesBlock" class="flex-grow flex-order-3 full-width"></div>
														</div> `;

		var catalogServices, catalogRepairParts;

		// get repair parts
		var formDataRepairParts = new FormData;
		formDataRepairParts.append('action', 'get_repair_parts_ajax');
		formDataRepairParts.append('selected_ids', JSON.stringify(this.currentSpares));
		this.sendData(formDataRepairParts, '', '', (responce) => {
			catalogRepairParts = new EditibleCatalog(responce, customTabs.querySelector('#RepairPartsBlock'), true, this.getSelectedServices());
		})



		// get works
		var formDataWorks = new FormData;
		formDataWorks.append('action', 'get_works_ajax');
		this.sendData(formDataWorks, '', '', (responce) => {
			catalogServices = new EditibleCatalog(responce, customTabs.querySelector('#ServicesBlock'), true, this.getSelectedServices());

		})

		//console.log(this);
		//console.log(this.catalogSpares)

		this.createSidenav(
			"Добавить услуги и материалы",
			'',
			customTabs,
			'100%',
			'Отмена',
			'Применить',
			(elem) => {
				this.addServicesToForm(document.querySelector(this.servicesParent),
				catalogServices.selected,
				catalogRepairParts.selected),
				elem}
		);




	}
	createSidenav(title, name, content, width, closeButtonText, applyButtonText, applyAction, formId){
		// sidepage
		var sidepage = document.createElement('article');
		sidepage.classList.add("sidenav");
		sidepage.setAttribute('id', 'slide-out');
		sidepage.style.width = width;

		// sidepage title
		var sidepageHeader = document.createElement('header');

		var sidepageTitleWrap = document.createElement('div');
		sidepageTitleWrap.classList.add("flex-column", "flex-justify-center");
		var sidepageTitle1 = document.createElement('span');
		var sidepageTitle2 = document.createElement('h5');

		sidepageTitle2.classList.add("sidenav__title");
		sidepageTitle1.classList.add("sidenav__sub-title");
		sidepageTitle1.textContent = title;
		sidepageTitle2.textContent = name;

		sidepageTitleWrap.append(sidepageTitle2);
		sidepageTitleWrap.append(sidepageTitle1);


		sidepageHeader.append(sidepageTitleWrap);

		sidepageHeader.classList.add("sidenav__header");
		let closeSidepageIconButton = this.createButton('close');
		closeSidepageIconButton.onclick = () => {mSidepage.close(); };
		sidepageHeader.append(closeSidepageIconButton);
		sidepage.append(sidepageHeader);

		// sidepage content
		var sidepageContent = document.createElement('section');
		sidepageContent.classList.add("sidenav__content");
		//sidepageContent.innerHTML = content;
		sidepageContent.append(content);
		sidepage.append(sidepageContent);



		// sidepage actions
		var sidepageActions = document.createElement('footer');
		sidepageActions.classList.add("sidenav__actions");

		let closeButton = this.createTextButton(closeButtonText, false);
		closeButton.onclick = () => {
			mSidepage.close();
		};
		sidepageActions.append(closeButton);

		if(applyAction && applyButtonText){

			let applyButton = this.createTextButton(applyButtonText, true, 'label');
			applyButton.setAttribute("for", formId);
			sidepageActions.append(applyButton);

			applyButton.onclick = () => {
				applyAction(mSidepage);
				if(!formId){
					mSidepage.close();
				}
			};
		}



		sidepage.append(sidepageActions);

		// Render
		document.body.append(sidepage);

		var mSidepage = M.Sidenav.init(sidepage, {
				edge: 'right',
				onCloseEnd: () => {
					sidepage.remove();
					mSidepage.destroy();
					this.selectedServices = null;
					this.selected = null;
				}
			}
		);




		let sidepageContentTabs = sidepage.querySelector(".tabs");
		if(sidepageContentTabs){
			let tabs = M.Tabs.init(sidepageContentTabs, {});
			tabs.updateTabIndicator();
		}

		// Open
		mSidepage.open();
	}
	deleteComment(post_id, comment_id, author, date){
		window.activeTab = "comments";
		this.createDialog(
			this.data.params.deleteCommentDialogTitle,  // title
			'',       // content
			'',                              // datatypes
			this.data.params.deleteCommentFunctionName, // actionName
			this.data.params.deleteCommentToastSuccessText,		// toastSuccessText
			this.data.params.deleteCommentToastFailText,				// toastFailText
			this.data.params.dialogDeleteButtonText, // confirmButtonText
			comment_id,														// id
			() => { this.reloadData(); this.updateDetails(post_id) }                              /// Callback
		);
	}
	editComment(elem, post_id, comment_id){
		let comment = elem.closest('article');
		let commentBlock = comment.querySelector('[data-attr="comment_text"]');

		let editCommentBlock = document.createElement('form');
		let editCommentElem = document.createElement('div');

		editCommentElem.classList.add("input-field", "col", "s12");

		let cancelCommentButton = this.createTextButton('Отмена');
		let updateCommentButton = this.createTextButton('Сохранить');
		updateCommentButton.setAttribute("type", "submit");

		let editCommentTextarea = document.createElement('textarea');
		let editCommentTextareaName = 'edit-comment-textarea';
		editCommentTextarea.setAttribute("name", editCommentTextareaName);
		editCommentTextarea.setAttribute("required", "required");
		editCommentTextarea.classList.add("materialize-textarea");
		editCommentTextarea.value = commentBlock.innerText;

		let label = document.createElement('label');
		label.setAttribute("for", editCommentTextareaName);
		label.classList.add("active");
		label.innerText = 'Комментарий';

		editCommentElem.append(editCommentTextarea);
		editCommentElem.append(label);
		commentBlock.append(editCommentBlock);
		commentBlock.firstElementChild.classList.add("hide");

		editCommentBlock.append(editCommentElem);
		editCommentBlock.append(cancelCommentButton);
		editCommentBlock.append(updateCommentButton);
	  setTimeout(() =>{editCommentTextarea.focus(); M.textareaAutoResize(editCommentTextarea)}, 10);


		cancelCommentButton.onclick = () => {commentBlock.firstElementChild.classList.remove("hide"); editCommentBlock.remove()};

		updateCommentButton.onclick = () => {
										this.saveComment(
											 editCommentTextarea.value,
											 comment_id,
											 post_id,
											 this.data.params.updateCommentFunctionName,
											 this.data.params.updateCommentToastSuccessText,
											 this.data.params.updateCommentToastFailText
									 );
		}
	}

	saveFunction(form, id, successToastText, failToastText, funcName, elem){
		form.addEventListener('submit', (event) => {

			event.preventDefault();
			var formData = new FormData;
			formData.append('action', funcName);
			formData.append('post_id', id);
			if(this.catalogServices){
				formData.append('services', JSON.stringify(this.getIds(this.catalogServices)));
			}
			if(this.catalogSpares){
				formData.append('spares', JSON.stringify(this.getIds(this.catalogSpares)));
		  }
			if(form.fileToUpload){
				formData.append('img', form.fileToUpload.files[0]);
			}

			var dataArray = this.serializeArray(form);
			for (var prop in dataArray) {
				formData.append(prop, dataArray[prop]);
			}



			this.sendData(formData, successToastText, failToastText, () => {elem.close(); this.reloadData()});


		});
	}
	reloadData(){
		let formData = new FormData();
		formData.append('action', this.data.params.getDataFunctionName);
		formData.append('ids', this.ids);
		formData.append('from_cats', this.data.params.from_cats_ids);
		this.sendData(formData, '', '', (responce) => {
			this.renderHtml(responce, this.targetID);
			if(this.tableFilter){
				tableFilter.renderHtml(responce);
			}

		});

	}
	changeStatus(status, id){
		var formData = new FormData;
		formData.append('action', this.data.params.сhangeStatusFunctionName);
		formData.append('post_id', id);
		formData.append('status', status);
		this.sendData(formData, this.data.params.сhangeStatusToastSuccessText, this.data.params.сhangeStatusToastFailText, () => {
			this.reloadData();
		});
	}
	changeMaster(master_id, post_id){
		var formData = new FormData;
		formData.append('action', this.data.params.сhangeMasterFunctionName);
		formData.append('post_id', post_id);
		formData.append('master_id', master_id);
		this.sendData(formData, this.data.params.сhangeMasterToastSuccessText, this.data.params.сhangeMasterToastFailText, () => {
			this.reloadData();
		});
	}
	phoneNumberFormatter(data){
		//Filter only numbers from the input
	  let cleaned = ('' + data).replace(/\D/g, '');

	  //Check if the input is of correct
	  let match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{2})(\d{2})$/);

	  if (match) {
	    //Remove the matched extension code
	    //Change this to format for any country code.
	    let intlCode = (match[1] ? '+1 ' : '')
	    return [intlCode, '(', match[2], ') ', match[3], '-', match[4], '-', match[5]].join('')
	  }

	  return null;
	}
	dateFormatter(date, showTime){
		const todayDate = new Date();
		let formattedDate = new Date(date);
		if(date){
			if(formattedDate.getDate() === todayDate.getDate() &&
	       formattedDate.getMonth() === todayDate.getMonth() &&
	       formattedDate.getFullYear() === todayDate.getFullYear()
			 )
			{

 				 if(showTime){
 					 return 'Сегодня, ' + new Intl.DateTimeFormat('ru-RU', { hour:'numeric', minute:'numeric' }).format(new Date(date));
 				 }
 				 else{
 					 return 'Сегодня';
 				 }
 			}
			if(formattedDate.getDate() === todayDate.getDate() - 1 &&
	       formattedDate.getMonth() === todayDate.getMonth() &&
	       formattedDate.getFullYear() === todayDate.getFullYear()
			 ){
				 if(showTime){
 					 return 'Вчера ' + new Intl.DateTimeFormat('ru-RU', { hour:'numeric', minute:'numeric' }).format(new Date(date));
 				 }
 				 else{
 					 return 'Вчера';
 				 }
			 }
			 if(formattedDate.getDate() === todayDate.getDate() + 1 &&
 	       formattedDate.getMonth() === todayDate.getMonth() &&
 	       formattedDate.getFullYear() === todayDate.getFullYear()
 			 ){
 				 if(showTime){
  					 return 'Завтра, ' + new Intl.DateTimeFormat('ru-RU', { hour:'numeric', minute:'numeric' }).format(new Date(date));
  				 }
  				 else{
  					 return 'Завтра';
  				 }
 			 }
 			else{
 				if(showTime){
 				 return new Intl.DateTimeFormat('ru-RU', { year: 'numeric', month: 'long', day: 'numeric', hour:'numeric', minute:'numeric' }).format(new Date(date))
 			 }
 			 else{
 				 return new Intl.DateTimeFormat('ru-RU', { year: 'numeric', month: 'long', day: 'numeric' }).format(new Date(date))
 			 }
 			}
	 	}
		else{
			return "-"
		}
	}
	getSelectOptions(current, all, ids, images){

		var html;
		var showEmpty = true;
		all.forEach(function(item, i, arr) {
				let selected = `${current === item ? 'selected disabled' : ''}`;
				let selectedId = `${current == ids[i][0] ? 'selected disabled' : ''}`;

				var image = `${images ? 'data-icon="' + ids[i][1] + '"' : ''}`;
				var value = `${Array.isArray(ids[i]) ? ids[i][0] : ids[i]}`;
				html += '<option ' + image + ' value="' + value + '" ' + selected + selectedId + '>' + item + '</option>';

				if(current === item || current == ids[i][0]){
					showEmpty = false;
				}
		})
		if(showEmpty){
			html += '<option disabled selected value="">Не выбран</option>';
		}
		return html;
	}
	getSelectValue(current, all){
		var html;
		var showEmpty = true;
		all.forEach(function(item, i, arr) {
			let selected = `${current === item ? 'selected disabled' : ''}`;
			html += '<option value="' + item + '" ' + selected + '>' + item + '</option>';
		});
		return html;
	}

	viewDetails(id){


		window.activeTab = "details";
		var formData = new FormData;
		formData.append('action', this.data.params.getFunctionName);
		formData.append('post_id', id);
		this.sendData(formData, '', '', (responce) => {
			console.log(responce);

			let sidepageContent = document.createElement("div");
			sidepageContent.innerHTML = this.getViewTemplate(responce).trim();

			this.createSidenav(
				this.data.params.getFunctionTitle + ' #' + id,
				responce.post_title,
				sidepageContent,
				'100%',
				'Закрыть',
			)

			//var tabsElems = document.querySelectorAll('.tabs');
		//	var tabsInstance = M.Tabs.init(tabsElems);
		  //var collapsibleElems = document.querySelectorAll('.collapsible');
		 // var collapsibleInstances = M.Collapsible.init(collapsibleElems, {accordion: false});
		//	var menuElems = document.querySelectorAll('.dropdown-trigger');
    //  var menuInstances = M.Dropdown.init(menuElems, {constrainWidth: false});
	})

}
	updateDetails(id) {
		var formData = new FormData;
		formData.append('action', this.data.params.getFunctionName);
		formData.append('post_id', id);
		this.sendData(formData, '', '', (responce) => {
			this.sidepage.updateContent(this.getViewTemplate(responce));
			var tabsElems = document.querySelectorAll('.tabs');
			var tabsInstance = M.Tabs.init(tabsElems);
			var collapsibleElems = document.querySelectorAll('.collapsible');
			var collapsibleInstances = M.Collapsible.init(collapsibleElems, {accordion: false});
			var menuElems = document.querySelectorAll('.dropdown-trigger');
			var menuInstances = M.Dropdown.init(menuElems, {constrainWidth: false});
		})
	}
  createButton(icon){
    let newButton = document.createElement("button");
    newButton.classList.add("waves-effect", "icon-button");
    newButton.innerHTML = '<i class="material-icons">' + icon + '</i>';
    return newButton;
  }
	showLoader(){
		let loader = document.createElement('div');
		let loaderContent = document.createElement('div');
		loader.classList.add("progress");
		loaderContent.classList.add("indeterminate");
		loader.appendChild(loaderContent);
		document.body.prepend(loader);
		return loader;
	}
	createTextButton(text, isPrimary, element){
		var newButton;

		if(element){
			newButton = document.createElement(element);
		}
		else{
			newButton = document.createElement("button");
		}

		newButton.classList.add("waves-effect", "waves-teal");
		if(isPrimary){
			newButton.classList.add("btn");
		}
		else{
			newButton.classList.add("btn-flat");
		}
		newButton.textContent = text;
	  return newButton;
	}
	editRow(id){
		var formData = new FormData;
		formData.append('action', this.data.params.getFunctionName);
		formData.append('post_id', id);
		this.sendData(formData, '', '', (responce) => {

			let sidepageContent = document.createElement("div");
			sidepageContent.classList.add("sidenav__content-wrapper")
			console.log(responce)
			sidepageContent.innerHTML = this.getFormTemplate(responce).trim();

		var editor = document.getElementById('post-editor');

			this.createSidenav(
				this.data.params.updateDialogTitle,
				`${responce.post_title ? responce.post_title : ''}`,
				sidepageContent,
				'100%',
				this.data.params.dialogCancelButtonText,
				this.data.params.dialogApplyButtonText,
				(elem) => {
					this.saveFunction(
						document.forms.data_form,
						id,
						this.data.params.updateToastSuccessText,
						this.data.params.updateToastFailText,
						this.data.params.updateFunctionName,
						elem
					);
					//console.log(wp.editor.getContent('post-editor'))
				},
					this.data.params.formId
				)

			this.wpContentEditorInit();



			if(this.data.params.pageTemplate === 'orders'){
				// make numbers from strings
				let spares = Object.keys(responce.spares);
				this.currentSpares = spares.map(i=>Number(i));
				this.addServicesToForm(document.querySelector(this.servicesParent), responce.services, responce.spares)
			}

		//	$("[name='client_tel_number']").mask("(999) 999-99-99");
			//document.forms.data_form.client_tel_number.mask("(999) 999-99-99");
			var elems = document.querySelectorAll('select');
			M.updateTextFields();
			var instances = M.FormSelect.init(elems, {constrainWidth: false});

			var dateElems = document.querySelectorAll('.datepicker');
			var dateInstances = M.Datepicker.init(dateElems, {
																												autoClose: true,
																												showClearBtn: true,
																												container: document.body,
																												i18n: calendarLocales,
																												firstDay: 1,
																												format: 'dd.mm.yyyy'
			});

		});
	}
	deleteRow(id, name){
		//elem.remove();
		this.createDialog(
			this.data.params.deleteDialogTitle,  // title
			'"' + name + '"',       // content
			'',                              // datatypes
			this.data.params.deleteFunctionName, // actionName
			this.data.params.deleteToastSuccessText,		// toastSuccessText
			this.data.params.deleteToastFailText,				// toastFailText
			this.data.params.dialogDeleteButtonText, // confirmButtonText
			id														// id
		);

	}
	sendData(obj, toastSuccessText, toastFailText, callback){
		// Ajax
		var request = new XMLHttpRequest();
		request.open('POST', MyAjax.ajaxurl, true);
		request.setRequestHeader('accept', 'application/json');
		request.send(obj);
		var loader = this.showLoader();
		// Функция для наблюдения изменения состояния request.readyState обновления statusMessage
		request.onreadystatechange = function () {
			if (request.readyState < 4){
				// <4 =  ожидаем ответ от сервера
				//console.log('test')

			}
			else if (request.readyState === 4) {
				// 4 = Ответ от сервера полностью загружен
				if (request.status == 200 && request.status < 300){
					if(toastSuccessText){
						M.toast({html: toastSuccessText, classes: 'toast-success', displayLength: 3000});
					}
						loader.remove();
						var response = JSON.parse(request.responseText);

						window.response = response;

						if(callback){
							callback(response);
						}
				}
				else{
						if(toastFailText){
							M.toast({html: toastFailText, classes: 'toast-success', displayLength: 3000});
						}
				}
			}
		}
		// end Ajax
	}
	serializeArray(form) {
	    var field, l, s = [];
			var dataObj = {};
	    if (typeof form == 'object' && form.nodeName == "FORM") {
	        var len = form.elements.length;
	        for (var i=0; i<len; i++) {
	            field = form.elements[i];
	            if (field.name && !field.disabled && field.type != 'file' && field.type != 'reset' && field.type != 'submit' && field.type != 'button') {
	                if (field.type == 'select-multiple') {
	                    l = form.elements[i].options.length;
	                    for (j=0; j<l; j++) {
	                        if(field.options[j].selected)
	                            //s[s.length] = { name: field.name, value: field.options[j].value };
															dataObj[field.name] =  field.options[j].value;
	                    }
	                } else if ((field.type != 'checkbox' && field.type != 'radio') || field.checked) {
	                    //s[s.length] = { name: field.name, value: field.value };
											dataObj[field.name] =  field.value;
	                }
	            }
	        }
	    }
	    return dataObj;
	}
	radioButtonHelper(value, currentValue) {
    if ( value == currentValue ) {
       return "checked";
    } else {
       return "";
    }
 };
	createDialog(title, content, dataTypes, actionName, toastSuccessText, toastFailText, confirmButtonText, id, callback){
		var dialog = document.createElement('form');
		dialog.classList.add("modal");

		let formData = new FormData();

		var dialogContent = document.createElement('div');
		dialogContent.classList.add("modal-content");
		dialogContent.innerHTML = '<h4>' + title + '</h4>';
		dialogContent.append(content);

    let dialogActions = document.createElement('div');
		dialogActions.classList.add("modal-footer");

		let dialogCancelButton = this.createTextButton(this.data.params.dialogCancelButtonText);
		dialogActions.append(dialogCancelButton);

		let dialogApplyButton = this.createTextButton(confirmButtonText);
		dialogActions.append(dialogApplyButton);

		dialog.append(dialogContent);
		dialog.append(dialogActions);

		document.body.append(dialog);

		var dialogInstance = M.Modal.init(dialog, {dismissible:true});
		dialogInstance.open();

		dialogCancelButton.onclick = () => {dialogInstance.close();dialog.remove()};

		dialogApplyButton.onclick = () => {

			formData.append('post_id', id);
			formData.append('action', actionName);
			//	debugger;
				//this.sendData(formData, toastSuccessText, toastFailText, () => {this.reloadData()});
				this.sendData(formData, toastSuccessText, toastFailText, () => {
					dialogInstance.close();
					dialog.remove();
					callback();
				});
		};
	}
	getFormTemplate(data){
		var template;



		if(this.data.params.pageTemplate === 'my_docs'){
			template = `
												<form id="data_form" name="data_form" method="post" action="#" enctype="multipart/form-data" class="col s12 flex-column flex-grow">
												<div class="row">
													<div class="input-field col s12 m-b-none">
														<input name="post_title" type="text" value="${data.post_title ? data.post_title : ''}">
														<label for="post_title">Название</label>
													</div>
												</div>
												<div class="row flex-grow">
													<div class="input-field col s12 m-none">
														<textarea name="post_content" id="post-editor">
															${data.post_content ? data.post_content : ''}
														</textarea>
													</div>
												</div>

												<input type="submit" id="data-form" hidden>
											</form>

			`;
		}

		if(this.data.params.pageTemplate === 'users'){
			template = `<div class="row m-none">
												<form id="data_form" name="data_form" method="post" action="#" enctype="multipart/form-data" class="col s12">
													<fieldset>

														<div class="row">

														<div class="col s4">
															<h5 class="sidepage-group-title">Должность</h5>

																<div class="input-field">
																	<select id="user_role" name="user_role">
															      <option value="Администратор" ${data.user_role == 'Администратор' ? 'selected' : ''}>Администратор</option>
															      <option value="Мастер" ${data.user_role == 'Мастер' ? 'selected' : ''}>Мастер</option>
															      <option value="Приемщик" ${data.user_role == 'Приемщик' ? 'selected' : ''}>Приемщик</option>
															    </select>
																</div>
															</div>

															<div class="col s8">
															<h5 class="sidepage-group-title">Зарплата в месяц</h5>

																<div class="input-field">
																	<input name="user_salary" type="number" style="width: 100px" value="${data.user_salary ? data.user_salary : 0}">
															    <span>&nbsp;&nbsp;+&nbsp;&nbsp;</span>
																	<input name="user_salary_percent" type="number" min="0" max="100" value="${data.user_salary_percent ? data.user_salary_percent : 0}" style="width: 80px"> %
																	<span>&nbsp;&nbsp;от заказа</span>
																</div>
															</div>

														</div>
													</fieldset>


												<fieldset>
												<h5 class="sidepage-group-title">Личные данные</h5>

													<div class="row">
														<div class="input-field col s4">
															<input name="user_name" type="text" value="${data.name ? data.name : ''}">
															<label for="user_name">Имя и фамилия</label>
														</div>
														<div class="input-field col s4">
															<input type="tel" name="user_tel_number" placeholder="(___) ___-__-__" value="${data.user_tel_number ? data.user_tel_number : ''}">
															<label for="client_tel_number" class="active">Номер телефона</label>
														</div>
														</div>
														<div class="row">
														<div class="input-field col s12">
															<textarea class="materialize-textarea" name="user_description">${data.description ? data.description : ''}</textarea>
															<label for="user_description">Комментарий</label>
														</div>
														</div>
													</fieldset>

													<fieldset>
													<h5 class="sidepage-group-title">Вход на сайт</h5>

													<div class="row">

														<div class="input-field col s4">
															<input name="user_login" type="text" value="${data.login ? data.login : ''}">
															<label for="user_login">Логин *</label>
														</div>
														<div class="input-field col s4">
															<input type="text" name="user_password" value="${data.pass ? data.pass : ''}">
															<label for="user_password">Пароль *</label>
														</div>
														<div class="input-field col s4">
															<input name="user_email" type="email" value="${data.mail ? data.mail : ''}">
															<label for="user_email">Почта</label>
														</div>
													</div>
													</fieldset>

													<fieldset>
													<h5 class="sidepage-group-title">Аватар</h5>

													<div class="row">

														<div class="file-field input-field col s8">
															<figure class="user-avatar" data-attr="${data.first_name ? data.first_name : ''} ${data.last_name ? data.last_name : ''}">
																<img class="user-avatar" id="saved-image-preview" src="${data.avatar ? data.avatar : ''}" width="48" height="48" alt="">
																<img class="user-avatar hide" id="attach-image-preview" src="" width="48" height="48"  alt="Image preview...">
																<button type="button" id="remove-avatar-button" class="icon-button ${data.avatar ? '' : 'hide'}"><i class="material-icons">close</i></button>
															</figure>
															<div class="btn btn-secondary">
																<span>Выбрать файл</span>
																<input type="hidden" name="MAX_FILE_SIZE" value="30000000" />
																<input type="file" id="fileToUpload" name="fileToUpload" multiple="false" onchange="previewFile()" accept="image/*">
															</div>
															<div class="file-path-wrapper">
																<input id="selected-file-input" class="file-path validate hide" type="text" placeholder="Файл не выбран" value="${data.avatar ? data.avatar : ''}" name="avatar">
																<label for="avatar" hidden>Аватар</label>
															</div>
														</div>

													</div>
													</fieldset>

													<input type="submit" id="data-form" hidden>
													</form>
													</div>`;
		}

		if(this.data.params.pageTemplate === 'orders'){
			template = `<div class="row m-none">
					<form id="data_form" name="data_form" method="post" action="#" enctype="multipart/form-data" class="col s12">

							<fieldset>
								<h5 class="sidepage-group-title">Устройство и неисправности</h5>

								<div class="row">
									<div class="input-field col s12 m6 l7">
										<input name="device_name" type="text" placeholder="Samsung s8+" autofocus required value="${data.device_name ? data.device_name : ''}">
										<label for="device_name">Устройство *</label>
									</div>

									<div class="input-field col s12 m6 l5">
										<input name="serial_number" type="text" required placeholder="Серийный номер" value="${data.serial_number ? data.serial_number : ''}">
										<label for="serial_number">IMEI *</label>
									</div>
								</div>

								<div class="row">
									<div class="input-field col s12 m6 l12">
										<input id="device_trouble_client_says" name="device_trouble_client_says" type="text" required value="${data.device_trouble_client_says ? data.device_trouble_client_says : ''}">
										<label for="device_trouble_client_says">Заявленная неисправность *</label>
									</div>
								</div>



								<div class="row">
									<div class="input-field col s12 m6 l6">
										<input name="device_looks" type="text" value="Трещины,сколы,потертости" value="${data.device_looks ? data.device_looks : ''}">
										<label for="device_looks">Внешний вид</label>
									</div>

									<div class="input-field col s12 m6 l6">
										<input name="complectation" type="text" placeholder="Сим карта, карта памяти" value="${data.complectation ? data.complectation : ''}">
										<label for="complectation">Комплектация</label>
									</div>
								</div>
							</fieldset>

							<fieldset>
								<h5 class="sidepage-group-title">Клиент</h5>

								<div class="row">

									<div class="input-field col s12 m5 l5">
										<input id="fio_clienta" name="fio_clienta" class="autocomplete" type="text" required value="${data.client_fio ? data.client_fio : ''}">
										<label for="fio_clienta">ФИО *</label>
									</div>

									<div class="input-field col s5 m3 l3">
										<input onkeydown="phoneNumberFormatter()" id="phone-number" name="client_tel_number" type="tel" placeholder="(___) ___-__-__" value="${data.client_tel_number ? data.client_tel_number : ''}">
										<label for="client_tel_number">Номер телефона </label>
									</div>

									<div class="input-field col s7 m4 l4">
										<select name="client_source">
											${this.getSelectValue(data.client_source,this.data.params.client_source)}
									 </select>
									 <label>Клиент узнал о нас</label>
									</div>
								</div>
							</fieldset>

							<fieldset name="form_services" class="mb-double">
								<h5 class="sidepage-group-title">Услуги и материалы</h5>
									<ul id="services-list" class="collection"></ul>
								<button type="button" class="btn-flat btn-secondary" onClick="table.addServices()">+ Добавить</button>
							</fieldset>

							<fieldset>
								<div class="flex-row flex-align-space-between">
								<h5 class="sidepage-group-title">Ремонт</h5>
								<div class="input-field m-none">
									<p class="m-none">
										<label>
											<input type="checkbox" name="urgent_remont" class="filled-in" ${data.urgent_remont == 'on' ? 'checked' : ''} />
											<span>Срочный</span>
										</label>
									</p>
								</div>
								</div>


								<div class="row">

									<div class="input-field col s12 m5 l7">
										<select name="master">

										 ${this.getSelectOptions(data.master,
										 												 Object.keys(this.data['params']['masters']),
																						 Object.values(this.data['params']['masters']),
																						 Object.values(this.data['params']['masters'])
																					 )
																					 }
									 </select>
									 <label>Мастер</label>
									</div>


									<div class="input-field col s6 m3 l2" style="min-width: 90px;">
										<input name="price" type="number" placeholder="0" value="${data.price ? data.price : '0'}">
										<label for="price">Стоимость</label>
									</div>

									<div class="input-field col s6 m4 l3" style="min-width: 146px;">
									  <input type="text" name="remont_ready_date" class="datepicker"  value="${data.remont_ready_date ? data.remont_ready_date : ''}">
										<label for="remont_ready_date" >Дата готовности</label>
									</div>


								</div>
							</fieldset>


							${data.device_name ? '' : '<fieldset>' +
										'<div class="row">' +
											'<div class="input-field col s12 l12">' +
												'<textarea name="notes" class="materialize-textarea"></textarea>' +
												'<label for="notes">Комментарий</label>' +
											'</div>' +
										'</div>' +
									'</fieldset>'}




							<input type="submit" id="data-form" hidden>

				</form>
			</div>`;
		}

		return template;
	}
	getViewTemplate(data){
		var template;
		if(this.data.params.pageTemplate === 'my_docs'){

				template = `
												<div class="row sidepage-datalist">
																					<div class="col s10">
																						<div id="post-details">
																							${data["post_content"]}
																						</div>
																					</div>

																					<div class="col s2">
																						<div class="row">
																							<div class="col">
																								<button title="Редактировать"
																												onClick="table.editRow( ${data["ID"]})"
																												class="mdl-button icon-button">
																													<i class="material-icons">edit</i>
																								</button>
																								<button title="Удалить"
																												onClick="table.deleteRow( ${data["ID"]}
																												class="mdl-button icon-button">
																													<i class="material-icons">delete</i>
																								</button>
																							</div>
																						</div>

																						<div class="row">
																							<div class="col">
																								<div class="grey-text">Изменен</div>
																								<div>${this.dateFormatter(data["post_modified"], true)}</div>
																							</div>
																						</div>

																						<div class="row">
																							<div class="col">
																								<div class="grey-text">Создал</div>
																								<div>${this.dateFormatter(data["post_date"], true)}</div>
																								<div>${data["creator"]}</div>
																							</div>
																						</div>

																					</div>
																				</div>	`;
		}

		if(this.data.params.pageTemplate === 'orders'){
			template = `<div class="row sidepage-datalist">
												<div class="col s10">
													<ul class="tabs">
														<li class="tab">
															<a href="#post-details" class="${window.activeTab == 'details' ? 'active' : ''}">
																<i class="material-icons left">info</i>
															</a>
														</li>
														<li class="tab">
															<a href="#post-comments" class="active" class="${window.activeTab == 'comments' ? 'active' : ''}">
																<i class="material-icons left">comments</i>${data["comments"]}
															</a>
														</li>
													</ul>

										    <div id="post-details">
													<div class="row">
														<div class="col s12">
															<div class="row">
																<div class="col s6">
																	<div class="grey-text">Устройство</div>
																	<h5>${data["device_name"]}</h5>
																	<div>${data["serial_number"]}</div>
																</div>
																<div class="col s6">
																	<div class="grey-text">Клиент</div>
																	<a href="tel:${data["client_tel_number"]}">${this.phoneNumberFormatter(data["client_tel_number"]) }</a>
																	<div>${data["client_fio"]}</div>
																</div>
															</div>
															<div class="row">
																<div class="col s6">
																	<div class="grey-text">Заявленная неисправность</div>
																	<div>${data["device_trouble_client_says"]}</div>
																</div>
																<div class="col s6">
																	<div class="grey-text">Внешний вид</div>
																	<div>${data["device_looks"]}</div>
																</div>
															</div>

															<div class="row">
																<div class="col s6">
																	<div class="grey-text">Комплектация</div>
																	<div>${data["complectation"]}</div>
																</div>
																<div class="col s6">
																	<div class="grey-text">Клиент узнал о нас</div>
																	<div>${data["client_source"]}</div>
																</div>
															</div>


															<div class="row">
																<div class="col s12">
																	<div class="card">
																		<ul class="collapsible">
																	    <li>
																	      <div class="collapsible-header"><i class="material-icons">build_circle</i><span>Работы</span><button class="icon-button"><i class="material-icons">add</i></button></div>
																	      <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
																	    </li>
																	    <li>
																	      <div class="collapsible-header"><i class="material-icons">memory</i><span>Запчасти</span><button class="icon-button"><i class="material-icons">add</i></button></div>
																	      <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
																	    </li>
																	  </ul>
																	</div>
																</div>
															</div>


														</div>
													</div>

												</div>



										    <div id="post-comments">
													<form id="add_comment" name="add_comment" class="row">
														<div class="input-field col s12">
															<textarea id="textarea-comment" class="materialize-textarea" required></textarea>
															<label for="textarea1">Комментарий</label>
														</div>
														<button type="submit" onClick="table.addComment(${data["ID"]})" class="btn waves-effect waves-light">Комментировать</button>
													</form>
													${(function () {
														var html = '';
														for(let i in data['comments_list']){
															html +=
															`<article class="comment" comment-id="${data['comments_list'][i]['comment_ID']}">
																<div data-attr="comment_author">${data['comments_list'][i]['comment_author']}</div>
																<div data-attr="comment_date">${data['comments_list'][i]['comment_date']}</div>
																<div data-attr="comment_text"><div class="comment__text-content">${data['comments_list'][i]['comment_content']}</div></div>
																<div class=""><img class="user-avatar" src="${data['comments_list'][i]['user_avatar']}" width="48" height="48" /></div>
																<button data-target="menu-${data['comments_list'][i]['comment_ID']}" class="dropdown-menu dropdown-trigger waves-effect icon-button" alignment="right" constrainWidth="false">
																	<i class="material-icons">more_vert</i>
																</button>
																<ul id="menu-${data['comments_list'][i]['comment_ID']}" class="dropdown-content">
																	<li class="edit_comment_button" onclick='table.editComment(this, ${data["ID"]}, ${data['comments_list'][i]['comment_ID']})'><a href="#">Редактировать</a></li>
																	<li class="delete_comment_button" onClick="table.deleteComment( ${data["ID"]}, ${data['comments_list'][i]['comment_ID']})"><a href="#" >Удалить</a></li>
																</ul>
															</article>`;
														}
														return html;
													 }()) }
												</div>

													</div>


												<div class="col s2">
													<div class="row">
														<div class="col">
															<button title="Редактировать ремонт" class="mdl-button mdl-js-button mdl-button--icon edit-user-link" post-id=" post[ID] "><i class="material-icons">edit</i></button>
															<button title="Удалить ремонт" class="mdl-button mdl-js-button mdl-button--icon delete-post-link" post-id=" post[ID] "><i class="material-icons">delete</i></button>
														</div>
													</div>
													<div class="row">
														<div class="col">
															<div class="grey-text">Стоимость</div>
															<div>${data["price"]}</div>
														</div>
													</div>
													<div class="row">
														<div class="col">
															<div class="grey-text">Статус</div>
															<span class="new badge">${data["status"]}</span>
														</div>
													</div>
													<div class="row">
														<div class="col">
															<div class="grey-text">Мастер</div>
															<div>${data["master"]}</div>
														</div>
													</div>
													<div class="row">
														<div class="col">
															<div class="grey-text">Срок</div>
															<div>${data["remont_ready_date"]}</div>
														</div>
													</div>
													<div class="row">
														<div class="col">
															<div class="grey-text">Создал</div>
															<div>${data["creation_date"]}</div>
															<div>${data["creator"]}</div>
														</div>
													</div>
													<div class="row">
														<div class="col">
															<div class="grey-text">Изменен</div>
															<div>${data["last_changed"]}</div>
															<div>${data["change_by"]}</div>
														</div>
													</div>
												</div>
											</div>`;
		}

		return template;
	}
}
