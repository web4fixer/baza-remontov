
<?php get_header() ; ?>
<?php
 $data = get_queried_object();
 $data_id = get_queried_object()->term_id;
 $term_meta = get_term_meta($data_id);
 $term = get_term($data_id);
?>

	<?php
		$posts_ids = array();
		if (have_posts()):
			while (have_posts()) : the_post();
			array_push($posts_ids, get_the_ID());
			endwhile;
			$post_number = sizeof($posts_ids);
		else:
			$posts_ids = array(1);
			$post_number = 0;
		endif;

		$get_orders = get_my_orders(get_current_user_id(), $posts_ids, array("3","5","36","34","2","35","33","4","261"));
		echo "<script>window.apiData = ".json_encode($get_orders)."</script>";
	?>




	 <section class="flex-column flex-grow">
		 <div class="page-content card">
			 <section class="flex-column">
         <div class="flex-row flex-align-center">
           <div class="flex-row breadcrumbs">
  					 <a href="<?php echo get_page_link(863) ?>" class="text-underline">Настройки</a>
  					 <span class="material-icons">arrow_forward</span>
  					 <a href="<?php echo get_page_link(796) ?>" class="text-underline">Клиенты</a>
  				 </div>
         </div>
			 </section>
       <header class="user-profile flex-row m-t-base">
				 <div class="flex-row m-right-double">
	 					<div class="flex-column">
	 						<h3 class="user-profile__name page-title"><?php echo $term_meta['client_fio'][0]; ?></h3>
							<div class="flex-row flex-wrap">

		 						 <div class="description-list m-right-double" style="margin-left: 0">
		 						 		<span class="description-list__key">Телефон</span>
		 								<a href="tel:<?php echo $term->name; ?>" class="description-list__value"><?php echo $term->name; ?></a>
		 						 </div>
		 						 <div class="description-list m-right-double">
		 						 		<span class="description-list__key">Почта</span>
		 								<a href="mailto:<?php echo $term_meta['client_email'][0]; ?>" class="description-list__value"><?php echo $term_meta['client_email'][0]; ?></a>
		 						 </div>
		 						 <div class="description-list m-right-double">
		 						 		<span class="description-list__key">Источник</span>
		 								<span class="description-list__value"><?php echo $term_meta['client_source'][0]; ?></span>
		 						 </div>
		 					 </div>
		 				 </div>
						</div>

				 <div class="flex-grow right-align m-right-double">
				 </div>
				 <button type="button" class="waves-effect waves-teal btn-flat icon-button">
					 <i class="material-icons">edit</i>
				 </button>
			 </header>


			 <div id="search-table"></div>
	     <ul class="tabs-custom">
	       <li class="tab current-menu-item"><a href="<?php echo get_category_link(259) ?>"><?php echo get_cat_name(259)?> (<?php echo sizeof($get_orders['page_data'])?>)</a></li>
	       <li class="tab"><a href="<?php echo get_category_link(260) ?>"><?php echo get_cat_name(260)?> (<?php echo $inactive_posts_count?>)</a></li>
	     </ul>

	     <div id="content-filter"></div>
	     <div id="content"></div>
		</div>
    <?php get_template_part( 'custom', 'footer' ); ?>
 	</section>







<script type="text/javascript">
  var table = new Datatable(window.apiData, "#content");
</script>

<?php get_footer(); ?>
