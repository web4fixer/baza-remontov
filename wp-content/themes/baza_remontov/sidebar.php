<?php if (is_user_logged_in()): ?>
<?php
	$user_id = get_current_user_id();
	$user_meta = get_user_meta($user_id);
	$term = get_term_by( 'name', $user_id, 'user_position' );
	$term_url = get_term_link($term->term_id, 'user_position');
?>





<aside class="z-depth-2 side-main-menu show-on-large">
	<section class="aside-menu-container flex-column">
		<a href="<?php echo site_url(); ?>" class="logo-container flex-column">
		 <svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="40" height="40" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name');?>"
			width="440.000000pt" height="469.000000pt" viewBox="0 0 440.000000 469.000000"
			preserveAspectRatio="xMidYMid meet">

		 <g transform="translate(0.000000,469.000000) scale(0.100000,-0.100000)"
		 fill="rgba(255,255,255,.87)" stroke="none">
		 <path fill="#fff" d="M1595 4248 c-189 -9 -425 -44 -615 -89 -358 -86 -610 -229 -683 -387
		 -20 -43 -22 -63 -22 -237 0 -187 0 -191 28 -251 129 -278 695 -481 1394 -501
		 l212 -6 11 27 c6 15 30 58 54 95 123 191 324 335 549 393 67 18 110 22 237 21
		 139 0 164 -3 243 -27 82 -24 191 -72 245 -106 21 -14 24 -13 48 15 14 17 37
		 55 52 85 26 53 27 63 30 236 4 229 -6 265 -108 365 -93 92 -221 161 -435 234
		 -78 27 -292 74 -445 99 -132 21 -527 50 -605 44 -11 0 -96 -5 -190 -10z"/>
		 <path fill="#fff" d="M2625 3075 c-38 -7 -86 -19 -105 -27 l-35 -15 236 -239 c204 -207
		 238 -246 252 -288 35 -104 8 -206 -73 -279 -94 -85 -214 -95 -320 -27 -25 16
		 -139 124 -254 240 -114 115 -211 210 -215 210 -4 0 -16 -26 -26 -57 -27 -82
		 -33 -287 -11 -374 82 -324 369 -546 696 -538 l75 2 365 -366 c338 -339 370
		 -369 435 -399 62 -29 82 -33 170 -36 83 -3 110 0 157 18 130 48 230 153 269
		 280 30 101 24 221 -15 310 -27 59 -61 97 -398 435 l-368 371 0 86 c0 48 -7
		 120 -15 160 -32 153 -141 320 -264 406 -170 120 -360 163 -556 127z"/>
		 <path fill="#fff" d="M277 2823 c-11 -10 -8 -535 3 -593 40 -211 268 -398 626 -515 230
		 -75 410 -108 745 -135 171 -14 579 8 579 31 0 5 -4 9 -9 9 -21 0 -193 175
		 -234 238 -87 132 -140 271 -153 399 -4 37 -12 71 -18 75 -6 4 -55 8 -109 8
		 -93 0 -410 31 -499 49 -444 89 -734 224 -875 409 -25 31 -42 39 -56 25z"/>
		 <path fill="#fff" d="M274 1664 c-12 -67 3 -515 19 -565 68 -209 256 -389 565 -544 145
		 -72 284 -115 552 -172 117 -25 141 -27 395 -27 295 0 375 8 600 63 228 56 422
		 139 590 252 104 70 259 213 270 250 6 18 -24 52 -191 214 -146 142 -203 191
		 -214 185 -63 -33 -361 -115 -510 -140 -164 -28 -211 -32 -465 -37 -286 -5
		 -323 -2 -632 48 -103 17 -287 66 -393 106 -247 92 -410 204 -533 366 -15 20
		 -32 37 -37 37 -5 0 -12 -16 -16 -36z"/>
		 </g>
		 </svg>
	 </a>
	 <div class="divider"></div>
		<?php
			wp_nav_menu( [
				'menu' => 'main menu',
			 	'container' => 'nav',
				'menu_class' => 'main-menu',
			 ]);
		?>
		<div class="divider"></div>
		<div class="flex-grow"></div>

		<ul class="main-menu">
			<li class="menu-item">
				<a class="mdl-navigation__link modal-trigger" href="#modal1">
					<i class="material-icons">manage_accounts</i>
				</a>
			</li>
			<li class="menu-item">
				<a href="#" class="mdl-navigation__link">
					<i class="material-icons">contact_support</i>
				</a>
			</li>
			<li class="menu-item">

				<a href="" class="mdl-navigation__link">
					<img class="avatar" width="40" height="40" src="<?php echo $user_meta['avatar'][0] ?>">
				</a>
			</li>
			<li class="menu-item">
				<a class="mdl-navigation__link" href="<?php echo wp_logout_url( home_url() ); ?>" title="Выход"><i class="material-icons">logout</i></a>
			</li>
		</ul>
	</section>
</aside>

<aside id="mobile-menu" class="sidenav">
	<ul class="sidenav-menu">
			<li>
				<a href="<?php echo site_url(); ?>" class="logo-container flex-row">
		 		 <svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="40" height="40" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name');?>"
		 			width="440.000000pt" height="469.000000pt" viewBox="0 0 440.000000 469.000000"
		 			preserveAspectRatio="xMidYMid meet">

		 		 <g transform="translate(0.000000,469.000000) scale(0.100000,-0.100000)"
		 		 fill="#666" stroke="none">
		 		 <path fill="#666" d="M1595 4248 c-189 -9 -425 -44 -615 -89 -358 -86 -610 -229 -683 -387
		 		 -20 -43 -22 -63 -22 -237 0 -187 0 -191 28 -251 129 -278 695 -481 1394 -501
		 		 l212 -6 11 27 c6 15 30 58 54 95 123 191 324 335 549 393 67 18 110 22 237 21
		 		 139 0 164 -3 243 -27 82 -24 191 -72 245 -106 21 -14 24 -13 48 15 14 17 37
		 		 55 52 85 26 53 27 63 30 236 4 229 -6 265 -108 365 -93 92 -221 161 -435 234
		 		 -78 27 -292 74 -445 99 -132 21 -527 50 -605 44 -11 0 -96 -5 -190 -10z"/>
		 		 <path fill="#666" d="M2625 3075 c-38 -7 -86 -19 -105 -27 l-35 -15 236 -239 c204 -207
		 		 238 -246 252 -288 35 -104 8 -206 -73 -279 -94 -85 -214 -95 -320 -27 -25 16
		 		 -139 124 -254 240 -114 115 -211 210 -215 210 -4 0 -16 -26 -26 -57 -27 -82
		 		 -33 -287 -11 -374 82 -324 369 -546 696 -538 l75 2 365 -366 c338 -339 370
		 		 -369 435 -399 62 -29 82 -33 170 -36 83 -3 110 0 157 18 130 48 230 153 269
		 		 280 30 101 24 221 -15 310 -27 59 -61 97 -398 435 l-368 371 0 86 c0 48 -7
		 		 120 -15 160 -32 153 -141 320 -264 406 -170 120 -360 163 -556 127z"/>
		 		 <path fill="#666" d="M277 2823 c-11 -10 -8 -535 3 -593 40 -211 268 -398 626 -515 230
		 		 -75 410 -108 745 -135 171 -14 579 8 579 31 0 5 -4 9 -9 9 -21 0 -193 175
		 		 -234 238 -87 132 -140 271 -153 399 -4 37 -12 71 -18 75 -6 4 -55 8 -109 8
		 		 -93 0 -410 31 -499 49 -444 89 -734 224 -875 409 -25 31 -42 39 -56 25z"/>
		 		 <path fill="#666" d="M274 1664 c-12 -67 3 -515 19 -565 68 -209 256 -389 565 -544 145
		 		 -72 284 -115 552 -172 117 -25 141 -27 395 -27 295 0 375 8 600 63 228 56 422
		 		 139 590 252 104 70 259 213 270 250 6 18 -24 52 -191 214 -146 142 -203 191
		 		 -214 185 -63 -33 -361 -115 -510 -140 -164 -28 -211 -32 -465 -37 -286 -5
		 		 -323 -2 -632 48 -103 17 -287 66 -393 106 -247 92 -410 204 -533 366 -15 20
		 		 -32 37 -37 37 -5 0 -12 -16 -16 -36z"/>
		 		 </g>
		 		 </svg>
				 База Ремонтов
		 	 </a>
			</li>
			<li><div class="divider"></div></li>
	    <li>

				<a href="" class="user-view">
					<?php echo $service_name?>
					<img class="avatar" width="40" height="40" src="<?php echo $user_meta['avatar'][0] ?>">
					<?php echo $user_meta['first_name'][0] ?>
				</a>
			</li>

	    <li><div class="divider"></div></li>
			<li>
			<?php
				wp_nav_menu( [
					'menu' => 'main menu mobile',
				 	'container' => 'ul',
					'menu_class' => '',
				 ]);
			?>
			</li>

			<li class="flex-grow"></li>
			<li><a class="waves-effect" href="#!"><i class="material-icons">contact_support</i>Поддержка</a></li>
			<li><a class="waves-effect" href="<?php wp_logout_url() ?>"><i class="material-icons">logout</i>Выйти</a></li>
	  </ul>
</aside>

<div id="modal1" class="modal">
	<div class="modal-content">
		<h4>Modal Header</h4>
		<?php
			echo "<pre>";
			print_r($user_meta);
			echo "</pre>";
		?>
	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
	</div>
</div>


<?php else: ?>

	<div class="flex-order-3 flex-row flex-grow flex-align-center flex-justify-center">
			<form class="card m-none" method="post" style="width: 440px">
				<div class="row">
					<div class="input-field col s12">
						<h1 class="page-title">Вход на Базу Ремонтов</h1>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input type="text" name="username" id="username" aria-describedby="login_error" class="validate" value="" size="20" autocapitalize="off">
						<label for="log">Login *</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input type="password" name="password" id="password" aria-describedby="login_error" class="validate password-input" value="" size="20">
						<label for="pwd">Password *</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
							<button type="submit" name="submit-login-dialog" class="btn full-width">Войти</button>
						</div>
					</div>
			</form>
	</div>


<?php endif; ?>
