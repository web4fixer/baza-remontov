<?php /* Template Name: Settings */ ?>

<?php get_header() ;
$user_id = get_current_user_id();
$user_groups = wp_get_object_terms($user_id, 'user_position');
$service_name = $user_groups[0]->name;
?>



<div class="flex-row flex-align-stretch flex-grow">
	<section class="page-content flex-column flex-grow card">
		<header class="flex-row">
      <h1 class="page-title">Настройки</h1>
    </header>
		<?php
			wp_nav_menu( [
				'menu' => 'my service',
				'container' => 'ul',
				'menu_class' => 'settings-list flex-row',
			 ]);
		?>
		<?php get_template_part( 'custom', 'footer' ); ?>
	</section>
	<!-- <div class="flex-row flex-align-center">
			<img
				class="service-logo"
				src="<?php echo $user_groups[0]->description ?>"
				title="<?php echo $service_name ?>"
				alt="<?php echo $service_name ?>"
				width="48"
				height="48"
				style="border-radius: 8px;"
			/>
		<h6><?php echo $service_name ?></h6>
		</div> -->
</div>

<?php get_footer(); ?>
