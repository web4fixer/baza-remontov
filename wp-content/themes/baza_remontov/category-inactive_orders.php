<?php get_header() ; ?>

<?php
  $user_id = get_current_user_id();
  $active_posts_count = getPostsNumber($user_id, array("3","5","36","34","2","35"));

  $get_orders = get_my_orders($user_id, array(), array("33","4","261"));
  echo "<script>window.apiData = ".json_encode($get_orders)."</script>";
?>

        <div class="nav-content-aligner">
          <h5>Заказы</h5>
          <button onClick="table.addOrder()" type="button" class="waves-effect waves-light btn">+ Заказ</button>
        </div>

        <ul class="tabs-custom">
          <li class="tab"><a href="<?php echo get_category_link(259) ?>"><?php echo get_cat_name(259)?> (<?php echo $active_posts_count?>)</a></li>
          <li class="tab current-menu-item"><a href="<?php echo get_category_link(260) ?>"><?php echo get_cat_name(260)?> (<?php echo sizeof($get_orders['page_data'])?>)</a></li>
        </ul>

        <div class="card">
        <div class="row">
          <div class="col s12">
            <div id="content-filter"></div>
            <main id="content">
            </main>
          </div>
        </div>
        </div>

<script type="text/javascript">
  var tableFilter = new TableFilter(window.apiData, "#content-filter", "#content");
  var table = new Datatable(window.apiData, "#content", tableFilter);
</script>


<?php get_footer(); ?>
