<?php get_header() ; ?>
<?php if (is_user_logged_in()): ?>

<main id="page">
	<div class="container">
	<?php
		if (have_posts()):
			while (have_posts()) : the_post();
				the_content();
			endwhile;
		else:
			echo '<p>Sorry, no posts matched your criteria.</p>';
		endif;
	?>

</div>
</main>

<?php endif; ?>

<?php get_footer(); ?>
