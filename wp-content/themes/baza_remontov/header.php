
<!doctype html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
	<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo('name'); ?> - <?php bloginfo('description'); ?></title>
	<link rel="shortcut icon" href="images/favicon.png">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<?php wp_enqueue_script("jquery"); ?>
	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
   <main id="page" class="flex-column flex-grow">
   	<article class="flex-grow flex-row flex-align-stretch">
			<?php get_sidebar(); ?>
