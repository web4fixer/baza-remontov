
<?php get_header() ; ?>

<?php

  $user_id = get_current_user_id();
  $inactive_posts_count = getPostsNumber($user_id, array("33","4","261"));

  $get_orders = get_my_orders($user_id, array(), array("3","5","36","34","2","35"));
  echo "<script>window.apiData = ".json_encode($get_orders)."</script>";
?>



  <section class="page-content flex-column flex-grow card">


    <header class="page-header flex-row flex-align-space-between">
      <section><h1 class="page-title">Заказы</h1></section>
      <section class="flex-row flex-justify-center"></section>
      <section class="flex-row flex-justify-end"><button type="button" onClick="table.addOrder()" class="waves-effect waves-light btn">+ Заказ</button></section>
    </header>

    <ul class="tabs-custom">
      <li class="tab current-menu-item"><a href="<?php echo get_category_link(259) ?>"><?php echo get_cat_name(259)?> (<?php echo sizeof($get_orders['page_data'])?>)</a></li>
      <li class="tab"><a href="<?php echo get_category_link(260) ?>"><?php echo get_cat_name(260)?> (<?php echo $inactive_posts_count?>)</a></li>
    </ul>

    <div id="content-filter"></div>
    <div id="content"></div>


</section>






<script type="text/javascript">
  var tableFilter = new TableFilter(window.apiData, "#content-filter", "#content");
  var table = new Datatable(window.apiData, "#content", tableFilter);
</script>


<?php get_footer(); ?>
