<?php get_header() ; ?>
<?php get_template_part( 'toolbar-page' ); ?>

	<?php
		if (have_posts()):
			while (have_posts()) : the_post();
				the_content();
			endwhile;
		else:
			echo '<p>Sorry, no posts matched your criteria. INDEX PHP</p>';
		endif;
	?>



<?php get_footer(); ?>
