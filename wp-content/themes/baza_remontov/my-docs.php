<?php /* Template Name: My docs */ ?>

<?php get_header() ; ?>

<div class="flex-row flex-align-stretch flex-grow">
	<section class="page-content flex-column flex-grow card">
		<?php
			wp_nav_menu( [
				'menu' => 'my service',
				'container' => 'ul',
				'menu_class' => 'tabs-pages',
			 ]);
		?>

		<header class="page-header flex-row flex-align-space-between">
			<section><h1 class="page-title"><?php echo the_title() ?></h1></section>
			<section class="flex-row flex-justify-center"><div id="search-table"></div></section>
			<section class="flex-row flex-justify-end"><button onClick="table.addDoc()" class="waves-effect waves-light btn">+ документ</button></section>
		</header>

		<?php wp_enqueue_editor(); ?>
    <div id="content">
    </div>
		<?php get_template_part( 'custom', 'footer' ); ?>
	</section>
</div>


  <?php
    $get_docs = get_my_docs(get_current_user_id());
    echo "<script>window.apiData = ".json_encode($get_docs)."</script>";
  ?>

<script type="text/javascript">
    var table = new Datatable(window.apiData, "#content", '');
</script>

<?php get_footer(); ?>
