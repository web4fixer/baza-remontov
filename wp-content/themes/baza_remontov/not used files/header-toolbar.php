<?php global $post; ?>
<?php $current_user = wp_get_current_user(); ?>
<header class="demo-header mdl-layout__header">
	<div class="mdl-layout__header-row">
		<h5 class="page-title"><?php the_title() ?></h5>
		<div class="mdl-layout-spacer mdl-typography--text-center">
			<?php get_search_form(); ?>
		</div>

		<div class="drawer-footer">
		<div class="drawer-footer__details">
			<button class="mdl-button mdl-button--primary add_post_btn">Добавить ремонт</button>
				<button id="add_remont" class="mdl-button mdl-button--primary">Добавить ремонт NEW</button>

			<a href="<?php echo get_author_posts_url( $current_user->ID, ''); ?>">
				<?php echo get_avatar( $current_user->ID, 32 ); ?>
			</a>
			<?php
			 echo '<span>' . $current_user->user_login . '</span>';
		 ?>
		</div>



		<button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" id="hdrbtn">
			<i class="material-icons">more_vert</i>
		</button>
		<ul class="mdl-menu mdl-js-menu mdl-js-ripple-effect mdl-menu--bottom-right" for="hdrbtn">
			<li class="mdl-menu__item">About</li>
			<li class="mdl-menu__item">Contact</li>
			<li class="mdl-menu__item">Legal information</li>
		</ul>
	</div>
</header>
