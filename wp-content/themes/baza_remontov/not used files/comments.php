<?php $args = array(
    'walker'            => null,
		'max_depth'         => '',
		'style'             => 'ul',
		'callback'          => 'mytheme_comment',
		'end-callback'      => null,
		'type'              => 'all',
		'reply_text'        => 'Reply',
		'page'              => '',
		'per_page'          => '',
		'avatar_size'       => 32,
		'reverse_top_level' => true,
		'reverse_children'  => '',
		'format'            => 'html5', // или xhtml, если HTML5 не поддерживается темой
		'short_ping'        => false,    // С версии 3.6,
		'echo'              => true,     // true или false
    );
?>

<?php if (get_comments_number()) :?>
    <span>Комментарии <span>(<?php comments_number('', '1', '%'); ?>)</span></span>
  <?php endif;?>
<div class="post-comments-block">
  <ul class="post-comments">
    <?php wp_list_comments($args); ?>
  </ul>
  <?php if (!comments_open()){?>
    <p>Комментарии запрещены</p>
  <?php }else{ ?>
  <?php if (!get_comments_number()) :?>
      <p>Комментариев пока нет, будьте первым.</p>
    <?php endif;?>
    <?php } ?>
</div>
