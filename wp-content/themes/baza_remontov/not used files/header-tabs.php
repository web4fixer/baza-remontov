
		<header class="demo-header mdl-layout__header">
			<div class="mdl-layout__header-row">
				<?php
					wp_nav_menu( array(
					    'container_class' => 'custom-menu-class',
							'container'       => 'div',           // (string) Контейнер меню. Обворачиватель ul. Указывается тег контейнера (по умолчанию в тег div)
							'container_id'    => '',              // (string) id контейнера (div тега)
							'menu_class'      => 'top-menu mdl-navigation',          // (string) class самого меню (ul тега)
							'menu_id'         => '',              // (string) id самого меню (ul тега)
							'echo'            => true,            // (boolean) Выводить на экран или возвращать для обработки
							'fallback_cb'     => 'wp_page_menu',  // (string) Используемая (резервная) функция, если меню не существует (не удалось получить)
							'before'          => '',              // (string) Текст перед <a> каждой ссылки
							'after'           => '',              // (string) Текст после </a> каждой ссылки
							'link_before'     => '',              // (string) Текст перед анкором (текстом) ссылки
							'link_after'      => '',              // (string) Текст после анкора (текста) ссылки
							'depth'           => 0,               // (integer) Глубина вложенности (0 - неограничена, 2 - двухуровневое меню)
							'walker'          => '',              // (object) Класс собирающий меню. Default: new Walker_Nav_Menu
							'theme_location'  => 'categories-menu'               // (string) Расположение меню в шаблоне. (указывается ключ которым было зарегистрировано меню в функции register_nav_menus)
						)); ?>


				<div class="mdl-layout-spacer"></div>

				<div class="mdl-textfield__expandable-holder">
					<form action="#">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
							<label class="mdl-button mdl-js-button mdl-button--icon" for="sample6">
							<i class="material-icons">search</i>
							</label>
							<div class="mdl-textfield__expandable-holder">
							<input class="mdl-textfield__input" type="text" id="sample6">
							<label class="mdl-textfield__label" for="sample-expandable">Expandable Input</label>
							</div>
						</div>
					</form>
				</div>
				<button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" id="hdrbtn">
				<i class="material-icons">more_vert</i>
				</button>
				<ul class="mdl-menu mdl-js-menu mdl-js-ripple-effect mdl-menu--bottom-right" for="hdrbtn">
				<li class="mdl-menu__item">About</li>
				<li class="mdl-menu__item">Contact</li>
				<li class="mdl-menu__item">Legal information</li>
				</ul>
			</div>
		</header>

		
