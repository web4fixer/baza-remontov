
<?php $current_user = wp_get_current_user(); ?>

<?php acf_form_head(); ?>

<?php get_header(); ?>
<?php get_template_part( 'header-toolbar' ); ?>
<?php get_sidebar(); ?>

<main class="mdl-layout__content mdl-color--grey-100">
	<section class="mdl-grid demo-content" style="flex-direction: column">

		<?php
		if ( $author_id = get_query_var( 'author' ) ) { $author = get_user_by( 'id', $author_id ); }
		 ?>


		<ul class="">
			<li class=""><?php echo get_avatar( $author->ID, 90 ); ?></li>
			<li class=""><?php echo 'Username: ' . $author->user_login ?></li>
			<li class=""><?php echo 'email: ' . $author->user_email ?></li>
			<li class=""><?php echo 'first name: ' . $author->user_firstname ?></li>
			<li class=""><?php echo 'last name: ' . $author->user_lastname ?></li>
			<li class=""><?php echo 'Отображаемое имя: ' . $author->display_name ?></li>
			<li class=""><?php echo 'ID: ' . $author->ID ?></li>
			<li class=""><?php echo 'Description: ' . $author->user_description ?></li>
		</ul>





		<?php if (have_posts()) { ?>
		<h3><?php echo 'Ремонты ' . $current_user->display_name ?></h3>
		<section class="mdl-grid demo-content">
			<table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp full-width selectable-rows">
						<thead>
							<tr>
								<th class="mdl-data-table__cell--non-numeric">ID</th>
								<th class="mdl-data-table__cell--non-numeric">Название</th>
								<th class="mdl-data-table__cell--non-numeric">Описание</th>
								<th class="mdl-data-table__cell--non-numeric">Комментарии</th>
							</tr>
						</thead>
					<tbody>
			<?php while (have_posts()) : the_post(); ?>

					<tr class="post" id="post-<?php the_ID(); ?>">
						<td class="mdl-data-table__cell--non-numeric"><?php the_ID() ?></h5></td>
						<td class="mdl-data-table__cell--non-numeric"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></td>
						<td class="mdl-data-table__cell--non-numeric"><?php echo wp_trim_words( get_the_content(), $num_words = 20, $more = null ); ?></td>
						<td class="mdl-data-table__cell--non-numeric">
							<a href="<?php the_permalink() ?>#comments" class="comments-number">
								<?php comments_number('нет комментариев', '1 комменатрий', '% комментариев'); ?>
							</a>
						</td>
					</tr>

			<?php endwhile; ?>

				</tbody>
			</table>
			</section>


		<?php } else { ?>
			<h5>Нет ремонтов</h5>
			<?php } ?>

	</section>
</main>


	<?php get_footer(); ?>
