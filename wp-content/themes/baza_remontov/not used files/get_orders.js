

'use strict';




console.log(1);



jQuery(function($) {

class TableFilter{
	constructor(data, targetID) {
		console.log(data);
		this.targetID = targetID;
		var targetPlace = document.querySelector(targetID);
		var catKeys = Object.keys(data['categories']);
		var catValues = Object.values(data['categories']);


		var sum = 0;
		for(let i in catValues){
			sum = sum + parseInt(catValues[i]);
		}



		if(sum > 0){
			this.createFilterItems(['Все'], '', document.querySelector('#test-filter'));
			for(let i in catKeys){
				this.createFilterItems(catKeys[i], catValues[i], targetPlace);
			}
		}
	}

	createFilterItems(name, count, selector){
		let button = document.createElement('button');
		button.classList.add('waves-effect', 'waves-light', 'btn-large');
		button.setAttribute('cat-name', name);
		button.innerHTML =  name + ' ' + count;
		selector.appendChild(button);
	}
	clean(){
		document.querySelector(this.targetID).innerHTML = '';
	}
}



class Table{
	constructor(data, targetID) {
		var targetPlace = document.querySelector(targetID);
		this.targetID = targetID;

		if(data['posts'].length == 0){
			targetPlace.innerHTML = '<div class="empty-state">No items</div>';
		}
		else{
			let table = document.createElement('table');
			let tableHeader = table.createTHead();
			let tableHeaderRow = tableHeader.insertRow();
			var tableBody = table.createTBody();
			table.classList.add('orders-table', 'highlight', 'responsive-table', 'striped');

			var keys;

			for(let i in data['posts']){
				keys = Object.keys(data['posts'][i]);
				break;
			}

			console.log(keys);

			tableHeaderRow.innerHTML =
			'<th>' + keys[0] + '</th>' +
			'<th>' + keys[1] + '</th>' +
			'<th>' + keys[2] + '</th>' +
			'<th>' + keys[4] + '</th>' +
			'<th>' + keys[5] + '</th>' +
			'<th>' + keys[7] + '</th>' +
			'<th>' + keys[12] + '</th>' +
			'<th>' + keys[13] + '</th>' +
			'<th></th>';

			for(let i in data['posts']){
				this.createRow(data['posts'][i], tableBody, data['All_categories']);
			}

			table.append(tableHeader);
			table.append(tableBody);
			targetPlace.append(table);

			$('#test table').DataTable( {
						paging: false,
					//  "columnDefs": [
					// 	{ "orderable": false, "targets": 8 }
					// ],
					"oLanguage": {
								"sInfo": "Всего: _TOTAL_ "
							}
				} );
				$(".dropdown-menu").dropdown({ alignment: 'right', constrainWidth: false });
				//document.querySelectorAll('.dropdown-menu').dropdown({ alignment: 'right', constrainWidth: false });
		}
	}

	createRowMenu(data){

		let html = '';
		for(let value in data){
			html += '<li><span>' + data[value] + '</span></li>';
		}
		return html;
	}

	createRow(data, selector, rowMenuData){
//		console.log(data);

			let row = selector.insertRow();
			let values = Object.values(data);
			row.setAttribute("id", data['ID']);
			row.setAttribute("name", data['Устройство']);

			row.innerHTML =
				'<td>' + data['ID'] + ' ' + data['Срочный ремонт'] + '</td>' +

				'<td><label><span class="badge new" data="' + data['Статус'] + '" style="height: auto">' + data['Статус'] +
					'<button class="dropdown-menu waves-effect icon-button" data-target="status-' + data['ID'] + '"><i class="material-icons">arrow_drop_down</i></button>' +
					'<ul id="status-' + data['ID'] + '" class="dropdown-content status-menu">' +
							this.createRowMenu(rowMenuData) +
					'</ul>' + '</span></label>' +
				'</td>' +
				'<td>' + data['Комментарии'] + '</td>' +
				'<td>' + data['Дата создания'] + '</td>' +
				'<td><span class="span-text view-post"> ' + data['Устройство'] + '</span>' + data['Серийный номер'] + '</td>' +
				'<td>' + data['Неисправность'] + '</td>' +
				'<td>' + data['Стоимость ремонта'] + '</td>' +
				'<td>' + data['Дата готовности'] + '</td>' +
				'<td class="actions-cell">' +
					'<button class="dropdown-menu waves-effect icon-button" data-target="menu-' + data['ID'] + '"><i class="material-icons">more_vert</i></button>' +
					'<ul id="menu-' + data['ID'] + '" class="dropdown-content">' +
						'<li class="edit-post"><span>Редактировать</span></li>' +
						'<li class="delete-post"><span>Удалить</span></li>' +
					'</ul>' +
				'</td>';
	}
	clean(){
		document.querySelector(this.targetID).innerHTML = '';
	}
}


function statusFilter(obj, filterBy){
	var posts = obj['posts'];
	function statusFilter(value) {
		return value['Статус'] === filterBy;
	}
	let filtered = posts.filter(statusFilter);
	let filteredObj = {};
	filteredObj['posts'] = filtered;
	return filteredObj;
}

function boolIcon(value,icon){
	if(value === 'true'){
		return '<i class="material-icons">' + icon +'</i>';
	}
	else{
		return '';
	}
}


window.postFilter = function(obj, id){
	console.log(obj);
	var posts = obj['posts'];
	function statusFilter(elem) {
		return elem['ID'] == id;
	}
	let filtered = posts.filter(statusFilter);
	return filtered[0];
}

window.renderPost = function(post){

	document.querySelector('#contentRender').innerHTML = '';

	let bodyContent = '<div class="row sidepage-datalist">' +
									'<div class="col s12">' +

									'<div class="col s12">' +
							      '<ul class="tabs">' +
							        '<li class="tab"><a href="#post-details"><i class="material-icons left">info</i></a></li>' +
							        '<li class="tab"><a href="#post-comments" ' +
												(function () {
													var html = 'class="active"';
													if(window.activeSidepageTabComments){
														return html;
													}
												 }()) + '><i class="material-icons left">comments</i>' + post['Комментарии'] + '</a></li>' +
							      '</ul>' +
							    '</div>' +
							    '<div id="post-details" class="col s12">' +

									'<div class="row">' +
										'<div class="col s3">Статус</div>' +
										'<div class="col s9">' + post['Статус'] + '</div>' +
									'</div>' +

									'<div class="row">' +
										'<div class="col s3">Статус</div>' +
										'<div class="col s9">' +
										'<button title="Редактировать ремонт" class="mdl-button mdl-js-button mdl-button--icon edit-user-link" post-id="' + post['ID'] + '"><i class="material-icons">edit</i></button>' +
										'<button title="Удалить ремонт" class="mdl-button mdl-js-button mdl-button--icon delete-post-link" post-id="' + post['ID'] + '"><i class="material-icons">delete</i></button>' +
										'</div>' +
									'</div>' +

									'<div class="row">' +
										'<div class="col s3">Принят в ремонт</div>' +
										// '<div class="col s9">' + response.post_date_gmt.split(' ')[0] + '</div>' +
										'<div class="col s9">' + post['Дата создания'] + '</div>' +
									'</div>' +

									'<div class="row">' +
										'<div class="col s3">Изменен</div>' +
										'<div class="col s9">' + post['Изменен'] + '</div>' +
									'</div>' +

									'<div class="row">' +
										'<div class="col s3">Неисправность</div>' +
										'<div class="col s9">' + post['Неисправность'] + '</div>' +
									'</div>' +

									'<div class="row">' +
										'<div class="col s3">ФИО клиента</div>' +
										'<div class="col s9">' + post['ФИО клиента'] + '</div>' +
									'</div>' +

									'<div class="row">' +
										'<div class="col s3">Комплектация</div>' +
										'<div class="col s9">' + post['Комплектация'] + '</div>' +
									'</div>' +

									'<div class="row">' +
										'<div class="col s3">Номер телефона</div>' +
										'<div class="col s9">' + post['Номер телефона клиента'] + '</div>' +
									'</div>' +

									'<div class="row">' +
										'<div class="col s3">Почта клиента</div>' +
										'<div class="col s9">' + post['Email клиента'] + '</div>' +
									'</div>' +

									'<div class="row">' +
										'<div class="col s3">Откуда клиент узнал о нас</div>' +
										'<div class="col s9">' + post['Откуда клиент узнал о нас'] + '</div>' +
									'</div>' +

									'<div class="row">' +
										'<div class="col s3">Серийный номер</div>' +
										'<div class="col s9">' +  post['Серийный номер'] + '</div>' +
									'</div>' +

									'<div class="row">' +
										'<div class="col s3">Внешний вид</div>' +
										'<div class="col s9">' +  post['Внешний вид'] + '</div>' +
									'</div>' +

									'<div class="row">' +
										'<div class="col s3">Стоимость ремонта</div>' +
										'<div class="col s9">' +  post['Стоимость ремонта'] + '</div>' +
									'</div>' +

									'<div class="row">' +
										'<div class="col s3">Предоплата</div>' +
										'<div class="col s9">' +  post['Предоплата'] + '</div>' +
									'</div>' +

									'<div class="row">' +
										'<div class="col s3">Дата готовности</div>' +
										'<div class="col s9">' +  post['Дата готовности'] + '</div>' +
									'</div>' +
										boolIcon(post['Срочный ремонт'][0],'whatshot') +
									'</div>' +
							    '<div id="post-comments" class="col s12">' +
										'<form id="add_comment" name="add_comment" class="row">' +
											'<div class="input-field col s12">' +
												'<textarea id="textarea-comment" class="materialize-textarea" required></textarea>' +
												'<label for="textarea1">Комментарий</label>' +
											'</div>' +
											'<button type="submit" id="add_comment_button" class="btn waves-effect waves-light">Комментировать</button>' +
										'</form>' +

									(function () {
										var html = '';
										for(let i in post['comments_array']){
											html +=
											'<article class="comment" comment-id="'+ post['comments_array'][i]['comment_ID'] +'">' +
												'<div data-attr="comment_author">' + post['comments_array'][i]['comment_author'] + '</div>' +
												'<div data-attr="comment_date">' + post['comments_array'][i]['comment_date'] + '</div>' +
												'<div data-attr="comment_text"><div class="comment__text-content">' + post['comments_array'][i]['comment_content'] + '</div></div>' +
												'<div class=""><img class="user-avatar" src="' + post['comments_array'][i]['user_avatar'] + '" width="48" height="48" /></div>' +
												'<button data-target="menu-' + post['comments_array'][i]['comment_ID'] + '" class="dropdown-menu waves-effect icon-button" alignment="right" constrainWidth="false">' +
													'<i class="material-icons">more_vert</i>' +
												'</button>' +
												'<ul id="menu-' + post['comments_array'][i]['comment_ID'] + '" class="dropdown-content">' +
													'<li class="edit_comment_button"><a href="#">Редактировать</a></li>' +
													'<li class="delete_comment_button"><a href="#">Удалить</a></li>' +
												'</ul>' +
											'</article>';
										}
										return html;
									 }()) +

									 '</div>' +

								'</div>';

	let sidePageData = {
		headerText:  post['Устройство'],
		bodyContent: bodyContent,
		closeActionText: 'Закрыть',
		applyActionText: ''
	}
	let sidepage = new Sidepage(sidePageData);









	////                          COMMENTS

	/// Edit comment

	$(".edit_comment_button").click(function() {

		let edit_comment_block = document.createElement('section');
		let cancel_comment_button = document.createElement('button');
		let update_comment_button = document.createElement('button');

		let comment = $(this).closest('article');
		let comment_id = comment.attr('comment-id');
		let comment_block = comment.children('[data-attr="comment_text"]');
		let comment_text = comment_block[0].innerText;



		edit_comment_block.innerHTML = '<div class="input-field col s12">' +
																		'<textarea name="edit-comment-textarea" id="edit-comment-textarea" class="materialize-textarea">' + comment_text + '</textarea>' +
																		'<label for="edit-comment-textarea" class="active">Комментарий</label>' +
																	'<button id="cancel_comment_button" class="btn-flat waves-effect waves-light">Отмена</button>' +
																	'<button id="update_comment_button" class="btn-flat waves-effect waves-light">Сохранить</button>' +
																	'</div>';


			comment_block[0].firstElementChild.classList.add("hide");
			comment_block.append(edit_comment_block);

			setTimeout("document.getElementById('edit-comment-textarea').focus()", 10);

	});

	$(document).on("click", "#cancel_comment_button" , function() {
		this.parentElement.parentElement.previousElementSibling.classList.remove("hide");
		this.parentElement.parentElement.remove();
	});


	$(document).on("click", "#update_comment_button" , function() {
		let comment = $(this).closest('article');
		let comment_id = comment.attr('comment-id');
		let comment_text = document.querySelector("#edit-comment-textarea").value;
		var formData = new FormData;

		formData.append('action', 'update_comment');
		formData.append('comment_id', comment_id);
		formData.append('comment_text', comment_text);

		window.activeSidepageTabComments = true;

		$.ajax({
			url: MyAjax.ajaxurl,
			dataType: 'json',
			data: formData,
			processData: false,
			contentType: false,
			type: 'POST',
			statusCode: {
				200: function(response) {
					window.getOrders('get_posts_ajax', window.activeCatsValues.join());
					M.toast({html: 'Комментарий обновлен', classes: 'toast-success', displayLength: 3000});
				}
			},

		});

	});


	// ADD comment


	document.forms.add_comment.addEventListener('submit', saveComment);

	function saveComment() {
		event.preventDefault();
		let comment_input = document.querySelector("#textarea-comment");
		let comment_text = comment_input.value;
		var formData = new FormData;

		formData.append('action', 'add_comment');
		formData.append('post_id', window.currentPostId);
		formData.append('post_comment_text', comment_text);
		window.activeSidepageTabComments = true;

		$.ajax({
			url: MyAjax.ajaxurl,
			dataType: 'json',
			data: formData,
			processData: false,
			contentType: false,
			type: 'POST',
			statusCode: {
				200: function() {
					M.toast({html: 'Комментарий добавлен', classes: 'toast-success', displayLength: 3000});
					window.getOrders('get_posts_ajax', window.activeCatsValues.join());
					comment_input.value = "";
					comment_input.nextElementSibling.classList.remove("active");
				}
			},
		});
	};

	// Delete comment

	$(".delete_comment_button").click(function() {
		var comment = $(this).closest('article');
		let comment_id = comment.attr('comment-id');


		let dialogText = document.querySelector("#confirm-dialog #confirm-dialog_text");
		let dialogHeaderText = document.querySelector("#confirm-dialog [data-attr='modal-title']");
		let comment_author = comment.children('[data-attr="comment_author"]')[0].innerText;
		let comment_date = comment.children('[data-attr="comment_date"]')[0].innerText;

		dialogText.innerHTML = 'Добавлен ' + comment_date + ' пользователем ' + comment_author;
		dialogHeaderText.innerHTML = 'Удалить комментарий?';

		$('#confirm-dialog').modal('open');

		let formData = new FormData;
		formData.append('action', 'delete_comment');
		formData.append('comment_id', comment_id);
		window.activeSidepageTabComments = true;
		$( "#confirm_dialog_action" ).click(function() {
			$('#confirm-dialog').modal('close');
			$.ajax({
				url: MyAjax.ajaxurl,
				dataType: 'json',
				data: formData,
				processData: false,
				contentType: false,
				type: 'POST',
				statusCode: {
					200: function() {
						M.toast({html: 'Комментарий удален', classes: 'toast-success', displayLength: 3000});
						window.getOrders('get_posts_ajax', window.activeCatsValues.join());
					}
				}
			});
		});
	});
	$(".dropdown-menu").dropdown({ alignment: 'right', constrainWidth: false });
	$('.tabs').tabs();


	// var instance = M.Tabs.getInstance($('.tabs'));
	// if(window.activeSidepageTabComments){
	// 	instance.select('post-comments');
	// }
}


window.getOrders = function(actionName, fromCategories, taxonomy, terms, enableFilter){
	var loaders = document.querySelectorAll(".loader");
	$.ajax({
			type: 'POST',
			url: MyAjax.ajaxurl,
			dataType: "json", // add data type
			beforeSend: function() {
				for(var i = 0; i< loaders.length; i++){
			    loaders[i].classList.remove('hide');
			  }
	    },
	    complete: function() {
				for(var i = 0; i< loaders.length; i++){
			    loaders[i].classList.add('hide');
			  }
	    },
			data: { action : actionName, categories: fromCategories, includeTaxonomy: taxonomy, includeTerms: terms },
			success: function(response) {
				if(response[0].posts.length > 0){


					window.ajaxResponse = response[0];

					if(window.myTable){
						window.myFilter.clean();
						window.myTable.clean();
					}

					if(window.sidepageOpen){
						var post = window.postFilter(window.ajaxResponse, window.currentPostId);
						window.renderPost(post);
					}

					 window.myTable = new Table(response[0], "#test");

					 if(enableFilter){
						 window.myFilter = new TableFilter(response[0], "#test-filter");
						 document.querySelector('#test-filter button:first-child').classList.add('active');
						 $('#test-filter').on( "click", "button", function() {

							 let catName = $(this).attr( "cat-name" );
							 var filteredObj;
							 var activeFilterButton = document.querySelector( "#test-filter button.active" );
							 window.myTable.clean();

							 if(this.getAttribute("cat-name") === 'Все'){
								 filteredObj = response[0];
								 console.log(filteredObj);
							 }
							 else{
								 filteredObj = statusFilter(response[0], catName);
							 }
							 activeFilterButton.classList.remove("active");
							 this.classList.add("active");
							 window.myTable = new Table(filteredObj, "#test");

					 });
					 }



			}
		}	 // if data length > 0
	});
}

  let table = new Table(window.apiData, "#test");

});
