

<main class="mdl-layout__content mdl-color--grey-100 mdl-shadow--2dp">
	<section class="mdl-grid demo-content mdl-shadow--2dp">
		<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
			<div class="mdl-tabs__tab-bar">
				<?php	$items = wp_get_nav_menu_items( 'category-menu' ); ?>
				<?php foreach ( $items as $item ) : ?>
				<?php $cur_cat = get_category($item->object_id); ?>
				<a href="<?php print_r('#' . $cur_cat->slug) ?>" class="mdl-tabs__tab"><?php print_r($cur_cat->name . ' (' . $cur_cat->count . ')') ?></a>
				<?php endforeach; ?>
			</div>
			<?php foreach ( $items as $item ) : ?>
			<?php $cur_cat = get_category($item->object_id); ?>
			<div class="mdl-tabs__panel" id="<?php print_r($cur_cat->slug) ?>">
				<?php
					global $post;
					$args = array(
						'posts_per_page'   => 0,
						'offset'           => 0,
						'cat'              => $item->object_id,
						'category_name'    => '',
						'orderby'          => 'date',
						'order'            => 'DESC',
						'include'          => '',
						'exclude'          => '',
						'meta_key'         => '',
						'meta_value'       => '',
						'post_type'        => 'post',
						'post_mime_type'   => '',
						'post_parent'      => '',
						'author'	   => '',
						'author_name'	   => '',
						'post_status'      => 'publish',
						'suppress_filters' => true,
						'fields'           => ''
					);
				$attachments = get_posts( $args );
				if ( $attachments ) :  ?>
					<table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable">
						<thead>
							<tr>
								<th class="mdl-data-table__cell--non-numeric">Устройство</th>
								<th class="mdl-data-table__cell--non-numeric">Статус</th>
								<th class="mdl-data-table__cell--non-numeric">Comments</th>
							</tr>
						</thead>
						<tbody>
						<?php	foreach ( $attachments as $post ) {
							setup_postdata( $post ); ?>

							<tr id="post-<?php the_ID(); ?>" class="post cat_id-<?php the_field('field_5d98dc1a9c8ed'); ?>">
								<td class="mdl-data-table__cell--non-numeric"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></td>
								<td class="mdl-data-table__cell--non-numeric">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height">
						        <input type="text" value="" class="mdl-textfield__input" id="sample6" readonly>
						        <input type="hidden" value="" name="sample6">
						        <i class="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
						        <label for="sample6" class="mdl-textfield__label"></label>
						        <ul for="sample6" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">
						            <li class="mdl-menu__item" data-val="BY" data-selected="true"><?php echo get_cat_name( $item->object_id ) ?></li>
						            <li class="mdl-menu__item" data-val="BR">Brazil</li>
						            <li class="mdl-menu__item" data-val="ES">Estonia</li>
						            <li class="mdl-menu__item" data-val="FI">Finland</li>
						        </ul>
							    </div>
								</td>
								<td class="mdl-data-table__cell--non-numeric">
									<a href="<?php the_permalink() ?>#comments" class="comments-number">
										<?php comments_number('нет комментариев', '1 комменатрий', '% комментариев'); ?>
									</a>
								</td>
							</tr>
							<?php	wp_reset_postdata(); ?>
						</tbody>
					</table>
					<?php	} ?>
				<?php else: ?>
					<h4>Ничего не найдено</h4>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
		</div>
	</section>
</main>

<script>
document.addEventListener("DOMContentLoaded", () => {
	document.querySelector(".mdl-tabs .mdl-tabs__tab:first-child").classList.add("is-active");
	document.querySelector(".mdl-tabs .mdl-tabs__tab-bar + .mdl-tabs__panel").classList.add("is-active");
});
</script>
