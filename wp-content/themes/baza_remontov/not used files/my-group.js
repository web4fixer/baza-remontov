'use strict';
jQuery(document).ready(function($) {

var form;





function setSelectedItem(name, data, attr){
	if(name == data){
		return attr;
	}
}




function setObjDataFromRadio(elem, obj_field_name, obj){
	for (var i = 0, length = elem.length; i < length; i++) {
		if (elem[i].checked) {
			obj.append(obj_field_name, elem[i].value);
			break;
		}
	}
}



function setInputValue(formSelector, inputName, data){
	console.log(data);
	if(data){

		if(formSelector.querySelector("label[for=" + inputName + "]")){
			formSelector.querySelector("label[for=" + inputName + "]").classList.add("active");
		}


		if(formSelector.elements[inputName].type === "textarea"){
			formSelector.elements[inputName].innerHTML = data;
		}

		if(formSelector.elements[inputName].type === "radio"){
			for(i=0; i < formSelector.elements[inputName].length; i++){
				if(formSelector.elements[inputName][i].value === data){
					formSelector.elements[inputName][i].checked = true;
				}
			}
		}

		else{
			formSelector.elements[inputName].value = data;
		}

	}
}

function setRadioValue(html, data){
	let radioButtons = html.querySelectorAll([type="radio"]);
	for(i=0; i < radioButtons.length; i++){
		if(radioButtons[i].value === data){
			radioButtons[i].checked = true;
		}
		else{
			radioButtons[i].checked = false;
		}
	}
}



let formHtml =  '<div class="row">' +
									'<form id="user_form" name="user_form" method="post" action="#" enctype="multipart/form-data" class="col s12">' +
									'<div class="row">' +
										'<div class="input-field col s4 mb-none mt-none">' +
												'<h5 class="sidepage-group-title radio-group-title">Должность</h5>' +
												'<div class="radio-item">' +
													'<label>' +
														'<input class="with-gap" name="user_role" value="Администратор" type="radio" />' +
														'<span>Администратор</span>' +
													'</label>' +
												'</div>' +
												'<div class="radio-item">' +
													'<label>' +
														'<input class="with-gap" name="user_role" value="Мастер" type="radio" checked />' +
														'<span>Мастер</span>' +
													'</label>' +
												'</div>' +

												'<div class="radio-item">' +
													'<label>' +
														'<input class="with-gap" name="user_role" value="Приемщик" type="radio" />' +
														'<span>Приемщик</span>' +
													'</label>' +
												'</div>' +
											'</div>' +
											'<div class="input-field col s8 mb-none mt-none">' +
													'<h5 class="sidepage-group-title radio-group-title">Зарплата</h5>' +
													'<div class="radio-item">' +
														'<label>' +
															'<input class="with-gap" name="user_salary_type" type="radio" value="Ставка (в месяц)" checked />' +
															'<span class="radio-button_container flex-row">Ставка (в месяц)' +
															'<div class="input-field">' +
																'<input class="radio-item_input" name="user_salary_month" type="number" placeholder="0" style="width: 64px">' +
															'</div>' +
															'</span>' +
														'</label>' +
													'</div>' +

													'<div class="radio-item">' +
														'<label>' +
															'<input class="with-gap" name="user_salary_type" type="radio" value="От стоимости ремонта" />' +
															'<span class="radio-button_container flex-row">От стоимости ремонта' +
															'<div class="input-field">' +
																'<input class="radio-item_input" name="user_salary_ratio" type="number" min="0" max="100" placeholder="0" style="width: 40px">' +
															'</div>' +
															'%</span>' +
														'</label>' +
													'</div>' +
												'</div>' +
								'</div>' +

									'<h5 class="sidepage-group-title">Личные данные</h5>' +

										'<div class="row">' +
											'<div class="input-field col s4">' +
												'<input name="user_name" type="text">' +
												'<label for="user_name">Имя *</label>' +
											'</div>' +
											'<div class="input-field col s4">' +
												'<input name="user_lastname" type="text">' +
												'<label for="user_lastname">Фамилия *</label>' +
											'</div>' +
											'<div class="input-field col s4">' +
												'<input type="tel" name="user_tel_number">' +
												'<label for="user_tel_number">Номер телефона</label>' +
											'</div>' +
											'</div>' +
											'<div class="row">' +
											'<div class="input-field col s12">' +
												'<textarea class="materialize-textarea" name="user_description"></textarea>' +
												'<label for="user_description">Заметки</label>' +
												'<span class="helper-text">Если есть договоренности или замечания</span>' +
											'</div>' +
											'</div>' +

										'<h5 class="sidepage-group-title">Вход на сайт</h5>' +

										'<div class="row">' +

											'<div class="input-field col s4">' +
												'<input name="user_login" type="text">' +
												'<label for="user_login">Логин *</label>' +
											'</div>' +
											'<div class="input-field col s4">' +
												'<input type="text" name="user_password">' +
												'<label for="user_password">Пароль *</label>' +
											'</div>' +
											'<div class="input-field col s4">' +
												'<input name="user_email" type="email">' +
												'<label for="user_email">Почта</label>' +
											'</div>' +
										'</div>' +

										'<h5 class="sidepage-group-title">Аватар</h5>' +

										'<div class="row">' +

											'<div class="file-field input-field col s8">' +
												'<div class="btn waves-effect waves-teal btn-flat">' +
													'<span>Выбрать файл</span>' +
													'<input type="hidden" name="MAX_FILE_SIZE" value="30000000" />' +
													'<input type="file" id="fileToUpload" name="fileToUpload" multiple="false" accept="image/*">' +
												'</div>' +
												'<div class="file-path-wrapper">' +
													'<input id="selected-file-input" class="file-path validate" type="text" placeholder="Файл не выбран" name="user_avatar">' +
													'<label for="user_avatar" hidden>Аватар</label>' +
												'</div>' +
											'</div>' +

										'</div>' +

										'<input type="submit" id="submit-user" hidden>' +
										'</form>' +
										'</div>';



function saveUser(formName,actionName,id,toastText) {

		event.preventDefault();

		var formData = new FormData;

		formData.append('action', actionName);
		formData.append('post_id', id);
		setObjDataFromRadio(document.getElementsByName('user_role'), 'user_role', formData);
		setObjDataFromRadio(document.getElementsByName('user_salary_type'), 'user_salary_type', formData)
		formData.append('user_salary', document.querySelector("[name=user_salary_type]:checked + .radio-button_container .radio-item_input").value);
		formData.append('user_name', formName["user_name"].value);
		formData.append('user_lastname', formName["user_lastname"].value);
		formData.append('user_tel_number', formName["user_tel_number"].value);
		formData.append('user_description', formName["user_description"].value);
		formData.append('user_login', formName["user_login"].value);
		formData.append('user_password',  formName["user_password"].value);
		formData.append('user_email', formName["user_email"].value);

		if(formName["user_avatar"].value){
			formData.append('img', $("#fileToUpload").prop('files')[0]);
			formData.append('user_avatar_exists', true);
		}

		$.ajax({
			url: MyAjax.ajaxurl,
			dataType: 'json',
			data: formData,
			processData: false,
			contentType: false,
			type: 'POST',
			statusCode: {
				200: function() {
					closeSidepage();
					M.toast({html: toastText, classes: 'toast-success', displayLength: 3000})
				}
			},
		});
	}





/// ADD USER



$("#add_user").click(function() {

	let sidePageData = {
		headerText: "Добавить пользователя",
		bodyContent: formHtml,
		closeActionText: 'Отмена',
		applyActionText: 'Сохранить',
		formId: 'submit-user'
	}
	let sidepage = new Sidepage(sidePageData);

	form = document.forms.user_form;
	form.addEventListener('submit', function(){saveUser(form,'add_ajax_user',0,'Пользователь добавлен')}, false);
});







//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




			$(document).on('click', '.edit-user-link', function(){
				// let bodyContent = '';
				var post_id = $(this).attr("post-id"); //this is the post id

				$.ajax({
						type: 'POST',
						url: MyAjax.ajaxurl,
						dataType: "json", // add data type
						data: { action : 'view_ajax_user', userId: post_id},
						success: function(response) {


							let sidePageData = {
								headerText: "Редактировать пользователя",
								bodyContent: formHtml,
								closeActionText: 'Отмена',
								applyActionText: 'Сохранить',
								formId: 'submit-user'
							}

							let sidepage = new Sidepage(sidePageData);

						 form = document.forms.user_form;

						setInputValue(form, 'user_role', response['user_role'][0]);

 						setInputValue(form, 'user_salary_type', response['user_salary_type'][0]);

 						if(response['user_salary_type'][0] === 'От стоимости ремонта'){
 							setInputValue(form, 'user_salary_ratio', response['user_salary'][0]);
 						}
 						if(response['user_salary_type'][0] === 'Ставка (в месяц)'){
 							setInputValue(form, 'user_salary_month', response['user_salary'][0]);
 						}
						setInputValue(form, 'user_avatar', response.avatar[0].split("/").pop());

						setInputValue(form, 'user_name', response.first_name[0]);
						setInputValue(form, 'user_lastname', response.last_name[0]);
						setInputValue(form, 'user_tel_number', response.user_tel_number[0]);
						setInputValue(form, 'user_description',  response.description[0]);
						setInputValue(form, 'user_login', response.data.user_login);
						setInputValue(form, 'user_password', response.data.user_pass);
						setInputValue(form, 'user_email', response.data.user_email);

						setInputValue(form, 'user_avatar', response.avatar[0].split("/").pop());

						form.addEventListener('submit', function(){saveUser(form,'update_ajax_user', post_id, 'Пользователь обновлен')}, false);


	}
	});
});


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




/// VIEW USER



			$(document).on('click', '.view-user', function()
			{
				let bodyContent = '';
				var post_id = $(this).attr("rel"); //this is the post id


					$.ajax({
							type: 'POST',
							url: MyAjax.ajaxurl,
							dataType: "json", // add data type
							data: { action : 'view_ajax_user', userId: post_id},
							success: function( response ) {
								console.log(response);
								console.log(response['user_role'][0]);

								var userAvatar = '';
								var userFirstNameAbbr = response.first_name.toString();
								var userLastNameAbbr = response.last_name.toString();


								bodyContent = '<div class="row sidepage-datalist">' +
    														'<div class="col s12">' +

																	'<div class="row">' +
		        												'<div class="col s3">Должность</div>' +
																		'<div class="col s9">' + response.roles[0] + '</div>' +
																	'</div>' +

																	'<div class="row">' +
		        												'<div class="col s3">Логин</div>' +
																		'<div class="col s9">' + response.data.user_login + '</div>' +
																	'</div>' +

																	'<div class="row">' +
		        												'<div class="col s3">Пароль</div>' +
																		'<div class="col s9">'+ '<button class="waves-effect waves-teal btn-flat">Напомнить пароль</button>' + '</div>' +
																	'</div>' +

																	'<div class="row">' +
		        												'<div class="col s3">Принят на работу</div>' +
																		'<div class="col s9">' + response.data.user_registered.split(' ')[0] + '</div>' +
																	'</div>' +

																	'<div class="row">' +
		        												'<div class="col s3">Почта</div>' +
																		'<div class="col s9"><a href="mailto:' + response.data.user_email + '">' + response.data.user_email + '</a></div>' +
																	'</div>' +

																	'<div class="row">' +
		        												'<div class="col s3">Номер телефона</div>' +
																		'<div class="col s9"><a href="tel:'+ response.data.user_url.replace(/^(https?:|)\/\//,'') + '">' + response.data.user_url.replace(/^(https?:|)\/\//,'').replace(/(\d\d\d)(\d\d\d)(\d\d)(\d\d)/, "$1-$2-$3-$4") + '</a></div>' +
																	'</div>' +

																	'<div class="row">' +
		        												'<div class="col s3">Коофициент</div>' +
																		'<div class="col s9">' + response.description[0] + ' %' + '</div>' +
																	'</div>' +




																'</div>' +
															'</div>';


								if(response.avatar){
									userAvatar = '<img width="48" height="48" src="' + response.avatar[0] + '" alt="' + userFirstNameAbbr.slice(0, 1) + ' ' + userLastNameAbbr.slice(0, 1) + '" />'
								}
								else{
									userAvatar = '<figure class="user-avatar mr-double">' +
						                    '<span class="user-avatar_name">' + userFirstNameAbbr.slice(0, 1) + '</span>' +
						                    '<span class="user-avatar_name">' + userLastNameAbbr.slice(0, 1) + '</span>' +
						                   '</figure>';
								}

								let sidePageData = {
									headerText:  userAvatar + response.first_name + ' ' + response.last_name,
									bodyContent: bodyContent,
									closeActionText: 'Закрыть',
									applyActionText: ''
								}
								let sidepage = new Sidepage(sidePageData);
								//document.querySelector('#post-container .mdl-grid').prepend(editPostBtn);

							}
					});
			});





/// DELETE USER

	$(document).on('click', '.delete-user', function(){
		var post_id = $(this).attr("post-id"); //this is the post id
		// var post_name = $(this).attr("post-name"); //this is the post id
		// var post_role = $(this).attr("post-role"); //this is the post id
		$.ajax({
				type: 'POST',
				url: MyAjax.ajaxurl,
				dataType: "json", // add data type
				data: { action : 'view_ajax_user', userId: post_id},
				success: function(response) {
					console.log(response);

				 let dialogText = document.querySelector("#confirm-dialog_text");
				 console.log(dialogText);
				 dialogText.innerHTML = response.first_name[0] + ' ' + response.last_name[0] + ' - ' + response.user_role[0];
				 $('#confirm-dialog').modal('open');
				}
		});

		$( "#confirm_dialog_action" ).click(function() {
			$('#confirm-dialog').modal('close')
			$.ajax({
					type: 'POST',
					url: MyAjax.ajaxurl,
					dataType: "json", // add data type
					data: { action : 'delete_ajax_user', user_id: post_id},
					success: function( response ) {
							M.toast({html: 'Пользователь удален', classes: 'toast-success', displayLength: 3000})
					}
			});
		});

});

























});
