<?php /* Template Name: General Settings */ ?>
<?php get_header();?>
<?php if (is_user_logged_in()): ?>


<div class="flex-row flex-align-stretch flex-grow">
	<section class="page-content flex-column flex-grow card">
		<div class="flex-grow">
			<?php
				wp_nav_menu( [
					'menu' => 'my service',
					'container' => 'ul',
					'menu_class' => 'tabs-pages',
				 ]);
			?>
			<header class="page-header flex-row flex-align-space-between">
				<section><h1 class="page-title"><?php echo the_title() ?></h1></section>
				<section class="flex-row flex-justify-center"></section>
				<section class="flex-row flex-justify-end"><button disabled class="waves-effect waves-light btn">Сохранить</button></section>
			</header>

    <form>
			<div class="row">
				<div class="input-field col s12">
					<input type="text" class="validate">
          <label for="first_name">Название сервиса *</label>
				</div>
			</div>

			<div class="row">
				<figure class="user-avatar" data-attr="">
					<img class="user-avatar" id="saved-image-preview" src="" width="48" height="48" alt="">
					<img class="user-avatar hide" id="attach-image-preview" src="" width="48" height="48"  alt="Image preview...">
					<button type="button" id="remove-avatar-button" class="icon-button"><i class="material-icons">close</i></button>
				</figure>
				<div class="file-field input-field col s8">
					<div class="btn btn-secondary">
						<span>Выбрать файл</span>
						<input type="hidden" name="MAX_FILE_SIZE" value="30000000" />
						<?php wp_nonce_field( 'my_file_upload', 'fileup_nonce' ); ?>
						<input type="file" id="my_file_upload" name="my_file_upload" onchange="previewFile()"  multiple="false" accept="image/*">
					</div>
					<div class="file-path-wrapper">
						<input id="selected-file-input" class="file-path validate" type="text" placeholder="Файл не выбран" name="user_avatar">
						<label for="user_avatar" hidden>Логотип сервиса</label>
					</div>
				</div>

			</div>

			<div class="row">
				<div class="input-field col s12">
					<input placeholder="(___)-___-__-__" type="number" class="validate">
          <label for="first_name">Номер телефона *</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<input type="text">
          <label for="first_name">Адрес</label>
				</div>
			</div>

			<input type="submit" id="data-form" hidden>
    </form>

		</div>
		<?php get_template_part( 'custom', 'footer' ); ?>
	</section>
</div>

<?php endif; ?>

<?php get_footer(); ?>
