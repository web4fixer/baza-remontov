
<?php get_header() ; ?>

	<?php
	$data = get_queried_object();
	$user_data = get_userdata($data->name);
	$user_meta = get_user_meta($user_data->ID);

$posts_ids = get_posts(array(
    'post_type' => 'post',
		'fields'          => 'ids', // Only get post IDs
    'posts_per_page'  => -1,
    'tax_query' => array(
        array(
        'taxonomy' => 'masters_taxonomy',
        'field' => 'slug',
        'terms' => $user_data->ID
			)
    ))
);


$get_active_orders = get_my_orders($user_data->ID, $posts_ids, array("3","5","36","34","2","35"));
$get_inactive_orders = get_my_orders($user_data->ID, $posts_ids, array("33","4","261"));
echo "<script>window.active_orders = ".json_encode($get_active_orders)."</script>";
echo "<script>window.inactive_orders = ".json_encode($get_inactive_orders)."</script>";


	 ?>




	 <section class="flex-column flex-grow">
		 <div class="page-content card">
			 <div class="flex-row">
				 <img class="materialboxed mr-double user-profile_avatar" width="128" src="<?php echo $user_meta['avatar'][0] ?>">

				 <div class="flex-column flex-grow">
					<h3 class="page-title profile__name">
					 <?php echo $user_data->first_name . ' ' . $user_data->last_name ?>
				 </h3>
				 <div class="flex-row flex-wrap">

						<div class="description-list m-right-double" style="margin-left: 0">
							 <span class="description-list__key">Должность</span>
							 <span class="description-list__value"><?php echo $user_meta['user_role'][0] ?></span>
						</div>
						<div class="description-list m-right-double">
							 <span class="description-list__key">Телефон</span>
							 <a href="tel:<?php echo $user_meta['user_tel_number'][0] ?>" class="description-list__value"><?php echo $user_meta['user_tel_number'][0] ?></a>
						</div>
						<div class="description-list m-right-double">
							 <span class="description-list__key">Почта</span>
							 <a href="mailto:<?php echo $user_data->user_email ?>" class="description-list__value"><?php echo $user_data->user_email ?></a>
						</div>
						<div class="description-list m-right-double">
							 <span class="description-list__key">Зарплата</span>
							 <span class="description-list__value"><?php echo $user_meta['user_salary'][0] ?> + <?php echo $user_meta['user_salary_percent'][0] ?>%</span>
						</div>
						<div class="description-list m-right-double">
							 <span class="description-list__key">Логин</span>
							 <span class="description-list__value"><?php echo $user_data->user_login ?></span>
						</div>
						</div>
					</div>
					<div class="flex-row">
						<div class="flex-grow right-align m-right-double">
							<div class="flex-column">
								 <span class="m-b-half secondary-text">Залогинился <?php echo human_time_diff($user_meta['last_login'][0]) ?> назад</span>
								 <span class="secondary-text">Нанят <?php echo date_i18n( 'd F Y', strtotime($user_data->user_registered) ); ?></span>
							</div>
						</div>
						<button class="icon-button waves-effect"><i class="material-icons">edit</i></button>
					</div>
			 </div>

			 <div class="divider" style="margin: 16px 0 8px 0"></div>

			 <header class="page-header flex-row flex-align-space-between">
	 			<section><h1 class="page-title">Заказы</h1></section>
	 			<section class="flex-row flex-justify-center"><div id="search-table"></div></section>
				<section class="flex-row flex-justify-end">
	        <button type="button" onClick="table.addOrder()" class="waves-effect waves-light btn hide-on-small-only">+ Заказ</button>
	        <button type="button" onClick="table.addOrder()" class="icon-button hide-on-med-and-up"><i class="material-icons">add</i></button>
	      </section>
	 		</header>


			<ul id="tabs" class="tabs">
				<li class="tab"><a id="active_orders" href="#active_orders-block" class="active">Активные <span id="active_orders_counter"></span></a></li>
				<li class="tab"><a id="inactive_orders" href="#inactive_orders-block">Завершенные <span id="inactive_orders_counter"></span></a></li>
			</ul>

			<div id="active_orders-block"></div>
			<div id="inactive_orders-block"></div>

		<div id="orders" class="flex-grow flex-column">
			<div id="content-filter"></div>
			<div id="content" class="flex-grow"></div>
		</div>





			 	<?php get_template_part( 'custom', 'footer' ); ?>
		</div>

 	</section>



	<script type="text/javascript">
	  var tabs = document.getElementById("tabs");
	  var instance = M.Tabs.init(tabs, {onShow: showOrders});
	  var tableFilter
	  var table
	  showOrders()

	  function showOrders(){
	    var attr = instance.$activeTabLink[0].getAttribute("id");
	    tableFilter = new TableFilter(window[attr], "#content-filter", "#content");
	    table = new Datatable(window[attr], "#content", tableFilter, document.querySelector('#' + attr + '_counter'));
	  }
	</script>

<?php get_footer(); ?>
