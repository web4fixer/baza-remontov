<?php




function get_my_orders($user_id, $posts_ids, $from_cats) {

  if($_POST['ids']){
    $posts_ids =  explode(',', $_POST['ids']);
  }

  $group_masters = array();

  $user_groups = wp_get_object_terms($user_id, 'user_position');


  $users = get_terms( array(
      'taxonomy' => 'user_position',
      'parent' => $user_groups[0]->term_id,
      'hide_empty' => false
  ) );


  foreach ( $users as $user ){
    $user_data = get_userdata($user->name);
    $user_id = $user_data->ID;
    $user_term = get_term_by( 'name', $user_id, 'user_position' ); // masters_taxonomy
    $user_meta = get_user_meta($user_id);
    $user_term_url = get_term_link( (int) $user_term->term_id, 'user_position'); // same post apge exists for masters_taxonomy

    $group_masters[$user_meta['first_name'][0]] = [$user_id, $user_meta['avatar'][0], $user_term_url];

  }




  //$clients_parent_term = get_term_by('name',$user_groups[0]->name,'clients_taxonomy');
  //$client_exist = term_exists( $_POST['client_tel_number'], 'clients_taxonomy' );

  $clients_parent_term = get_term_by('name',$user_groups[0]->name,'clients_taxonomy');

  $clients_terms = get_terms('clients_taxonomy', array(
    'orderby' => 'name',
    'hide_empty' => 0,
    'parent' => $clients_parent_term->term_id,
  ));

  $clients = array();
  foreach ( $clients_terms as $term ){
    $term_fio = get_term_meta( $term->term_id, 'client_fio', true );
    $clients[$term_fio] = $term->name;
  }

  $response = array();
  $page_data = array();
  $page_params = array(

    "ids" => $posts_ids,
    "getDataFunctionName" => "get_my_orders_ajax",

    "saveFunctionName" => "create_my_order",
    "addDialogTitle" => "Добавить заказ",
    "addToastSuccessText" => "Заказ добавлен",
    "addToastFailText" => "Не удалось добавить заказ",

    "getFunctionName" => "get_order_data",
    "getFunctionTitle" => "Заказ",

    "updateFunctionName" => "update_order",
    "updateDialogTitle" => "Редактировать заказ",
    "updateToastSuccessText" => "Заказ обновлен",
    "updateToastFailText" => "Не удалось обновить заказ",

		"deleteFunctionName" => "delete_order",
    "deleteDialogTitle" => "Удалить заказ",
    "deleteToastSuccessText" => "Заказ удален",
    "deleteToastFailText" => "Не удалось удалить заказ",

    "addCommentFunctionName" => "add_comment",
    "addCommentToastSuccessText" => "Комментарий добавлен",
    "addCommentToastFailText" => "Не удалось добавить комментарий",

    "deleteCommentFunctionName" => "delete_comment",
    "deleteCommentDialogTitle" => "Удалить комментарий?",
    "deleteCommentToastSuccessText" => "Комментарий удален",
    "deleteCommentToastFailText" => "Не удалось удалить комментарий",

    "updateCommentFunctionName" => "update_comment",
    "updateCommentToastSuccessText" => "Комментарий обновлен",
    "updateCommentToastFailText" => "Не удалось обновить комментарий",

    "сhangeStatusFunctionName" => "change_order_status",
    "сhangeStatusToastSuccessText" => "Статус обновлен",
    "сhangeStatusToastFailText" => "Не удалось обновить статус",

    "сhangeMasterFunctionName" => "set_master_for_order",
    "сhangeMasterToastSuccessText" => "Мастер назначен",
    "сhangeMasterToastFailText" => "Не удалось назначить мастера",

    "dialogCancelButtonText" => "Отмена",
    "dialogApplyButtonText" => "Сохранить",
    "dialogDeleteButtonText" => "Удалить",
    "pageTemplate" => "orders",
    "formId" => "data-form",
    "empty_text" => "Тут пока пусто",
    "columns" => array(
                       "#",
                       "Мастер",
                       "Устройство",
                       "Статус",
                       "Срок ремонта",
                       "Заявленная неисправность",
                       "Стоимость",
                       "Принят",
                       "Изменен",
                       "Действия"),
   "masters" => $group_masters,
   "clients" => $clients,
   "device_trouble_client_says" => array(
                       'Быстро разряжается' => null,
                       'Не включается' => null,
                       'Заблокирован' => null,
                       'Не заряжается' => null,
                       'Выключается' => null,
                       'Разбит дисплей' => null,
                       'Нет звука' => null,
                       'Упал в воду' => null,
                     ),
    "client_source"  => array('Наружная реклама','По рекомендации','Реклама на сайте','Facebook','Google','Instagram','YouTube'),
  //  "all_services" => make_works_tree_format(get_all_works($user_id)),
    //"all_spares" => make_spares_tree_format(get_all_spares($user_id))
  );


  $cats_obj = array();
  // Get all cats list, get its names by id
  $cats = get_categories(array(
    'taxonomy' => 'category',
    'hide_empty' => false,
    'include' => $from_cats,  // active categories
    )
  );

  foreach( $cats as $cat ){
    $cats_obj[$cat->name] = 0;
  }

  $all_cats_obj = array();
  $all_cats = get_categories(array(
    'taxonomy' => 'category',
    'hide_empty' => false,
    'include' => array("3","5","36","34","2","35","33","4","261"),  // all categories
    )
  );

  foreach( $all_cats as $all_cat ){
    $all_cats_obj[$all_cat->name] = 0;
  }




  $posts_array = get_posts( array(
    'numberposts' => 0,
    'category'    => $from_cats,
    'orderby'     => 'date',
    'post__in' =>  $posts_ids,
    'meta_key'    => '',
    'nopaging'    => true,
    'include'     => '',
    'meta_value'  =>'',
    'post_type'   => 'post',
    'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
    'tax_query' => array(
      array(
        'taxonomy' => 'user_position',
        'field' => 'term_id',
        'terms' =>  array($user_groups[0]->term_id),
        'include_children' => false
      )
    )
  ) );


  $metadata = array();

if($posts_array){
  foreach ( $posts_array as $post ){


    $post_data = array();
    $id = $post->ID;
    $post_data['ID'] = $id;
    $post_master = wp_get_post_terms( $id, 'masters_taxonomy' );

    $post_category = get_the_category($post->{"ID"});
    $post_data['status'] = $post_category[0]->{"name"};
    $post_data['status_id'] = $post_category[0]->{"cat_ID"};

    $master_meta = get_user_meta($post_master[0]->{"name"});
    $master_name = $master_meta['first_name'][0];
    $post_data['master'] = $master_meta['first_name'][0];

    $user = get_user_by( 'id', $post->{"post_author"} );
    $post_data['creator'] = $user->{"data"}->{"display_name"};
    $post_data['comments'] = $post->{"comment_count"};



    // Get works and spares
    $works = wp_get_post_terms( $id, 'works_taxonomy');
    $works_selected_ids = array();

    foreach ( $works as $work ){
      array_push($works_selected_ids, strval($work->term_id));
    }

    $spares = wp_get_post_terms( $id, 'repair_parts_taxonomy');
    $spares_selected_ids = array();

    foreach ( $spares as $spare ){
      array_push($spares_selected_ids, strval($spare->term_id));
    }

    $post_data['services'] = $works;
    $post_data['selected_services'] = $works_selected_ids;
    $post_data['spares'] = $spares;
    $post_data['selected_spares'] = $spares_selected_ids;

    // $post_data['creation_date'] = date_i18n('Y-m-d, H:i', strtotime($post->{"post_date"}));
    // $post_data['creation_date2'] = date_i18n('Y,m,d,H,i', strtotime($post->{"post_date"}));
    $post_data['creation_date'] = $post->{"post_date"};
    $post_data['edit_date'] = $post->{"post_modified"};
    $post_data['device_name'] = $post->{"post_title"};

    $post_data['device_trouble_client_says'] = $post->{"post_content"};

    $post_meta = get_post_meta($id);

    foreach($post_meta as $key => $value) {
      $post_data[$key] = $value[0];
    }

    array_push($metadata, $post_data);


    $post_cats = wp_get_post_categories($id);
    $current_cat_name = get_the_category_by_ID($post_cats[0]);
    $cats_obj[$current_cat_name] = $cats_obj[$current_cat_name] + 1;

  }
}


$page_params['categories'] = $cats_obj;
$page_params['from_cats_ids'] = $from_cats;
$page_params['all_categories'] = $all_cats_obj;

$response['page_data'] = $metadata;
$response['params'] = $page_params;
return $response;

}




















function get_my_orders_ajax() {
  echo json_encode(get_my_orders(get_current_user_id(), $_POST['ids'], $_POST['from_cats']));
  exit;
}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_my_orders_ajax', 'get_my_orders_ajax');
















function update_order() {
  $cur_user_id = get_current_user_id();
  $user_groups = wp_get_object_terms($cur_user_id, 'user_position');
  if($_POST['remont_ready_date']){
    $remont_ready_date = date_i18n('Y-m-d', strtotime($_POST['remont_ready_date']));
  }

  $post_meta_fields = array(
    'serial_number' => $_POST['serial_number'],
    'device_looks' => $_POST['device_looks'],
    'complectation' => $_POST['complectation'],
    'remont_price' => $_POST['remont_price'],
    'remont_ready_date' => $remont_ready_date,
    'price' => $_POST['price'],
    'urgent_remont' => $_POST['urgent_remont'],
  );

  $post_data = array(
  	'ID'             => $_POST['post_id'],
    'post_title'     => $_POST['device_name'],
  	'post_content'   => $_POST['device_trouble_client_says'],
  	'post_status'    => 'publish',
  	'post_type'      => 'post',                                   // Метки поста (указываем ярлыки, имена или ID).
  	'tax_input'      => array( 'user_position' => array($user_groups[0]->name)), // К каким таксам прикрепить запись. Аналог 'post_category', только для для новых такс.                                                  //?
  	'meta_input'     => $post_meta_fields
);

  wp_set_object_terms($_POST['post_id'], $_POST['master'], 'masters_taxonomy');

  wp_delete_object_term_relationships( $_POST['post_id'], 'works_taxonomy' );
  wp_set_object_terms($_POST['post_id'], json_decode($_POST['services']), 'works_taxonomy');

  // foreach ( json_decode($_POST['services']) as $item ){
  //   wp_update_term($item, 'works_taxonomy',array('count' => 1 );
  // }

  wp_delete_object_term_relationships( $_POST['post_id'], 'repair_parts_taxonomy' );
  wp_set_object_terms($_POST['post_id'], json_decode($_POST['spares']), 'repair_parts_taxonomy');

  // способ редактировать поля клиента из заказа
  $post_client_data = wp_get_object_terms($_POST['post_id'], 'clients_taxonomy');
  $post_client_id = $post_client_data[0]->term_id;
  update_term_meta( $post_client_id, 'client_source', $_POST['client_source']);
  update_term_meta( $post_client_id, 'client_fio', $_POST['fio_clienta']);
  update_term_meta( $new_term['term_id'], 'tel', $_POST['client_tel_number']);
  // wp_update_term( $post_client_id, 'clients_taxonomy', array(
  //  	'name' => $_POST['client_tel_number']
  //  ));
   //////

  $post_id = wp_update_post( $post_data );
//  print_r (json_decode($_POST['services']));
  //exit;
  //echo json_encode($post_id);

}

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_update_order', 'update_order');








function create_my_order() {
  $cur_user_id = get_current_user_id();
  $user_meta = get_user_meta($cur_user_id);
  $user_groups = wp_get_object_terms($cur_user_id, 'user_position');


  $term = get_term( $user_groups[0]->term_id, 'user_position');
  $orders_count = intval($term->count) + 1;
  wp_update_term($user_groups[0]->term_id, 'user_position',
   array(
    'count' => $orders_count
    )
  );

  $remont_ready_date = '';
  if($_POST['remont_ready_date']){
    $remont_ready_date = date_i18n('Y-m-d', strtotime($_POST['remont_ready_date']));
  }
  else{
    $remont_ready_date = "";
  }
  $post_meta_fields = array(
    'serial_number' => $_POST['serial_number'],
    'device_looks' => $_POST['device_looks'],
    'complectation' => $_POST['complectation'],
    'remont_price' => $_POST['remont_price'],
    'remont_ready_date' => $remont_ready_date,
    'price' => $_POST['price'],
    'urgent_remont' => $_POST['urgent_remont'],
    'order_number' => $orders_count
  );



  $post_data = array(
  	'ID'             => 0,
    'post_title'     => $_POST['device_name'],
  	'post_content'   => $_POST['device_trouble_client_says'],
  	'post_status'    => 'publish',
  	'post_type'      => 'post',
  	'post_category'  => array(2), // категория - новые                                        // Метки поста (указываем ярлыки, имена или ID).
  	'tax_input'      => array( 'user_position' => array($user_groups[0]->name)), // К каким таксам прикрепить запись. Аналог 'post_category', только для для новых такс.                                                  //?
  	'meta_input'     => $post_meta_fields                            // добавит указанные мета поля. По умолчанию: ''. с версии 4.4.
);

  $post_id = wp_insert_post( $post_data );

  if ( $post_id && ! is_wp_error( $post_id ) ) {



    if($_POST['notes']){
      $comment_data = [
        'comment_author'  => $user_meta['first_name'][0],
        'comment_post_ID'  => $post_id,
        'comment_content'  => $_POST['notes'],
        'user_id'          => $cur_user_id
      ];
      wp_insert_comment( wp_slash($comment_data) );
    }


    wp_set_object_terms($post_id, $_POST['master'], 'masters_taxonomy');
    wp_set_object_terms($post_id , $user_groups[0]->name,'user_position');




    wp_set_object_terms($post_id, json_decode($_POST['services']), 'works_taxonomy');
    wp_set_object_terms($post_id, json_decode($_POST['spares']), 'repair_parts_taxonomy');



    $clients_parent_term = get_term_by('name',$user_groups[0]->name,'clients_taxonomy');

    $client_exist = term_exists( $_POST['client_tel_number'], 'clients_taxonomy' );

    $current_time = time();

    if(!$client_exist){
       $new_term = wp_insert_term(
        $current_time,  // новый термин
       'clients_taxonomy', // таксономия
       array(
         'parent'      => $clients_parent_term->term_id
       )
     );
     wp_set_object_terms($post_id, $new_term['term_id'],'clients_taxonomy');
     update_term_meta( $new_term['term_id'], 'client_source', $_POST['client_source']);
     update_term_meta( $new_term['term_id'], 'tel', $_POST['client_tel_number']);
     update_term_meta( $new_term['term_id'], 'client_fio', $_POST['fio_clienta']);

    }
    else{
      $term_id = intval($client_exist['term_id']);
      wp_set_object_terms($post_id, $term_id,'clients_taxonomy');
      update_term_meta( $term_id, 'client_source', $_POST['client_source']);
      update_term_meta( $term_id, 'client_fio', $_POST['fio_clienta']);
    }
   //if (isset($_POST['Email_клиента']) or isset($_POST['Email клиента']) or isset($_POST['Email клиента']) ) {

   //}
}

  echo json_encode($post_id);
  exit;
}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_create_my_order', 'create_my_order');













function get_order_data() {

    $id = $_POST['post_id'];
    $user_groups = wp_get_object_terms($user_id, 'user_position');
/////////////////////////////////////////////////////////////////////

    $works = wp_get_post_terms( $id, 'works_taxonomy');
    $works_selected_ids = array();

    foreach ( $works as $work ){
      $work->type = "build_circle";
      $work->data = array(
        "name" => $work->name,
        "price" => $work->description
      );
    }

    $works_data = array();
    foreach ( $works as $key=>$item ){
      $works_data[$item->term_id] = $item;
    }


//////////////////////////////////////////////////////
    $spares = wp_get_post_terms( $id, 'repair_parts_taxonomy');
    $spares_selected_ids = array();

    foreach ( $spares as $spare ){
      $spare->type = "grid_view";
      $spare->data = array(
        "name" => $spare->name,
        "price" => $spare->slug,
        "description" => $spare->description
      );
    }

    $spares_data = array();
    foreach ( $spares as $key=>$item ){
      $spares_data[$item->term_id] = $item;
    }



    $post = get_post($id);
    $post_comments = get_comments(array( 'post_id' => $id, 'count' => false, 'orderby' => array('comment_date'=>'DESC') ));

    foreach ( $post_comments as $post_comment ){
      $user_meta = get_user_meta($post_comment->user_id);
      $post_comment->{"user_avatar"} = $user_meta['avatar'][0];
      $post_comment->{"comment_date"} = date_i18n('Y-m-d H:i', strtotime($post_comment->{"comment_date"}));
    }

    $client_term = wp_get_post_terms( $id, 'clients_taxonomy', array('fields' => 'all') );
    $client_id = $client_term[0]->term_id;
    $post_category = get_the_category($post->{"ID"});
    $user = get_user_by( 'id', $post->{"post_author"} );
    $post_last_edited_by = get_user_by( 'id', $post->{"_edit_last"} );

    $post_master = wp_get_post_terms( $id, 'masters_taxonomy' );
    $master_meta = get_user_meta($post_master[0]->{"name"});

    $post_data = array(
      "ID"=> $id,
      "client_tel_number"=> get_term_meta( $client_id, 'tel', true ),
      "client_fio"=> get_term_meta( $client_id, 'client_fio', true ),
      "client_source"=> get_term_meta( $client_id, 'client_source', true ),
      "creator"=> $user->{"data"}->{"display_name"},
      "comments"=> $post->{"comment_count"},
      "comments_list"=> $post_comments,
      "creation_date"=> date_i18n('Y-m-d H:i', strtotime($post->{"post_date"})),
      "device_name"=> $post->{"post_title"},
      "last_changed"=> date_i18n('Y-m-d H:i', strtotime($post->{"post_modified"})),
      "device_trouble_client_says"=> $post->{"post_content"},
      "change_by"=> $post_last_edited_by->display_name,
      "status"=> $post_category[0]->{"name"},
      "status_id"=> $post_category[0]->{"cat_ID"},
      "master"=> $master_meta['first_name'][0],
      "services"=> $works_data,
      "selected_services"=> $works_selected_ids,
      "spares"=> $spares_data,
      "selected_spares"=> $spares_selected_ids
    );
    $post_meta = get_post_meta($id);
    foreach($post_meta as $key => $value) {
      $post_data[$key] = $value[0];
    }

    echo json_encode($post_data, JSON_UNESCAPED_UNICODE);
	  exit;
}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_order_data', 'get_order_data');


























function delete_order() {
  	$post_id = $_POST['post_id'];
		$object_term_relationships = wp_delete_object_term_relationships( $post_id, 'user_position' );
		$delete_post = wp_delete_post($post_id);
		$obj_merged = (object) array_merge((array) $object_term_relationships, (array) $delete_post);
		echo json_encode($obj_merged);
		exit;
}

add_action('wp_ajax_delete_order', 'delete_order');




function change_order_status() {
    $cur_user_id = get_current_user_id();
  	$post_id = $_POST['post_id'];
    $post_new_status_id = get_cat_ID($_POST['status']);
    wp_set_post_categories($post_id, $post_new_status_id, false);
}

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_change_order_status', 'change_order_status');











function set_master_for_order() {
  //update_post_meta( $_POST['post_id'], 'master', $_POST['master_id']);
  $terms = wp_set_object_terms($_POST['post_id'], $_POST['master_id'], 'masters_taxonomy');
  echo json_encode($terms);
  exit;
}

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_set_master_for_order', 'set_master_for_order');




























?>
