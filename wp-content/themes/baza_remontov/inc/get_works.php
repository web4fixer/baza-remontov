<?php

function get_works() {

  $response = array();
  $page_params = array(

    "getDataFunctionName" => "get_works_ajax",

    "updateCatFunctionName" => "update_work_cat",

    "addCatFunctionName" => "create_work_cat",
    "addCatDialogTitle" => "Добавить категорию",
    "addCatButton" => "+ категория",
    "addCatToastSuccessText" => "Категория добавлена",
    "addCatToastFailText" => "Не удалось добавить категорию",

    "editCatFunctionName" => "update_work_cat",
    "editCatDialogTitle" => "Редактировать категорию",
    "editCatToastSuccessText" => "Категория обновлена",
    "editCatToastFailText" => "Не удалось обновить категорию",

		"deleteCatFunctionName" => "delete_work_cat",
    "deleteCatDialogTitle" => "Удалить категорию",
    "deleteCatToastSuccessText" => "Категория удалена",
    "deleteCatToastFailText" => "Не удалось удалить категорию",

    "addFunctionName" => "create_work_sub_cat",
    "addDialogTitle" => "Добавить работу",
    "addToastSuccessText" => "Работа добавлена",
    "addToastFailText" => "Не удалось добавить работу",

    "updateFunctionName" => "update_work_sub_cat",
    "updateDialogTitle" => "Редактировать работу",
    "updateToastSuccessText" => "Работа обновлена",
    "updateToastFailText" => "Не удалось обновить работу",

    "deleteFunctionName" => "delete_work_sub_cat",
    "deleteDialogTitle" => "Удалить работу?",
    "deleteDialogButton" => "Удалить",
    "deleteToastSuccessText" => "Работа удалена",
    "deleteToastFailText" => "Не удалось удалить работу",

    "dialogCancelButtonText" => "Отмена",
    "dialogApplyButtonText" => "Сохранить",

    "searchFieldText" => "Поиск",
    "columns" => array("Название",
                       "Стоимость"),
    "topLevelCatData" => array(
      "name" => "Название",
      "required" => true,
      "type" => "text",
    ),
		"empty_text" => "Тут пока пусто",
  );


  $response['page_data'] = get_all_works();
  $response['params'] = $page_params;

  return $response;

}







function get_all_works() {

  $user_groups = wp_get_object_terms(get_current_user_id(), 'user_position');
  $user_works_group = get_term_by( 'name', $user_groups[0]->name, 'works_taxonomy' );

  $page_data = array();
  $works_cats = get_terms( array(
      'taxonomy' => 'works_taxonomy',
      'parent' => $user_works_group->term_id,
      'hide_empty' => false
  ) );

  foreach ( $works_cats as $cat ){
    $new_array = array(
      "cat_id" => $cat->term_id,
      "name" => $cat->name,
      "data" => array(    "value" => $cat->name,
                          "name" => "Название",
                          "type" => "text",
                          "required"   => true
                        ),
      "parent_id" => $cat->parent,
      "columns" => array(
        0 => array("name" => "Название",
                  "field_name" => "name",
                  "type" => "text",
                  "required"   => true
                ),
        1 => array("name" => "Стоимость",
                  "field_name" => "price",
                  "type" => "currency",
                  "required"   => true
                ),
      )
    );
    $sub_cats = get_terms( array(
        'taxonomy' => 'works_taxonomy',
        'parent' => $cat->term_id,
        'hide_empty' => false
    ));

    if($sub_cats){
      $new_array['sub_cats'] = array();
      foreach ( $sub_cats as $sub_cat ){
        $new_sub_array = array(
          "cat_id" => $sub_cat->term_id,
          "data" =>  array(
            "name" => $sub_cat->name,
            "price" => $sub_cat->description
          ),
          "parent_id" => $sub_cat->parent,
          "type" => "build_circle",

        );
        $new_array['sub_cats'][$sub_cat->term_id] = $new_sub_array;
      }
    }
    else{
      $new_array['empty_text'] = "Нет записей";
    }

    $page_data[$cat->term_id] = $new_array;
  }

  return $page_data;
}


// function make_works_tree_format($arr) {
//   $data = array();
//   foreach ( $arr as $work ){
//     $new_array = array(
//       "id" => $work['cat_id'],
//       "text" => array_values($work['data'])[0],
//     );
//     if($work['sub_cats']){
//       $new_array['children'] = array();
//       foreach ( $work['sub_cats'] as $sub_work ){
//         $new_sub_array = array(
//           "id" => $sub_work['cat_id'],
//           "text" => $sub_work['name'] . " / " . $sub_work['price'] . " грн",
//           "attributes" => array(
//             "price" => $sub_work['price']
//           )
//         );
//         array_push($new_array['children'], $new_sub_array);
//       }
//     }
//     array_push($data, $new_array);
//   }
//   return $data;
// }


// if($work->sub_cats){
//
// }




function get_works_ajax() {
  echo json_encode(get_works(get_current_user_id()));
  exit;
}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_works_ajax', 'get_works_ajax');


function create_work_cat() {
  $current_user_id = get_current_user_id();
  $user_groups = wp_get_object_terms($current_user_id, 'user_position');
  $user_works_group = get_term_by( 'name', $user_groups[0]->name, 'works_taxonomy' );

  $new_cat = wp_insert_term( $_POST['0'], 'works_taxonomy',array(
     'parent' => $user_works_group->term_id
     )
   );
   echo json_encode($new_cat);
   exit;
}
add_action('wp_ajax_create_work_cat', 'create_work_cat');





function update_work_cat() {
  $upd_cat = wp_update_term( $_POST['postId'], 'works_taxonomy',
   array(
    'name' => $_POST['0']
    )
  );
  echo json_encode($upd_cat);
  exit;
  //wp_die( $upd_cat['term_id'] ); // чтобы сервер прислал id

}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_update_work_cat', 'update_work_cat');





function create_work_sub_cat() {
 $cat_id = wp_insert_term(
   $_POST['0'], 'works_taxonomy',
	 array(
    'parent'      => $_POST['postId'],
		'description' => $_POST['1'],
		)
	);

  $term = get_term($new_sub_cat['term_id'], 'works_taxonomy');

  $new_sub_array = array();

  $new_sub_array[$term->term_id] = array(
      "cat_id" => $term->term_id,
      "data" =>  array(
        "name" => $term->name,
        "description" => $term->description,
      ),
      "parent_id" => $term->parent,
      "type" => "grid_view",
    );

  echo json_encode($new_sub_array);
  exit;
}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_create_work_sub_cat', 'create_work_sub_cat');






function update_work_sub_cat() {
  $upd_cat = wp_update_term( $_POST['cat_id'], 'works_taxonomy',
 	 array(
 		'name' => $_POST['0'],
 		'description'=> $_POST['1'],
 		)
  );
  echo json_encode($upd_cat);
  exit;
}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_update_work_sub_cat', 'update_work_sub_cat');




function delete_work_cat() {
    // Query Arguments
  	$post_id = $_POST['postId'];

    $term_children = get_term_children( $post_id, 'works_taxonomy' );
    if ( !empty( $term_children ) ) {
        foreach ( $term_children as $term_child ) {
            wp_delete_term( $term_child, 'works_taxonomy' );
        }
    }

		$deleted_term = wp_delete_term( $_POST['postId'], 'works_taxonomy' );
		echo json_encode($deleted_term);
		exit;
}

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_delete_work_cat', 'delete_work_cat');





function delete_work_sub_cat() {
    // Query Arguments
  	$post_id = $_POST['postId'];
		$deleted_term = wp_delete_term( $_POST['postId'], 'works_taxonomy' );
		echo json_encode($deleted_term);
		exit;
}

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_delete_work_sub_cat', 'delete_work_sub_cat');



?>
