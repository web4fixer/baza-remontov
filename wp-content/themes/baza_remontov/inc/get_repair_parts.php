<?php

function get_repair_parts() {

  $response = array();

  $page_params = array(
		"getDataFunctionName" => "get_repair_parts_ajax",

    "addCatFunctionName" => "create_repair_part_cat",
    "addCatDialogTitle" => "Добавить категорию",
    "addCatButton" => "+ категория",
    "addCatToastSuccessText" => "Категория добавлена",
    "addCatToastFailText" => "Не удалось добавить категорию",

		"editCatFunctionName" => "update_repair_part_cat",
    "editCatDialogTitle" => "Редактировать категорию",
    "editCatToastSuccessText" => "Категория обновлена",
    "editCatToastFailText" => "Не удалось обновить категорию",

		"deleteCatFunctionName" => "delete_repair_part_cat",
    "deleteCatDialogTitle" => "Удалить категорию",
    "deleteCatToastSuccessText" => "Категория удалена",
    "deleteCatToastFailText" => "Не удалось удалить категорию",

    "addFunctionName" => "create_repair_part",
    "addDialogTitle" => "Добавить",
    "addToastSuccessText" => "Запчасть добавлена",
    "addToastFailText" => "Не удалось добавить запчасть",

    "updateFunctionName" => "update_repair_part",
    "updateDialogTitle" => "Редактировать запчасть",
    "updateTooltipText" => "Редактировать",
    "updateToastSuccessText" => "Запчасть обновлена",
    "updateToastFailText" => "Не удалось обновить запчасть",

    "importFunctionName" => "import_repair_part",
    "importToastSuccessText" => "Запчасти импортированы",
    "importTooltipText" => "Импорт",
    "importToastFailText" => "Не удалось импортировать запчасти",

    "exportTooltipText" => "Скачать",

    "deleteFunctionName" => "delete_repair_part",
    "deleteDialogTitle" => "Удалить запчасть?",
    "deleteTooltipText" => "Удалить",
    "deleteDialogButton" => "Удалить",
    "deleteToastSuccessText" => "Запчасть удалена",
    "deleteToastFailText" => "Не удалось удалить запчасть",

    "dialogCancelButtonText" => "Отмена",
    "dialogApplyButtonText" => "Сохранить",


    "searchFieldText" => "Поиск",
    "columns" => array("Название",
                       "Стоимость"),
    "topLevelCatData" => array(
      "name" => "Название",
      "required" => true,
      "type" => "text",
    ),
		"empty_text" => "Тут пока пусто",

  );

  $response['page_data'] = get_all_spares();
  $response['params'] = $page_params;

  return $response;

}


function import_repair_part() {
  $data = json_decode(stripslashes($_POST['data']));
  foreach ( $data as $subcat ){
    $term = term_exists($subcat[0], 'repair_parts_taxonomy', intval($_POST['cat_id']) );
    if ( $term !== 0 && $term !== null ) {
      $new_cat = wp_update_term($term['term_id'], 'repair_parts_taxonomy',
       array(
        'name' => $subcat[0],
        'description' => $subcat[1],
        )
      );
    }
    else{
      $new_cat = wp_insert_term($subcat[0], 'repair_parts_taxonomy',
       array(
        'name' => $subcat[0],
        'description' => $subcat[1],
        'parent'      => intval($_POST['cat_id']),
        )
      );
    }
  }
}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_import_repair_part', 'import_repair_part');









function get_all_spares() {

  $user_groups = wp_get_object_terms(get_current_user_id(), 'user_position');
  $user_repair_parts_group = get_term_by( 'name', $user_groups[0]->name, 'repair_parts_taxonomy' );
  $page_data = array();
  $cats = get_terms( array(
      'taxonomy' => 'repair_parts_taxonomy',
      'parent' => $user_repair_parts_group->term_id,
      'hide_empty' => false
  ) );

  $existing_spares = array();

  if($_POST['selected_ids']){
    $existing_spares = json_decode($_POST['selected_ids']);
  }




  if($cats){
    foreach ( $cats as $cat ){



      $new_array = array(
        "cat_id" => $cat->term_id,
        "name" => $cat->name,
        "data" => array(    "value" => $cat->name,
                            "name" => "Название",
                            "type" => "text",
                            "required"   => true
                          ),
        "parent_id" => $cat->parent,
        "sub_cats" => array(),
        "columns" => array(
          0 => array("name" => "Название",
                    "field_name" => "name",
                    "type" => "text",
                    "required"   => true
                  ),
          1 => array("name" => "Стоимость",
                    "field_name" => "price",
                    "type" => "currency",
                    "required"   => true
                  )
        )
      );

      $sub_cats = get_terms( array(
          'taxonomy' => 'repair_parts_taxonomy',
          'parent' => $cat->term_id,
          'hide_empty' => false,
          'pad_counts' => true
      ));


      if($sub_cats){
        $new_array['sub_cats'] = array();
        foreach ( $sub_cats as $sub_cat ){
          if($sub_cat->count == 0 || in_array(intval($sub_cat->term_id), $existing_spares)){
            //if($sub_cat->count == 0 || in_array($sub_cat->term_id, $existing_works)){
            $new_sub_array = array(
              "cat_id" => $sub_cat->term_id,
              "data" =>  array(
                "name" => $sub_cat->name,
                "price" => $sub_cat->description,
              ),
              "parent_id" => $sub_cat->parent,
              "type" => "grid_view",
            );
            $new_array['sub_cats'][$sub_cat->term_id] = $new_sub_array;
          }
        }
      }
      else{
        $new_array['empty_text'] = "Нет записей";
      }
      $page_data[$cat->term_id] = $new_array;
    }
  }

  return $page_data;
}


// function make_spares_tree_format($arr) {
//   $data = array();
//   foreach ( $arr as $work ){
//     $new_array = array(
//       "id" => $work['cat_id'],
//       "text" => array_values($work['data'])[0]
//     );
//     if($work['sub_cats']){
//       $new_array['children'] = array();
//       foreach ( $work['sub_cats'] as $sub_work ){
//         $new_sub_array = array(
//           "id" => $sub_work['cat_id'],
//           "text" => $sub_work['name'] . " / " . $sub_work['price'] . " грн",
//           "attributes" => array(
//             "price" => $sub_work['price']
//           )
//         );
//         array_push($new_array['children'], $new_sub_array);
//       }
//     }
//     array_push($data, $new_array);
//   }
//   return $data;
// }


function get_repair_parts_ajax() {
  echo json_encode(get_repair_parts(get_current_user_id()));
  exit;
}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_repair_parts_ajax', 'get_repair_parts_ajax');


function create_repair_part_cat() {
  $current_user_id = get_current_user_id();
  $user_groups = wp_get_object_terms($current_user_id, 'user_position');
  $user_repair_parts_group = get_term_by( 'name', $user_groups[0]->name, 'repair_parts_taxonomy' );

  $new_cat = wp_insert_term( $_POST['0'], 'repair_parts_taxonomy',array(
     'parent' => $user_repair_parts_group->term_id
     )
   );
   echo json_encode($new_cat);
   exit;
}
add_action('wp_ajax_create_repair_part_cat', 'create_repair_part_cat');





function update_repair_part_cat() {
  $upd_cat = wp_update_term( $_POST['postId'], 'repair_parts_taxonomy',
   array(
    'name' => $_POST['0']
    )
  );
  echo json_encode($upd_cat);
  exit;
  //wp_die( $upd_cat['term_id'] ); // чтобы сервер прислал id

}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_update_repair_part_cat', 'update_repair_part_cat');





function create_repair_part() {
 $new_sub_cat = wp_insert_term( $_POST['0'], 'repair_parts_taxonomy',
	 array(
		'description' => $_POST['1'],
		'parent'      => $_POST['postId'],
		)
	);

  $term = get_term($new_sub_cat['term_id'], 'repair_parts_taxonomy');

  $new_sub_array = array();

  $new_sub_array[$term->term_id] = array(
      "cat_id" => $term->term_id,
      "data" =>  array(
        "name" => $term->name,
        "price" => $term->description
      ),
      "parent_id" => $term->parent,
      "type" => "grid_view",
    );

  echo json_encode($new_sub_array);
  exit;

}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_create_repair_part', 'create_repair_part');






function update_repair_part() {
  $upd_cat = wp_update_term( $_POST['cat_id'], 'repair_parts_taxonomy',
 	 array(
 		'name' => $_POST['0'],
 		'description'=> $_POST['1'],
 		)
  );
  echo json_encode($upd_cat);
  exit;
}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_update_repair_part', 'update_repair_part');




function delete_repair_part_cat() {
    // Query Arguments
  	$post_id = $_POST['postId'];

		$term_children = get_term_children( $post_id, 'repair_parts_taxonomy' );

    if ( !empty( $term_children ) ) {
        foreach ( $term_children as $term_child ) {
            wp_delete_term($term_child, 'repair_parts_taxonomy' );
        }
    }

		wp_delete_term( $post_id, 'repair_parts_taxonomy' );

}

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_delete_repair_part_cat', 'delete_repair_part_cat');





function delete_repair_part() {
    // Query Arguments
  	$post_id = $_POST['postId'];
		$deleted_term = wp_delete_term( $_POST['postId'], 'repair_parts_taxonomy' );
		echo json_encode($deleted_term);
		exit;
}

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_delete_repair_part', 'delete_repair_part');


?>
