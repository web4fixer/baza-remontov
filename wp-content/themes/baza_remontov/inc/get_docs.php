<?php

// for uploading avatars

require_once ABSPATH . 'wp-admin/includes/image.php';
require_once ABSPATH . 'wp-admin/includes/file.php';
require_once ABSPATH . 'wp-admin/includes/media.php';

///

function get_my_docs($user_id) {

  $response = array();
  $page_data = array();
  $page_params = array(

    "getDataFunctionName" => "get_my_docs_ajax",

    "saveFunctionName" => "create_my_doc",
    "addDialogTitle" => "Добавить документ",
    "addToastSuccessText" => "Документ добавлен",
    "addToastFailText" => "Не удалось добавить документ",

    "getFunctionName" => "get_doc_data",
    "getFunctionTitle" => "Документ",

    "updateFunctionName" => "update_doc",
    "updateDialogTitle" => "Редактировать документ",
    "editToastSuccessText" => "Документ обновлен",
    "editToastFailText" => "Не удалось обновить документ",

		"deleteFunctionName" => "delete_doc",
    "deleteDialogTitle" => "Удалить документ",
    "deleteToastSuccessText" => "Документ удален",
    "deleteToastFailText" => "Не удалось удалить документ",

    "dialogCancelButtonText" => "Отмена",
    "dialogApplyButtonText" => "Сохранить",
    "dialogDeleteButtonText" => "Удалить",
    "pageTemplate" => "my_docs",
    "formId" => "data-form",
    "columns" => array("Название", "Создал", "Изменен", "Создан", "Действия")
  );


  // $user_groups = wp_get_object_terms($user_id, 'user_position');
  // $user_clients_group = get_term_by( 'name', $user_groups[0]->name, 'user_position' );

  $posts = get_posts( array(
    'orderby'     => 'date',
    'order'       => 'DESC',
    'post_type'   => 'my_doc',
    'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
  ) );


  foreach ( $posts as $post ){

    $user_data = get_userdata($post->post_author);
    $user_id = $user_data->ID;
    $user_meta = get_user_meta($user_id);
    // $term_url = get_term_link( (int) $user->term_id, 'user_position');

    $new_array = array(
      "ID" => $post->ID,
      "creator" => $user_meta['first_name'][0],
      "post_date" => date_i18n('Y-m-d H:i', strtotime($post->post_date)),
      "post_title" => $post->post_title,
      "post_content" => $post->post_content,
      "post_modified" => $post->post_modified,
      "ID" => $post->ID
    );
    array_push($page_data, $new_array);
  }

  $response['page_data'] = $page_data;
  $response['params'] = $page_params;

  return $response;

}


function get_my_docs_ajax() {
  echo json_encode(get_my_docs(get_current_user_id()));
  exit;
}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_my_docs_ajax', 'get_my_docs_ajax');






function create_my_doc() {
  $cur_user_id = get_current_user_id();
  $user_groups = wp_get_object_terms($cur_user_id, 'user_position');
  // Создаем массив данных новой записи
  $post_data = array(
    'ID'           => $_POST['post_id'],
  //  'post_date'    => $_POST['post_date'],
  	'post_title'    => sanitize_text_field( $_POST['post_title'] ),
  	'post_content'  => $_POST['post_content'],
  	'post_status'   => 'publish',
    'post_type'   => 'my_doc',
    //'tax_input'      => array( 'user_position' => array($user_groups[0]->name)),
  );

  // Вставляем запись в базу данных
  $post_id = wp_insert_post( $post_data );
  wp_set_object_terms($post_id , $user_groups[0]->name,'user_position');
}
add_action('wp_ajax_create_my_doc', 'create_my_doc');





function update_doc() {
  // Создаем массив данных
  $my_post = array();
  $my_post['ID'] = $_POST['post_id'];
  $my_post['post_title'] = $_POST['post_title'];
  $my_post['post_content'] = $_POST['post_content'];

// Обновляем данные в БД
  wp_update_post( wp_slash($my_post) );

}

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_update_doc', 'update_doc');








function get_doc_data() {

    $post_id = $_POST['post_id'];
    $post = get_post( $post_id );

    $user_data = get_userdata($post->post_author);
    $user_id = $user_data->ID;
    $user_meta = get_user_meta($user_id);

      $post_data = array(
        "ID" => $post->ID,
        "creator" => $user_meta['first_name'][0],
        "post_date" => date_i18n('Y-m-d H:i', strtotime($post->post_date)),
        "post_title" => $post->post_title,
        "post_content" => $post->post_content,
        "post_modified" => $post->post_modified,
        "ID" => $post->ID
      );

    echo json_encode($post_data, JSON_UNESCAPED_UNICODE);
	  exit;

}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_doc_data', 'get_doc_data');












function delete_doc() {
		$delete_post = wp_delete_post($_POST['post_id']);
		echo json_encode($delete_post);
		exit;
}

add_action('wp_ajax_delete_doc', 'delete_doc');













?>
