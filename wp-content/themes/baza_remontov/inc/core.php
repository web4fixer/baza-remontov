<?php

add_theme_support( 'html5', array( 'search-form' ) );

add_theme_support('widgets');



function add_theme_scripts() {
	wp_enqueue_style( 'materialize', get_template_directory_uri() . '/css/materialize.css', array(), '1.1', 'all');
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	wp_enqueue_script("jquery");

	wp_enqueue_script('materialize', get_template_directory_uri() . '/js/materialize.js');

	wp_enqueue_script('script', get_template_directory_uri() . '/js/script.js', array('jquery'), '1.0.0', true );
	wp_enqueue_script('datatable', get_template_directory_uri() . '/js/datatable.js');
	wp_enqueue_script('catalog', get_template_directory_uri() . '/js/catalog.js');
	wp_enqueue_script('xlsxtojs', get_template_directory_uri() . '/js/xlsx.full.min.js');
	wp_enqueue_script('sortable-table', get_template_directory_uri() . '/js/sortable-table.js');
}

add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );


// Подключаем ajax

function add_ajax_script(){
	wp_enqueue_script( 'my-ajax-request', get_template_directory_uri() . '/js/script.js' );
	wp_localize_script( 'my-ajax-request', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}

add_action( 'wp_enqueue_scripts', 'add_ajax_script' );


# Закрывает все маршруты REST API от публичного доступа
add_filter( 'rest_authentication_errors', function( $result ){

	if( empty( $result ) && ! current_user_can('edit_others_posts') ){
		return new WP_Error( 'rest_forbidden', 'You are not currently logged in.', array( 'status' => 401 ) );
	}

	return $result;
});












?>
