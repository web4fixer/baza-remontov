<?php




function add_comment() {
  $cur_user_id = get_current_user_id();
  $user_meta = get_user_meta($cur_user_id);
  $user_fio = $user_meta['first_name'][0] . ' ' . $user_meta['last_name'][0];

  $data = [
    'comment_author'  => $user_fio,
  	'comment_post_ID'  => $_POST['post_id'],
  	'comment_content'  => $_POST['comment_text'],
  	'user_id'          => $cur_user_id
  ];

  $comment_data = wp_insert_comment( wp_slash($data) );

  echo json_encode($data);
  exit;
}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_add_comment', 'add_comment');




function update_comment() {
  $data = [
    'comment_ID'      => $_POST['comment_id'],
	  'comment_content' => $_POST['comment_text']
  ];
  $comment_data = wp_update_comment( wp_slash($data) );
  echo json_encode($comment_data);

  $updated_comment = get_comment($_POST['comment_id']);
  return $updated_comment;

  exit;

}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_update_comment', 'update_comment');







function delete_comment() {
  $deleted_comment = wp_delete_comment($_POST['post_id']);
  echo json_encode($deleted_comment);
  exit;

}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_delete_comment', 'delete_comment');

?>
