<?php


function get_my_clients($user_id) {

  $response = array();
  $page_data = array();
  $page_params = array(

    "getDataFunctionName" => "get_my_clients_ajax",

    "saveFunctionName" => "create_my_user",
    "addDialogTitle" => "Добавить клиента",
    "addToastSuccessText" => "Клиент добавлен",
    "addToastFailText" => "Не удалось добавить клиента",

    "getFunctionName" => "get_user_data",

    "editFunctionName" => "update_user",
    "updateDialogTitle" => "Редактировать клиента",
    "editToastSuccessText" => "Клиент обновлен",
    "editToastFailText" => "Не удалось обновить клиента",

		"deleteFunctionName" => "delete_user",
    "deleteDialogTitle" => "Удалить клиента",
    "deleteToastSuccessText" => "Клиент удален",
    "deleteToastFailText" => "Не удалось удалить клиента",

    "dialogCancelButtonText" => "Отмена",
    "dialogApplyButtonText" => "Сохранить",
    "dialogDeleteButtonText" => "Удалить",
    "pageTemplate" => "clients",
    "formId" => "data-form",
    "columns" => array("ФИО", "Ремонтов", "Номер телефона", "Откуда узнал о нас", "Действия")
  );



  $current_user = get_current_user_id();
  $user_groups = wp_get_object_terms($current_user, 'user_position');
  $user_clients_group = get_term_by( 'name', $user_groups[0]->name, 'clients_taxonomy' );


  $clients = get_terms( array(
      'taxonomy' => 'clients_taxonomy',
      'parent' => $user_clients_group->term_id,
      'hide_empty' => false
  ) );


  foreach ( $clients as $client ){

    $posts = get_posts(array(
      'post_type' => 'post',
      'tax_query' => array(
          array(
          'taxonomy' => 'clients_taxonomy',
          'field' => 'term_id',
          'terms' => $client->term_id)
      ))
    );

    $client_id = $client->term_id;

    $new_array = array(
      "client_id"=> $client_id,
      "term_url"=> get_term_link( $client, 'clients_taxonomy' ),
      "name"=> get_term_meta( $client_id, 'client_fio', true ),
      "mail"=> get_term_meta( $client_id, 'client_email', true ),
      "source"=> get_term_meta( $client_id, 'client_source', true ),
      "tel"=> get_term_meta( $client_id, 'tel', true ),
      "posts_number"=> sizeof($posts)
    );

    array_push($page_data, $new_array);
  }

  $response['page_data'] = $page_data;
  $response['params'] = $page_params;

  return $response;

}


function get_my_clients_ajax() {
  echo json_encode(get_my_clients());
  exit;
}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_my_clients_ajax', 'get_my_clients_ajax');





?>
