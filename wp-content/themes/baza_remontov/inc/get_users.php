<?php

// for uploading avatars

require_once ABSPATH . 'wp-admin/includes/image.php';
require_once ABSPATH . 'wp-admin/includes/file.php';
require_once ABSPATH . 'wp-admin/includes/media.php';

///

function get_my_users($user_id) {

  $response = array();
  $page_data = array();
  $page_params = array(

    "getDataFunctionName" => "get_my_users_ajax",

    "saveFunctionName" => "create_my_user",
    "addDialogTitle" => "Добавить пользователя",
    "addToastSuccessText" => "Пользователь добавлен",
    "addToastFailText" => "Не удалось добавить пользователя",

    "getFunctionName" => "get_user_data",

    "updateFunctionName" => "update_user",
    "updateDialogTitle" => "Редактировать пользователя",
    "editToastSuccessText" => "Пользователь обновлен",
    "editToastFailText" => "Не удалось обновить пользователя",

		"deleteFunctionName" => "delete_user",
    "deleteDialogTitle" => "Удалить пользователя",
    "deleteToastSuccessText" => "Пользователь удален",
    "deleteToastFailText" => "Не удалось удалить пользователя",

    "dialogCancelButtonText" => "Отмена",
    "dialogApplyButtonText" => "Сохранить",
    "dialogDeleteButtonText" => "Удалить",
    "pageTemplate" => "users",
    "formId" => "data-form",
    "columns" => array("Аватар", "Имя", "Должность", "Почта", "Номер телефона","Нанят", "Действия")
  );






  $user_groups = wp_get_object_terms($user_id, 'user_position');
  $user_clients_group = get_term_by( 'name', $user_groups[0]->name, 'user_position' );

  $users = get_terms( array(
      'taxonomy' => 'user_position',
      'parent' => $user_clients_group->term_id,
      'hide_empty' => false
  ) );
  //
  //
  // $user_groups = wp_get_object_terms($user_id, 'user_position');
  //
  // $users = get_terms( array(
  //     'taxonomy' => 'user_position',
  //     'parent' => $user_groups[0]->term_id,
  //     'hide_empty' => false
  // ) );


  foreach ( $users as $user ){

    $user_data = get_userdata($user->name);
    $user_id = $user_data->ID;
    $user_meta = get_user_meta($user_id);
    $term_url = get_term_link( (int) $user->term_id, 'user_position');

    $new_array = array(
      "user_id"=> $user_id,
      "registered"=>  $user_data->user_registered,
      "name"=> $user_meta['first_name'][0],
      "term_url"=> $term_url,
      "type"=> $user_meta['user_role'][0],
      "avatar" => $user_meta['avatar'][0],
      "mail"=> $user_data->user_email,
      "tel"=> $user_meta['user_tel_number'][0]
    );
    array_push($page_data, $new_array);
  }

  $response['page_data'] = $page_data;
  $response['params'] = $page_params;

  return $response;

}


function get_my_users_ajax() {
  echo json_encode(get_my_users(get_current_user_id()));
  exit;
}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_my_users_ajax', 'get_my_users_ajax');


function create_my_user() {
  // $current_user_id = get_current_user_id();
  // $user_groups = wp_get_object_terms($current_user_id, 'user_position');
  // $user_works_group = get_term_by( 'name', $user_groups[0]->name, 'works_taxonomy' );
  //
  // $new_cat = wp_insert_term( $_POST['0'], 'works_taxonomy',array(
  //    'parent' => $user_works_group->term_id
  //    )
  //  );
  //  echo json_encode($new_cat);
  //  exit;
  $userdata = array(
   'ID'              => $_POST['post_id'],
   'user_pass'       => $_POST['user_password'],
   'user_login'      => $_POST['user_login'],
   'user_nicename'   => '',
   'user_url'        => '',
   'user_email'      => $_POST['user_email'],
   'display_name'    => '',
   'nickname'        => '',
   'first_name'      => $_POST['user_name'],
   'last_name'       => $_POST['user_lastname'],
   'description'     => $_POST['user_description'],
   'rich_editing'    => '', // false - выключить визуальный редактор
   'user_registered' => '', // дата регистрации (Y-m-d H:i:s) в GMT
   'role'            => $_POST['user_role'],
   'jabber'          => '',
   'aim'             => '',
   'yim'             => '',
  );
  $new_user = wp_insert_user( $userdata );
  //echo json_encode($new_user);
  //exit;
}
add_action('wp_ajax_create_my_user', 'create_my_user');


function set_terms_for_ajax_user($user_id) {

		$cur_user_id = get_current_user_id();
		$user_groups = wp_get_object_terms($cur_user_id, 'user_position');
		wp_set_object_terms($user_id , $user_groups[0]->name,'user_position');

		wp_insert_term($user_id, 'user_position', array(
                                            				'description' => $user_id,
                                            				'slug'        => $user_id,
                                            				'parent'      => $user_groups[0]->term_id
                                            			)
		);

    wp_insert_term($user_id, 'masters_taxonomy');

    wp_set_object_terms($post_id, $_POST['master'], 'masters_taxonomy');

		update_user_meta( $user_id, 'user_role', $_POST['user_role']);
		update_user_meta( $user_id, 'user_tel_number', $_POST['user_tel_number']);
		update_user_meta( $user_id, 'user_salary_percent', $_POST['user_salary_percent']);
		update_user_meta( $user_id, 'user_salary', $_POST['user_salary']);

		if (isset($_FILES['img'])) {

			$overrides = array('test_form' => false);
			$attachment_id = wp_handle_upload($_FILES['img'], $overrides );
			update_user_meta( $user_id, 'avatar', $attachment_id[url] );

			// if ( is_wp_error( $attachment_id ) ) {
			// 	echo "Ошибка загрузки медиафайла.";
			// }
			// else {
			// 	echo "Медиафайл был успешно загружен!";
			// }
		}
    else{
      echo json_encode('no image');
    }
}



function update_user() {
  $update_user = wp_update_user( array(
   'ID'              => $_POST['post_id'],
   'user_pass'       => $_POST['user_password'],
   'user_login'      => $_POST['user_login'],
   'user_email'      => $_POST['user_email'],
   'first_name'      => $_POST['user_name'],
   'description'     => $_POST['user_description'],
   'role'            => $_POST['user_role'],
  ) );


  update_user_meta( $_POST['post_id'], 'user_role', $_POST['user_role']);
  update_user_meta( $_POST['post_id'], 'user_tel_number', $_POST['user_tel_number']);
  update_user_meta( $_POST['post_id'], 'user_salary_percent', $_POST['user_salary_percent']);
  update_user_meta( $_POST['post_id'], 'user_salary', $_POST['user_salary']);


  //if($_POST['user_avatar_exists']){
    if (isset($_FILES['img']) && $_POST['avatar']) {
      $overrides = array('test_form' => false);
      $attachment_id = wp_handle_upload($_FILES['img'], $overrides );
      update_user_meta( $_POST['post_id'], 'avatar', $attachment_id[url] );
      // if ( is_wp_error( $attachment_id ) ) {
      //   echo "Ошибка загрузки медиафайла.";
      // }
      // else {
      //   echo "Медиафайл был успешно загружен!";
      // }
    }
//  }
    if(!$_POST['avatar']){
      update_user_meta( $_POST['post_id'], 'avatar', '' );
    }


  //wp_die( $upd_cat['term_id'] ); // чтобы сервер прислал id

}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_update_user', 'update_user');


function delete_user() {
  $user_id = $_POST['post_id'];
  $cur_user_id = get_current_user_id();
  $user_groups = wp_get_object_terms($cur_user_id, 'user_position');

  $term_id = get_term_by('name', "{$user_id}", 'user_position');


  $object_term_relationships = wp_delete_object_term_relationships( $user_id, 'user_position' );
  $delete_term = wp_delete_term($term_id->term_id, 'user_position');
  $delete_user = wp_delete_user($user_id);


  $obj_merged = (object) array_merge((array) $object_term_relationships, (array) $delete_term, (array) $delete_user);
  echo json_encode($obj_merged);
  exit;
}

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_delete_user', 'delete_user');




function get_user_data() {
		$user_id = $_POST['post_id'];
		$user_data = get_userdata($user_id);
		$user_meta = get_user_meta($user_id);

    $new_array = array(
      "ID"=> $user_id,
      "name"=> $user_meta['first_name'][0],
      "description"=> $user_data->description,
      "avatar" => $user_meta['avatar'][0],
      "user_tel_number"=> $user_meta['user_tel_number'][0],
      "login"=> $user_data->user_login,
      "pass"=> $user_data->user_pass,
      "registered"=> $user_data->user_registered,
      "user_role"=> $user_data->user_role,
      "user_salary"=> $user_data->user_salary,
      "user_salary_percent"=> $user_meta['user_salary_percent'][0],
      "mail"=> $user_data->user_email
    );

		echo json_encode($new_array);
		exit;
}
// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_user_data', 'get_user_data');






// function send_welcome_email_to_new_user($user_id) {
// 	 $user = get_userdata($user_id);
// 	 $user_email = $user->user_email;
// 	 // for simplicity, lets assume that user has typed their first and last name when they sign up
// 	 $user_full_name = $user->user_firstname . $user->user_lastname;
//
// 	 // Now we are ready to build our welcome email
// 	 $to = $user_email;
// 	 $subject = "Hi " . $user_full_name . ", welcome to our site!";
// 	 $body = '
// 						 <h1>Dear ' . $user_full_name . ',</h1></br>
// 						 <p>Thank you for joining our site. Your account is now active.</p>
// 						 <p>Please go ahead and navigate around your account.</p>
// 						 <p>Let me know if you have further questions, I am here to help.</p>
// 						 <p>Enjoy the rest of your day!</p>
// 						 <p>Kind Regards,</p>
// 						 <p>poanchen</p>
// 	 ';
// 	 $headers = array('Content-Type: text/html; charset=UTF-8');
// 	 if (wp_mail($to, $subject, $body, $headers)) {
// 		 error_log("email has been successfully sent to user whose email is " . $user_email);
// 	 }else{
// 		 error_log("email failed to sent to user whose email is " . $user_email);
// 	 }
//  }
//
//  // THE ONLY DIFFERENCE IS THIS LINE
//  add_action('user_register', 'send_welcome_email_to_new_user');




function new_service_register($user_id) {
	// Загрузка логотипа нового сервиса

	if( wp_verify_nonce( $_POST['fileup_nonce'], 'my_file_upload' ) ){
		if ( ! function_exists( 'wp_handle_upload' ) )
			require_once( ABSPATH . 'wp-admin/includes/file.php' );

		$file = & $_FILES['my_file_upload'];
		$overrides = [ 'test_form' => false ];
		$movefile = wp_handle_upload( $file, $overrides );

		wp_insert_term($_POST['company_name'],'user_position', // таксономия
			array(
				'description' => $movefile['url']
			)
		);

		if ( $movefile && empty($movefile['error']) ) {
			echo "Файл был успешно загружен.\n";


		} else {
			echo "Возможны атаки при загрузке файла!\n";
		}
	}

	wp_set_object_terms($user_id, $_POST['company_name'],'user_position');
  wp_set_object_terms($user_id, $_POST['company_name'],'clients_taxonomy');
	wp_set_object_terms($user_id, $_POST['company_name'],'works_taxonomy');
	wp_set_object_terms($user_id, $_POST['company_name'],'repair_parts_taxonomy');

	$new_term = get_term_by('name',$_POST['company_name'],'user_position');
		wp_insert_term(
			"$user_id",  // новый термин
			'user_position', // таксономия
			array(
				'description' => $user_id,
				'slug'        => $user_id,
				'parent'      => $new_term->term_id
			)
		);



    $term_url = get_term_link($new_term->term_id, 'user_position');
    $user_data = wp_update_user( array( 'ID' => $user_id, 'user_url' => $term_url, 'role' => 'owner' ) );

		update_user_meta( $user_id, 'user_role', "Администратор");
		update_user_meta( $user_id, 'user_salary_percent', 'От стоимости ремонта');
		update_user_meta( $user_id, 'user_salary', '0');
		update_user_meta( $user_id, 'user_tel_number', '');
		update_user_meta( $user_id, 'avatar', '');
		nocache_headers();
		wp_clear_auth_cookie();
    wp_set_auth_cookie($user_id);
		wp_set_current_user($user_id);

    $user = get_user_by( 'id', $user_id );
    do_action( 'wp_login', $user->user_login );//`[Codex Ref.][1]
    wp_redirect( home_url() ); // You can change home_url() to the specific URL,such as "wp_redirect( 'http://www.wpcoke.com' )";
    exit;
}


if(is_user_logged_in()){
	add_action( 'user_register', 'set_terms_for_ajax_user', 10, 1 );
}
else{
	add_action( 'user_register', 'new_service_register', 10, 1 );
}


?>
