

<footer class="page-footer flex-row flex-align-center flex-justify-center">
	<a href="<?php echo site_url(); ?>"><?php bloginfo('name'); ?></a>© 2020 |</a>
	<a href="mailto:support@remonline.ru">support@bazaremontov.ua</a>
</footer>
<div id="mobile-bottom-menu" class="hide-on-large-only">
	<?php
		wp_nav_menu( [
			'menu' => 'mobile menu',
		 	'container' => 'div',
			'menu_class' => 'mobile-menu-bottom',
		 ]);
	?>
	<!-- <a href="#" data-target="mobile-menu" class="sidenav-trigger waves-effect waves-teal btn-flat icon-button m-right-double hide-on-large-only">
	  <i class="material-icons">menu</i>
	</a> -->
<ul class="mobile-menu-bottom flex-column" style="width: 25%">
	<li class="menu-item menu-item-type-post_type menu-item-object-page">
		<a href="#" data-target="mobile-menu" class="sidenav-trigger">
		  <i class="material-icons">menu</i>Меню
		</a>
	</li>
</ul>

</div>
