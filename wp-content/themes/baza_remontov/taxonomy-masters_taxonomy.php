<?php get_header() ; ?>
<main id="page">
<h1>Master's page</h1>
	<?php

 	$data = get_queried_object();
	$user_data = get_userdata($data->name);
	$user_meta = get_user_meta($user_data->ID);

	// echo "<pre>";
 // print_r($user_data) ;
 // echo "</pre>";

		$posts_ids = array();
		if (have_posts()):
			while (have_posts()) : the_post();
			array_push($posts_ids, get_the_ID());
			endwhile;
			$post_number = sizeof($posts_ids);
		else:
			$posts_ids = array(1);
			$post_number = 0;
		endif;

		$get_orders = get_my_orders(get_current_user_id(), $posts_ids);
		echo "<script>window.apiData = ".json_encode($get_orders)."</script>";
	?>


	<div class="row">
		<div class="col s12">
			<div class="card" style="margin: 16px 0;">
				<table>
					<tr>
						<td>posts</td>
						<td><?php echo $post_number ?></td>
					</tr>
					<tr>
						<td>user ID</td>
						<td><?php echo $user_data->ID ?></td>
					</tr>
					<tr>
						<td>term ID</td>
						<td><?php echo $data->term_id ?></td>
					</tr>
					<tr>
						<td>name</td>
						<td><?php echo $user_data->first_name . ' ' . $user_data->last_name ?></td>
					</tr>
					<tr>
						<td>user_registered</td>
						<td><?php echo $user_data->user_registered ?></td>
					</tr>
					<tr>
						<td>user_login</td>
						<td><?php echo $user_data->user_login ?></td>
					</tr>
					<tr>
						<td>user_pass</td>
						<td><?php echo $user_data->user_pass ?></td>
					</tr>
					<tr>
						<td>user_email</td>
						<td><?php echo $user_data->user_email ?></td>
					</tr>
					<tr>
						<td>role</td>
						<td><?php echo $user_meta['user_role'][0] ?></td>
					</tr>
					<tr>
						<td>avatar</td>
						<td><img class="rounded" width="32" height="32" src="<?php echo $user_meta['avatar'][0] ?>"></td>
					</tr>
					<tr>
						<td>tel</td>
						<td><?php echo $user_meta['user_tel_number'][0] ?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col s12">
			<div id="content"></div>
		</div>
	</div>

	<script type="text/javascript">
	  var table = new Datatable(window.apiData, "#content");
	</script>

</main>
<?php get_footer(); ?>
